Language: es.tag

[1]  seamos_/VSfin_/ser los_/ART_/el que_/CQUE_/que aman_/VLfin_/amar y_/CC_/y los_/ART_/el amados_/VLadj_/<unknown> ;_/SEMICOLON_/; 
[2]  estaban_/VEfin_/estar interesados_/VLadj_/interesar en_/PREP_/en el_/ART_/el amor_/NC_/amor ,_/CM_/, no_/NEG_/no en_/PREP_/en la_/ART_/el guerra_/NC_/guerra ._/FS_/. 
[3]  seamos_/VSfin_/ser los_/ART_/el que_/CQUE_/que aman_/VLfin_/amar y_/CC_/y los_/ART_/el amados_/VLadj_/<unknown> ;_/SEMICOLON_/; 
[4]  estaban_/VEfin_/estar interesados_/VLadj_/interesar en_/PREP_/en el_/ART_/el amor_/NC_/amor ,_/CM_/, no_/NEG_/no en_/PREP_/en la_/ART_/el guerra_/NC_/guerra ._/FS_/. 
[5]  pero_/NC_/pero había_/VHfin_/haber renunciado_/VLadj_/renunciar a_/PREP_/a su_/PPO_/suyo amor_/NC_/amor por_/PREP_/por la_/ART_/el música_/NC_/música para_/CSUBI_/para hacer_/VLinf_/hacer carrera_/NC_/carrera 
[6]  y_/CC_/y la_/ART_/el palabra_/NC_/palabra ,_/CM_/, cariño_/NC_/cariño ,_/CM_/, será_/VSfin_/ser '_/QT_/' Basingstoke_/NP_/<unknown> '_/QT_/' "_/QT_/" ._/FS_/. 
[7]  si_/CSUBX_/si no_/NEG_/no esperamos_/VLfin_/esperar encontrar_/VLinf_/encontrar el_/ART_/el amor_/NC_/amor y_/CC_/y estar_/VEinf_/estar sanos_/ADJ_/sano y_/CC_/y tener_/VLinf_/tener éxito_/NC_/éxito ,_/CM_/, 
[8]  amor_/NC_/amor ,_/CM_/, valores_/NC_/valor fuertes_/ADJ_/fuerte ,_/CM_/, 
[9]  Todo_/NP_/<unknown> eso_/DM_/ese envuelto_/VLadj_/envolver en_/PREP_/en un_/ART_/un amor_/NC_/amor incondicional_/ADJ_/incondicional 
[10]  que_/CQUE_/que este_/DM_/este es_/VSfin_/ser el_/ART_/el amor_/NC_/amor de_/PREP_/de mi_/PPO_/mi|mío vida_/NC_/vida ,_/CM_/, mi_/PPO_/mi|mío hija_/NC_/hijo Jay_/NP_/<unknown> ._/FS_/. 
[11]  El_/ADJ_/<unknown> poeta_/NC_/poeta Auden_/NP_/<unknown> dijo_/VLfin_/decir :_/COLON_/: "_/QT_/" Muchos_/QU_/mucho han_/VHfin_/haber vivido_/VLadj_/vivir sin_/PREP_/sin amor_/NC_/amor ._/FS_/. 
[12]  Una_/NP_/<unknown> última_/ADJ_/último cosa_/NC_/cosa :_/COLON_/: cuando_/CSUBX_/cuando le_/PPC_/él|le pongan_/VLfin_/poner valor_/NC_/valor a_/PREP_/a cosas_/NC_/cosa como_/CSUBX_/como la_/ART_/el salud_/NC_/salud ,_/CM_/, 
[13]  acerca de_/PREP_/acerca~de sus_/PPO_/suyo seres_/NC_/ser queridos_/ADJ_/querido 
[14]  más_/ADV_/más creo_/VLfin_/crear|creer|creer que_/CQUE_/que ,_/CM_/, realmente_/ADV_/real ,_/CM_/, se_/SE_/se trata_/VLfin_/tratar de_/PREP_/de una_/ART_/un especie_/NC_/especie de_/PREP_/de amor_/NC_/amor ._/FS_/. 
[15]  Y_/ALFS_/Y mi_/PPO_/mi|mío amor_/NC_/amor por_/PREP_/por la_/ART_/el ciencia_/NC_/ciencia ficción_/NC_/ficción 
[16]  Y_/ALFS_/Y mi_/PPO_/mi|mío romance_/NC_/romance con_/PREP_/con el_/ART_/el océano_/NC_/océano sigue_/VLfin_/seguir ,_/CM_/, 
[17]  Va_/VLadj_/<unknown> a_/PREP_/a ser_/VSinf_/ser un_/ART_/un romance_/NC_/romance épico_/ADJ_/épico ,_/CM_/, 
[18]  la_/ART_/el posibilidad_/NC_/posibilidad de_/PREP_/de amar_/VLinf_/amar --_/DASH_/-- 
[19]  ¿_/FS_/? Estás_/VEfin_/estar preparado_/VLadj_/preparar para_/CSUBI_/para colocar_/VLinf_/colocar esto_/DM_/este dentro de_/PREP_/dentro~de un_/ART_/un ser_/NC_/ser querido_/ADJ_/querido ,_/CM_/, de_/PREP_/de tu_/PPO_/tú propio_/ADJ_/propio hijo_/NC_/hijo ,_/CM_/, 
[20]  pero_/NC_/pero tenía_/VLfin_/tener una_/ART_/un novia_/NC_/novio en_/PREP_/en Inglaterra_/NP_/Inglaterra ._/FS_/. El_/ART_/el amor_/NC_/amor de_/PREP_/de su_/PPO_/suyo vida_/NC_/vida ,_/CM_/, 
[21]  El_/VLinf_/<unknown> pequeño_/ADJ_/pequeño acto_/NC_/acto de_/PREP_/de amor_/NC_/amor de_/PREP_/de mi_/PPO_/mi|mío padre_/NC_/padre de_/PREP_/de acogida_/VLadj_/acoger 
[22]  incluso_/ADV_/incluso con_/PREP_/con el_/ART_/el más_/ADV_/más pequeño_/ADJ_/pequeño acto_/NC_/acto de_/PREP_/de amor_/NC_/amor ._/FS_/. 
[23]  su_/PPO_/suyo acto_/NC_/acto de_/PREP_/de amor_/NC_/amor y_/CC_/y cariño_/NC_/cariño 
[24]  que_/CQUE_/que mi_/PPO_/mi|mío pasión_/NC_/pasión por_/PREP_/por los_/ART_/el autos_/NC_/auto y_/CC_/y camiones_/NC_/camión 
[25]  Era_/VLinf_/<unknown> un_/ART_/un verano_/NC_/verano de_/PREP_/de amor_/NC_/amor si_/CSUBX_/si eras_/VLfin_/erar|ser blanco_/ADJ_/blanco ._/FS_/. 
[26]  Mis_/NP_/<unknown> madre_/NC_/madre de_/PREP_/de acogida_/VLadj_/acoger me_/PPX_/yo dijo_/VLfin_/decir que_/CQUE_/que me_/PPX_/yo fuera_/ADV_/fuera a_/PREP_/a pensar_/VLinf_/pensar sobre_/PREP_/sobre el_/ART_/el amor_/NC_/amor 
[27]  de_/PREP_/de la_/ART_/el búsqueda_/NC_/búsqueda del_/PDEL_/del amor_/NC_/amor ,_/CM_/, un_/ART_/un lugar_/NC_/lugar donde_/ADV_/donde la_/ART_/el disfunción_/NC_/disfunción 
[28]  Amor_/NC_/amor y_/CC_/y paz_/NC_/paz fueron_/VSfin_/ser mencionadas_/VLadj_/mencionar ._/FS_/. 
[29]  CA_/NP_/CA :_/COLON_/: Amor_/NC_/amor y_/CC_/y paz_/NC_/paz fueron_/VSfin_/ser mencionadas_/ADJ_/mencionado ,_/CM_/, 
[30]  Mapendo_/NP_/<unknown> quiere_/VLfin_/querer decir_/VLinf_/decir "_/QT_/" gran_/ADJ_/gran|grande amor_/NC_/amor "_/QT_/" en_/PREP_/en Swahili_/NP_/<unknown> ._/FS_/. 
[31]  dijo_/VLfin_/decir :_/COLON_/: "_/QT_/" El_/ART_/el amor_/NC_/amor y_/CC_/y la_/ART_/el compasión_/NC_/compasión son_/VSfin_/ser necesidades_/NC_/necesidad ._/FS_/. 
[32]  en_/PREP_/en el_/ART_/el Buda_/NP_/<unknown> del_/PDEL_/del amor_/NC_/amor y_/CC_/y la_/ART_/el bondad_/NC_/bondad ._/FS_/. 
[33]  Fue_/NP_/<unknown> energía_/NC_/energía ,_/CM_/, amor_/NC_/amor y_/CC_/y alegría_/NC_/alegría ._/FS_/. 
[34]  son_/VSfin_/ser el_/ART_/el amor_/NC_/amor y_/CC_/y el_/ART_/el trabajo_/NC_/trabajo ._/FS_/. 
[35]  Amor_/NC_/amor :_/COLON_/: manejar_/VLinf_/manejar con_/PREP_/con éxito_/NC_/éxito 
[36]  Hay_/NP_/<unknown> amor_/NC_/amor en_/PREP_/en el_/ART_/el aire_/NC_/aire ._/FS_/. 
[37]  Es_/ADV_/<unknown> también_/ADV_/también el_/ART_/el gran_/ADJ_/gran|grande amor_/NC_/amor y_/CC_/y pasión_/NC_/pasión de_/PREP_/de mi_/PPO_/mi|mío vida_/NC_/vida ._/FS_/. 
[38]  solamente_/ADV_/solo por_/PREP_/por tener_/VLinf_/tener el_/ART_/el amor_/NC_/amor y_/CC_/y la_/ART_/el tenacidad_/NC_/<unknown> humana_/ADJ_/humano 
[39]  Este_/ADV_/<unknown> proyecto_/NC_/proyecto se_/SE_/se denomina_/VLfin_/denominar "_/QT_/" Nidos_/NP_/<unknown> de_/PREP_/de Amor_/NP_/<unknown> "_/QT_/" ._/FS_/. 
[40]  Éste_/DM_/este se_/SE_/se llama_/VLfin_/llamar "_/QT_/" Nido_/NP_/<unknown> de_/PREP_/de Mechones_/NP_/<unknown> "_/QT_/" ._/FS_/. 
[41]  Y_/ALFS_/Y éste_/DM_/este ,_/CM_/, "_/QT_/" Nido_/NC_/nido de_/PREP_/de canción_/NC_/canción de_/PREP_/de amor_/NC_/amor en_/PREP_/en una_/ART_/un cinta_/NC_/cinta múltiple_/ADJ_/multiple "_/QT_/" ._/FS_/. 
[42]  Y_/ALFS_/Y éste_/DM_/este ,_/CM_/, "_/QT_/" Nido_/NP_/<unknown> para_/PREP_/para el_/ART_/el Amor_/NP_/<unknown> "_/QT_/" ._/FS_/. 
[43]  Más_/ADV_/más que_/CQUE_/que nada_/QU_/nada ,_/CM_/, aprendí_/VLfin_/aprender sobre_/PREP_/sobre el_/ART_/el amor_/NC_/amor ._/FS_/. 
[44]  El_/NP_/<unknown> Amor_/NP_/<unknown> es_/VSfin_/ser algo_/QU_/algo que_/CQUE_/que descubres_/VLfin_/descubrir por_/PREP_/por ti_/PPX_/tú mismo_/ADJ_/mismo ._/FS_/. 
[45]  Es_/ADV_/<unknown> primero_/ADV_/primero amor_/NC_/amor ._/FS_/. 
[46]  es_/VSfin_/ser lo_/ART_/el que_/CQUE_/que llamamos_/VLfin_/llamar amor_/NC_/amor ._/FS_/. 
[47]  y_/CC_/y a_/PREP_/a tomar_/VLinf_/tomar este_/DM_/este cuidado_/ADJ_/cuidado ,_/CM_/, este_/DM_/este amor_/NC_/amor ,_/CM_/, este_/DM_/este respeto_/NC_/respeto 
[48]  e_/CC_/y inclusive_/ADV_/inclusive ,_/CM_/, a veces_/ADV_/a~veces ,_/CM_/, en_/PREP_/en elementos_/NC_/elemento de_/PREP_/de amor_/NC_/amor ._/FS_/. 
[49]  "_/QT_/" Mejor_/NP_/<unknown> la_/ART_/el inmersión_/NC_/inmersión 
[50]  compasión_/NC_/compasión y_/CC_/y amor_/NC_/amor 
[51]  amor_/NC_/amor ,_/CM_/, compasión_/NC_/compasión ,_/CM_/, imaginación_/NC_/imaginación ,_/CM_/, 
[52]  y_/CC_/y comunidad_/NC_/comunidad y_/CC_/y amor_/NC_/amor ._/FS_/. 
[53]  Anónimo_/NP_/<unknown> :_/COLON_/: Querido_/NC_/querido Fox_/NP_/<unknown> News_/NP_/News ,_/CM_/, 
[54]  Estoy_/NP_/<unknown> aquí_/ADV_/aquí para_/CSUBI_/para contaros_/VLinf_/contar la_/ART_/el historia_/NC_/historia de_/PREP_/de "_/QT_/" loco_/ADJ_/loco amor_/NC_/amor "_/QT_/" ,_/CM_/, 
[55]  una_/ART_/un trampa_/NC_/trampa psicológica_/ADJ_/psicológico ,_/CM_/, disfrazada_/VLadj_/disfrazar de_/PREP_/de amor_/NC_/amor ,_/CM_/, 
[56]  Yo_/NP_/<unknown> fui_/VLfin_/ir|ser capaz_/ADJ_/capaz de_/CSUBI_/de poner_/VLinf_/poner fin_/NC_/fin a_/PREP_/a mi_/PPO_/mi|mío loco_/ADJ_/loco amor_/NC_/amor particular_/ADJ_/particular 
[57]  un_/ART_/un poema_/NC_/poema de_/PREP_/de amor_/NC_/amor diferente_/ADJ_/diferente a_/PREP_/a cualquier_/QU_/cualquier otro_/QU_/otro que_/CQUE_/que yo_/PPX_/yo hubiese_/VHfin_/haber escuchado_/VLadj_/escuchar antes_/ADV_/antes ._/FS_/. 
[58]  es_/VSfin_/ser a través de_/PREP_/a~través~de la_/ART_/el promesa_/NC_/promesa de_/PREP_/de sexo_/NC_/sexo y_/CC_/y amor_/NC_/amor ._/FS_/. 
[59]  en_/PREP_/en aquel_/DM_/aquel tiempo_/NC_/tiempo ,_/CM_/, pero_/CCAD_/pero el_/ART_/el amor_/NC_/amor no_/NEG_/no era_/VSfin_/ser uno_/CARD_/uno de_/PREP_/de ellos_/PPX_/él ._/FS_/. 
[60]  su_/PPO_/suyo nombre_/NC_/nombre significa_/VLfin_/significar amor_/NC_/amor --_/DASH_/-- 
[61]  el_/ART_/el amor_/NC_/amor y_/CC_/y la_/ART_/el compasión_/NC_/compasión crean_/VLfin_/crear|creer respeto_/NC_/respeto hacia_/PREP_/hacia toda_/QU_/todo la_/ART_/el vida_/NC_/vida ._/FS_/. 
[62]  tanto_/ADV_/tanto amor_/NC_/amor y_/CC_/y tanta_/ADJ_/tanto compasión_/NC_/compasión ._/FS_/. 
[63]  les_/PPC_/él diste_/VLfin_/dar|distar el_/ART_/el mejor_/ADJ_/bueno|mejor computador_/NC_/computador ._/FS_/. Les_/PPC_/él diste_/VLfin_/dar|distar amor_/NC_/amor ,_/CM_/, 
[64]  terminan_/VLfin_/terminar el_/ART_/el resto_/NC_/resto de_/PREP_/de su_/PPO_/suyo vida_/NC_/vida con_/PREP_/con todo_/QU_/todo ese_/DM_/ese amor_/NC_/amor ,_/CM_/, educación_/NC_/educación ,_/CM_/, dinero_/NC_/dinero 
[65]  y_/CC_/y ahí_/ADV_/ahí conociste_/VLfin_/conocer al_/PAL_/al amor_/NC_/amor de_/PREP_/de tu_/PPO_/tú vida_/NC_/vida ._/FS_/. 
[66]  Esto es_/ADV_/esto~es lo_/ART_/el que_/CQUE_/que realmente_/ADV_/real necesitamos_/VLfin_/necesitar :_/COLON_/: conexión_/NC_/conexión y_/CC_/y amor_/NC_/amor --_/DASH_/-- cuarta_/ORD_/cuarto necesidad_/NC_/necesidad ._/FS_/. 
[67]  porque_/CSUBX_/porque el_/ART_/el amor_/NC_/amor da_/VLfin_/dar mucho_/QU_/mucho miedo_/NC_/miedo ._/FS_/. No_/NEG_/no quieren_/VLfin_/querer salir_/VLinf_/salir heridos_/VLadj_/herir ._/FS_/. 
[68]  o_/CC_/o el_/ART_/el amor_/NC_/amor ?_/FS_/? Todos_/QU_/todo necesitamos_/VLfin_/necesitar los_/ART_/el seis_/CARD_/seis ,_/CM_/, pero_/CCAD_/pero cualquiera_/QU_/cualquier 
[69]  Cada_/NC_/<unknown> uno_/CARD_/uno de_/PREP_/de ellos_/PPX_/él está_/VEfin_/estar en_/PREP_/en una_/ART_/un relación_/NC_/relación romántica_/ADJ_/romántico 
[70]  una_/ART_/un vida_/NC_/vida en_/PREP_/en que_/CQUE_/que tu_/PPO_/tú trabajo_/NC_/trabajo ,_/CM_/, la_/ART_/el crianza_/NC_/crianza de_/PREP_/de tus_/PPO_/tu hijos_/NC_/hijo ,_/CM_/, tu_/PPO_/tú amor_/NC_/amor ,_/CM_/, tu_/PPO_/tú tiempo_/NC_/tiempo libre_/ADJ_/libre ;_/SEMICOLON_/; el_/ART_/el tiempo_/NC_/tiempo se_/SE_/se detiene_/VLfin_/detener para_/PREP_/para ti_/PPX_/tú ._/FS_/. 
[71]  Pero_/NP_/<unknown> en_/PREP_/en la_/ART_/el tercera_/ORD_/tercero arena_/NC_/arena de_/PREP_/de la_/ART_/el vida_/NC_/vida ,_/CM_/, el_/ART_/el amor_/NC_/amor ,_/CM_/, Len_/NP_/<unknown> es_/VSfin_/ser un_/ART_/un fracaso_/NC_/fracaso abismal_/ADJ_/<unknown> ._/FS_/. 
[72]  Recomponer_/VLinf_/recomponer tu_/PPO_/tú trabajo_/NC_/trabajo ,_/CM_/, tu_/PPO_/tú amor_/NC_/amor ,_/CM_/, 
[73]  Erez_/NP_/<unknown> Lieberman_/NP_/<unknown> Aiden_/NP_/<unknown> :_/COLON_/: Todo_/QU_/todo el_/ART_/el mundo_/NC_/mundo sabe_/VLfin_/saber 
[74]  pondrá_/VLfin_/poner nunca_/ADV_/nunca la_/ART_/el menor_/ADJ_/pequeño nota_/NC_/nota de_/PREP_/de tristeza_/NC_/tristeza 
[75]  Escribió_/VLfin_/escribir esto_/DM_/este a_/PREP_/a su_/PPO_/suyo amor_/NC_/amor ,_/CM_/, 
[76]  Es_/ADV_/<unknown> una_/ART_/un expresión_/NC_/expresión de_/PREP_/de amor_/NC_/amor ._/FS_/. Es_/VSfin_/ser una_/ART_/un expresión_/NC_/expresión de_/PREP_/de ..._/DOTS_/... 
[77]  El_/NP_/<unknown> Arte_/NP_/<unknown> ,_/CM_/, en_/PREP_/en el_/ART_/el sentido_/NC_/sentido platónico_/ADJ_/<unknown> ,_/CM_/, es_/VSfin_/ser verdad_/NC_/verdad ,_/CM_/, es_/VSfin_/ser belleza_/NC_/belleza ,_/CM_/, y_/CC_/y amor_/NC_/amor ._/FS_/. 
[78]  No_/ADV_/<unknown> tenemos_/VLfin_/tener en realidad_/ADV_/en~realidad problemas_/NC_/problema para_/CSUBI_/para hablar_/VLinf_/hablar de_/PREP_/de amor_/NC_/amor ._/FS_/. 
[79]  Bien_/ADV_/bien ,_/CM_/, esta_/DM_/este experiencia_/NC_/experiencia del_/PDEL_/del amor_/NC_/amor ,_/CM_/, y_/CC_/y la_/ART_/el experiencia_/NC_/experiencia del_/PDEL_/del diseño_/NC_/diseño ,_/CM_/, para_/PREP_/para mí_/PPX_/yo ,_/CM_/, 
[80]  Descubrí_/NP_/<unknown> algo_/QU_/algo acerca_/ADV_/acerca del_/PDEL_/del amor_/NC_/amor y_/CC_/y el_/ART_/el diseño_/NC_/diseño a través de_/PREP_/a~través~de un_/ART_/un proyecto_/NC_/proyecto llamado_/VLadj_/llamar Deep_/NP_/<unknown> Blue_/NP_/<unknown> ._/FS_/. 
[81]  sabes_/VLfin_/saber ,_/CM_/, podrías_/VLfin_/poder quitar_/VLinf_/quitar la_/ART_/el palabra_/NC_/palabra amor_/NC_/amor de_/PREP_/de un_/ART_/un montón_/NC_/montón de_/PREP_/de cosas_/NC_/cosa en_/PREP_/en nuestra_/ADJ_/nuestro sociedad_/NC_/sociedad ,_/CM_/, 
[82]  Dijo_/NP_/<unknown> ,_/CM_/, "_/QT_/" El_/NC_/<unknown> amor_/NC_/amor no_/NEG_/no es_/VSfin_/ser egoísta_/ADJ_/egoísta "_/QT_/" ,_/CM_/, dijo_/VLfin_/decir ,_/CM_/, 
[83]  "_/QT_/" El_/NP_/<unknown> amor_/NC_/amor no_/NEG_/no se_/SE_/se trata_/VLfin_/tratar de_/CSUBI_/de contar_/VLinf_/contar cuantas_/ADV_/cuanto veces_/NC_/vez digo,'Te_/ADJ_/<unknown> quiero_/VLfin_/querer '_/QT_/' ._/FS_/. 
[84]  El_/VLinf_/<unknown> amor_/NC_/amor no_/NEG_/no es_/VSfin_/ser egoísta_/ADJ_/egoísta "_/QT_/" ._/FS_/. Y_/CC_/y pensé_/VLfin_/pensar sobre_/PREP_/sobre esto_/DM_/este ,_/CM_/, y_/CC_/y pensé_/VLfin_/pensar ,_/CM_/, 
[85]  "_/QT_/" Sabes_/NP_/<unknown> ,_/CM_/, no_/NEG_/no estoy_/VEfin_/estar demostrando_/VLger_/demostrar amor_/NC_/amor aquí_/ADV_/aquí ._/FS_/. De_/PREP_/de verdad_/NC_/verdad que_/CQUE_/que no_/NEG_/no estoy_/VEfin_/estar demostrando_/VLger_/demostrar amor_/NC_/amor ._/FS_/. 
[86]  Pusimos_/ADV_/<unknown> el_/ART_/el coche_/NC_/coche de nuevo_/ADV_/de~nuevo en_/PREP_/en el_/ART_/el centro_/NC_/centro de_/PREP_/de nuestros_/PPO_/nuestro pensamientos_/NC_/pensamiento ,_/CM_/, y_/CC_/y pusimos_/VLfin_/poner amor_/NC_/amor ,_/CM_/, 
[87]  Hay_/NP_/<unknown> mucho_/ADV_/mucho que_/CQUE_/que decir_/VLinf_/decir acerca de_/PREP_/acerca~de la_/ART_/el confianza_/NC_/confianza y_/CC_/y el_/ART_/el amor_/NC_/amor ,_/CM_/, 
[88]  Es_/ADV_/<unknown> cierto_/QU_/cierto que_/CQUE_/que la_/ART_/el confianza_/NC_/confianza y_/CC_/y el_/ART_/el amor_/NC_/amor ,_/CM_/, que_/CQUE_/que hacen_/VLfin_/hacer que_/CQUE_/que merezca_/VLfin_/merecer la_/ART_/el pena_/NC_/pena ._/FS_/. 
[89]  "_/QT_/" Santo_/NP_/<unknown> Jesús_/NP_/Jesús ,_/CM_/, este_/DM_/este niño_/NC_/niño ya_/ADV_/ya ha_/VHfin_/haber comenzado_/VLadj_/comenzar "_/QT_/" ._/FS_/. 
[90]  y_/CC_/y fue_/VLfin_/ir|ser amor_/NC_/amor a primera vista_/ADV_/a~primera~vista ._/FS_/. 
[91]  ¿_/FS_/? Cómo_/INT_/cómo empezó_/VLfin_/empezar ?_/FS_/? Pues_/CSUBF_/pues ,_/CM_/, observen_/VLfin_/observar la_/ART_/el atención_/NC_/atención que_/CQUE_/que José_/NP_/José presta_/VLfin_/prestar a_/PREP_/a los_/ART_/el detalles_/NC_/detalle ._/FS_/. 
[92]  Y_/ALFS_/Y lo_/ART_/el más_/ADV_/más importante_/ADJ_/importante ,_/CM_/, simplemente_/ADV_/simple quiéranlos_/VLfin_/<unknown> ._/FS_/. 
[93]  Nada_/ADV_/<unknown> funciona_/VLfin_/funcionar mejor_/ADJ_/bueno|mejor que_/CQUE_/que el_/ART_/el amor_/NC_/amor incondicional_/ADJ_/incondicional ._/FS_/. 
[94]  saben_/VLfin_/saber que_/CQUE_/que el_/ART_/el amor_/NC_/amor hace_/VLfin_/hacer las_/ART_/el cosas_/NC_/cosa reales_/ADJ_/real ._/FS_/. 
[95]  La_/NP_/La pasión_/NC_/pasión es_/VSfin_/ser tu_/PPO_/tú gran_/ADJ_/gran|grande amor_/NC_/amor ._/FS_/. 
[96]  tu_/PPO_/tú gran_/ADJ_/gran|grande amor_/NC_/amor en_/PREP_/en comparación_/NC_/comparación con_/PREP_/con las_/ART_/el otras_/QU_/otro cosas_/NC_/cosa 
[97]  de_/PREP_/de su_/PPO_/suyo amor_/NC_/amor por_/PREP_/por ella_/PPX_/él ._/FS_/. 
[98]  a_/PREP_/a una_/ART_/un sola_/ADJ_/solo cosa_/NC_/cosa :_/COLON_/: el_/ART_/el amor_/NC_/amor ,_/CM_/, 
[99]  amor_/NC_/amor al_/PAL_/al país_/NC_/país ,_/CM_/, a_/PREP_/a las_/ART_/el subidas_/VLadj_/subir 
[100]  amor_/NC_/amor y_/CC_/y respeto_/NC_/respeto 
[101]  amor_/NC_/amor y_/CC_/y respeto_/NC_/respeto a_/PREP_/a uno_/CARD_/uno mismo_/ADJ_/mismo ,_/CM_/, 
[102]  o sea_/ADV_/o~sea que_/CQUE_/que lo_/PPC_/él hacen_/VLfin_/hacer por_/PREP_/por gusto_/NC_/gusto --_/DASH_/-- 
[103]  seamos_/VSfin_/ser los_/ART_/el que_/CQUE_/que aman_/VLfin_/amar y_/CC_/y los_/ART_/el amados_/VLadj_/<unknown> ;_/SEMICOLON_/; 
[104]  luego_/ADV_/luego de_/CSUBI_/de recibir_/VLinf_/recibir una_/ART_/un queja_/VLfin_/quejar del_/PDEL_/del Senador_/NC_/senador americano_/ADJ_/americano Joe_/NP_/Joe Lieberman_/NP_/<unknown> ,_/CM_/, 
[105]  también_/ADV_/también nos_/PPX_/nosotros enseñaron_/VLfin_/enseñar amor_/NC_/amor y_/CC_/y confianza_/NC_/confianza ._/FS_/. 
[106]  y_/CC_/y se_/SE_/se podía_/VLfin_/poder sentir_/VLinf_/sentir ese_/DM_/ese amor_/NC_/amor en_/PREP_/en los_/ART_/el sermones_/VLfin_/sermonar que_/CQUE_/que daba_/VLfin_/dar 
[107]  que_/CQUE_/que son_/VSfin_/ser aficionados_/VLadj_/aficionar si_/CSUBX_/si querían_/VLfin_/querer unirse_/VCLIinf_/unirse a_/PREP_/a nosotros_/PPX_/nosotros en_/PREP_/en el_/ART_/el escenario_/NC_/escenario 
[108]  45_/CARD_/@card@ años_/NC_/año de_/PREP_/de una_/ART_/un historia_/NC_/historia de_/PREP_/de amor_/NC_/amor 
[109]  y_/CC_/y no_/NEG_/no están_/VEfin_/estar demostrando_/VLger_/demostrar de_/PREP_/de verdad_/NC_/verdad amor_/NC_/amor por_/PREP_/por ellos_/PPX_/él ._/FS_/. 
[110]  y_/CC_/y como_/CSUBX_/como los_/ART_/el jóvenes_/NC_/joven amantes_/VLfin_/amantar ,_/CM_/, tenemos_/VLfin_/tener miedo_/NC_/miedo 
[111]  Porque_/NP_/<unknown> al_/CSUBI_/al preguntar_/VLinf_/preguntar a_/PREP_/a las_/ART_/el personas_/NC_/persona sobre_/PREP_/sobre el_/ART_/el amor_/NC_/amor 
[112]  éstos_/DM_/este tienen_/VLfin_/tener un_/ART_/un sentido_/NC_/sentido fuerte_/ADJ_/fuerte de_/PREP_/de amor_/NC_/amor y_/CC_/y pertenencia_/NC_/pertenencia ._/FS_/. 
[113]  con_/PREP_/con un_/ART_/un sentido_/NC_/sentido fuerte_/ADJ_/fuerte de_/PREP_/de amor_/NC_/amor y_/CC_/y pertenencia_/NC_/pertenencia ._/FS_/. 
[114]  pensaban_/VLfin_/pensar que_/CQUE_/que eran_/VSfin_/ser dignas_/ADJ_/digno 
[115]  de_/PREP_/de amor_/NC_/amor y_/CC_/y pertenencia_/NC_/pertenencia ._/FS_/. 
[116]  la_/ART_/el pertenencia_/NC_/pertenencia ,_/CM_/, el_/ART_/el amor_/NC_/amor ._/FS_/. 
[117]  pero_/NC_/pero eres_/VSfin_/ser digno_/ADJ_/digno de_/PREP_/de amor_/NC_/amor y_/CC_/y pertenencia_/NC_/pertenencia "_/QT_/" ._/FS_/. 
[118]  "_/QT_/" Amor_/NP_/<unknown> verdadero_/ADJ_/verdadero ..._/DOTS_/... en_/PREP_/en otro_/QU_/otro matrimonio_/NC_/matrimonio ._/FS_/. "_/QT_/" 
[119]  ¿_/FS_/? La_/ART_/el gente_/NC_/gente ama_/VLfin_/amar a_/PREP_/a sus_/PPO_/suyo hijas_/NC_/hijo 
[120]  sí_/PPX_/él ,_/CM_/, creo_/VLfin_/crear|creer|creer que_/CQUE_/que podrías_/VLfin_/poder sentir_/VLinf_/sentir probablemente_/ADV_/probable|probablemente la_/ART_/el emoción_/NC_/emoción del_/PDEL_/del amor_/NC_/amor ._/FS_/. 
[121]  llena_/ADJ_/lleno de_/PREP_/de risas_/NC_/risa y_/CC_/y amor_/NC_/amor ,_/CM_/, en_/PREP_/en una_/ART_/un familia_/NC_/familia muy_/ADV_/muy unida_/VLadj_/unir ._/FS_/. 
[122]  como_/CSUBX_/como el_/ART_/el amor_/NC_/amor y_/CC_/y la_/ART_/el bondad_/NC_/bondad 
[123]  una_/ART_/un canción_/NC_/canción de_/PREP_/de amor_/NC_/amor bosnia_/ADJ_/bosnio 
[124]  y_/CC_/y en_/PREP_/en relaciones_/NC_/relación entre_/PREP_/entre amantes_/VLfin_/amantar y_/CC_/y padres_/NC_/padre de_/PREP_/de familia_/NC_/familia 
[125]  haría_/VLfin_/hacer un_/ART_/un espacio_/NC_/espacio para_/PREP_/para el_/ART_/el amor_/NC_/amor ,_/CM_/, un_/ART_/un espacio_/NC_/espacio para_/PREP_/para la_/ART_/el generosidad_/NC_/generosidad ._/FS_/. 
[126]  Sé_/NP_/<unknown> que_/CQUE_/que puede_/VMfin_/poder sonar_/VLinf_/sonar rudo_/ADJ_/rudo ,_/CM_/, 
[127]  (_/LP_/( Video_/NP_/<unknown> )_/RP_/) John_/NP_/John Edwards_/NP_/Edwards :_/COLON_/: Estaría_/VEfin_/estar encantado_/VLadj_/encantar de_/CSUBI_/de realizar_/VLinf_/realizar una_/ART_/un ._/FS_/. 
[128]  o_/CC_/o respeto_/NC_/respeto ,_/CM_/, o_/CC_/o amor_/NC_/amor ._/FS_/. 
[129]  que_/CQUE_/que iba_/VLfin_/ir a_/PREP_/a crecer_/VLinf_/crecer y_/CC_/y se_/SE_/se casaría_/VLfin_/casar con_/PREP_/con ese_/DM_/ese amor_/NC_/amor de_/PREP_/de la_/ART_/el secundaria_/ADJ_/secundario 
[130]  parezca_/VLfin_/parecer haber_/VHinf_/haber escapado_/VLadj_/escapar de_/PREP_/de un_/ART_/un poema_/NC_/poema de_/PREP_/de amor_/NC_/amor 
[131]  Puedes_/VLfin_/poder pasar_/VLinf_/pasar del_/PDEL_/del amor_/NC_/amor al_/CSUBI_/al odio_/VLfin_/odiar ,_/CM_/, 
[132]  Audiencia_/NP_/<unknown> :_/COLON_/: Fiesta_/NC_/fiesta ._/FS_/. Amor_/NC_/amor ._/FS_/. 
[133]  Después de_/PREP_/después~de 1,300_/CARD_/@card@ años_/NC_/año estos_/DM_/este dos_/NC_/dos amantes_/VLfin_/amantar 
[134]  Cantan_/VLfin_/cantar por_/PREP_/por amor_/NC_/amor ,_/CM_/, bailan_/VLfin_/bailar por_/PREP_/por amor_/NC_/amor ,_/CM_/, 
[135]  componen_/VLfin_/componer poemas_/NC_/poema e_/CC_/y historias_/NC_/historia acerca_/ADV_/acerca del_/PDEL_/del amor_/NC_/amor ._/FS_/. 
[136]  Cuentan_/VLfin_/contar mitos_/NC_/mito y_/CC_/y leyendas_/NC_/leyenda acerca_/ADV_/acerca del_/PDEL_/del amor_/NC_/amor ._/FS_/. 
[137]  Suspiran_/VLfin_/suspirar por_/PREP_/por amor_/NC_/amor ,_/CM_/, viven_/VLfin_/vivir por_/PREP_/por amor_/NC_/amor ,_/CM_/, 
[138]  matan_/VLfin_/matar por_/PREP_/por amor_/NC_/amor y_/CC_/y mueren_/VLfin_/morir por_/PREP_/por amor_/NC_/amor ._/FS_/. 
[139]  Los_/NP_/<unknown> antropólogos_/NC_/antropólogo han_/VHfin_/haber encontrado_/VLadj_/encontrar evidencia_/VLfin_/evidenciar de_/PREP_/de amor_/NC_/amor romántico_/ADJ_/romántico en_/PREP_/en 170_/CARD_/@card@ sociedades_/NC_/sociedad ._/FS_/. 
[140]  Pero_/NP_/<unknown> el_/ART_/el amor_/NC_/amor no_/NEG_/no siempre_/ADV_/siempre es_/VSfin_/ser una_/ART_/un experiencia_/NC_/experiencia feliz_/ADJ_/feliz ._/FS_/. 
[141]  realizaron_/VLfin_/realizar muchas_/QU_/mucho preguntas_/NC_/pregunta acerca_/ADV_/acerca del_/PDEL_/del amor_/NC_/amor ,_/CM_/, 
[142]  Casi_/NP_/<unknown> nadie_/NC_/nadie sale_/VLfin_/salar vivo_/ADJ_/vivo del_/PDEL_/del amor_/NC_/amor ._/FS_/. 
[143]  lo_/ART_/el que_/CQUE_/que pienso_/VLfin_/pensar es_/VSfin_/ser el_/ART_/el poema_/NC_/poema de_/PREP_/de amor_/NC_/amor mas_/ADV_/más poderoso_/ADJ_/poderoso en_/PREP_/en la_/ART_/el Tierra_/NP_/<unknown> ._/FS_/. 
[144]  el_/ART_/el dolor_/NC_/dolor corre_/VLfin_/correr a través de_/PREP_/a~través~de mi_/PPO_/mi|mío cuerpo_/NC_/cuerpo con_/PREP_/con el_/ART_/el fuego_/NC_/fuego de_/PREP_/de mi_/PPO_/mi|mío amor_/NC_/amor por_/PREP_/por tí_/VLfin_/ter ._/FS_/. 
[145]  Dolor_/NP_/<unknown> ardiendo_/VLger_/arder a punto de_/PREP_/a~punto~de estallar_/VLinf_/estallar de_/PREP_/de mi_/PPO_/mi|mío amor_/NC_/amor por_/PREP_/por ti_/PPX_/tú ,_/CM_/, 
[146]  consumido_/VLadj_/consumir por_/PREP_/por el_/ART_/el fuego_/NC_/fuego de_/PREP_/de mi_/PPO_/mi|mío amor_/NC_/amor por_/PREP_/por ti_/PPX_/tú ,_/CM_/, 
[147]  Y_/ALFS_/Y estoy_/VEfin_/estar pensando_/VLger_/pensar en_/PREP_/en tu_/PPO_/tú amor_/NC_/amor por_/PREP_/por mi_/PPO_/mi|mío ,_/CM_/, 
[148]  Estoy_/NP_/<unknown> desgarrado_/VLadj_/desgarrar por_/PREP_/por tu_/PPO_/tú amor_/NC_/amor para_/PREP_/para mi_/PPO_/mi|mío ._/FS_/. 
[149]  ¿_/FS_/? A_/PREP_/a dónde_/INT_/dónde vas_/VLfin_/ir con_/PREP_/con mi_/PPO_/mi|mío amor_/NC_/amor ?_/FS_/? 
[150]  Recuerda_/VLfin_/recordar lo_/ART_/el que_/CQUE_/que te_/PPX_/tú dije_/VLfin_/decir ,_/CM_/, mi_/PPO_/mi|mío amor_/NC_/amor ._/FS_/. 
[151]  Adios_/NP_/<unknown> ,_/CM_/, mi_/PPO_/mi|mío amor_/NC_/amor ,_/CM_/, adios_/NC_/<unknown> "_/QT_/" ._/FS_/. 
[152]  El_/VLinf_/<unknown> amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser una_/ART_/un de_/PREP_/de las_/ART_/el sensaciones_/NC_/sensación mas_/ADV_/más poderosas_/ADJ_/poderoso en_/PREP_/en la_/ART_/el Tierra_/NP_/<unknown> ._/FS_/. 
[153]  Pero_/NP_/<unknown> el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser mucho_/ADV_/mucho mas_/ADV_/más que_/CQUE_/que un_/ART_/un clímax_/NC_/clímax de_/PREP_/de cocaína_/NC_/cocaína --_/DASH_/-- 
[154]  El_/VLinf_/<unknown> amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser una_/ART_/un obsesión_/NC_/obsesión ._/FS_/. Le_/PPC_/él|le posee_/VLfin_/poseer ._/FS_/. 
[155]  El_/ADJ_/<unknown> amor_/NC_/amor es_/VSfin_/ser salvaje_/ADJ_/salvaje ._/FS_/. 
[156]  asociada_/VLadj_/asociar con_/PREP_/con amor_/NC_/amor romántico_/ADJ_/romántico intenso_/ADJ_/intenso ._/FS_/. 
[157]  el_/ART_/el dijo_/VLfin_/decir ,_/CM_/, "_/QT_/" Mientras_/NP_/<unknown> menor_/ADJ_/pequeño es_/VSfin_/ser mi_/PPO_/mi|mío esperanza_/NC_/esperanza ,_/CM_/, mas_/ADV_/más candente_/ADJ_/candente mi_/PPO_/mi|mío amor_/NC_/amor "_/QT_/" ._/FS_/. 
[158]  Cuando_/VLger_/<unknown> a_/PREP_/a usted_/PPX_/usted le_/PPC_/él|le rechazan_/VLfin_/rechazar en_/PREP_/en el_/ART_/el amor_/NC_/amor ,_/CM_/, 
[159]  no_/NEG_/no solo_/ADV_/solo usted_/PPX_/usted está_/VEfin_/estar sumergido_/VLadj_/sumergir en_/PREP_/en sentimientos_/NC_/sentimiento de_/PREP_/de amor_/NC_/amor romántico_/ADJ_/romántico ,_/CM_/, 
[160]  que_/CQUE_/que el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser un_/ART_/un impulso_/NC_/impulso ,_/CM_/, un_/ART_/un impulso_/NC_/impulso básico_/ADJ_/básico de_/PREP_/de apareamiento_/NC_/apareamiento ._/FS_/. 
[161]  El_/VLinf_/<unknown> amor_/NC_/amor romántico_/ADJ_/romántico permite_/VLfin_/permitir que_/CQUE_/que usted_/PPX_/usted enfoque_/NC_/enfoque su_/PPO_/suyo energía_/NC_/energía de_/PREP_/de apareamiento_/NC_/apareamiento 
[162]  Pienso_/ADJ_/<unknown> que_/CQUE_/que de_/PREP_/de toda_/QU_/todo la_/ART_/el poesía_/NC_/poesía que_/CQUE_/que he_/VHfin_/haber leído_/VLadj_/leer sobre_/PREP_/sobre amor_/NC_/amor romántico_/ADJ_/romántico ,_/CM_/, 
[163]  El_/NP_/<unknown> dijo_/VLfin_/decir ,_/CM_/, "_/QT_/" El_/VLfin_/<unknown> buen_/ADJ_/bueno amor_/NC_/amor vive_/VLfin_/vivir en_/PREP_/en estado_/NC_/estado de_/PREP_/de necesidad_/NC_/necesidad "_/QT_/" ._/FS_/. 
[164]  También_/ADV_/también he_/VHfin_/haber llegado_/VLadj_/llegar a_/CSUBI_/a creer_/VLinf_/creer que_/CQUE_/que el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser una_/ART_/un adicción_/NC_/adicción :_/COLON_/: 
[165]  Tengo_/VLfin_/tener una_/ART_/un amiga_/NC_/amigo quien_/REL_/quien pasó_/VLfin_/pasar por_/PREP_/por un_/ART_/un terrible_/ADJ_/terrible romance_/NC_/romance ,_/CM_/, 
[166]  el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser una_/ART_/un de_/PREP_/de las_/ART_/el substancias_/NC_/substancia mas_/ADV_/más adictivas_/ADJ_/<unknown> en_/PREP_/en la_/ART_/el tierra_/NC_/tierra ._/FS_/. 
[167]  de_/PREP_/de lo_/ART_/el que_/CQUE_/que usted_/PPX_/usted y_/CC_/y yo_/PPX_/yo llamamos_/VLfin_/llamar "_/QT_/" amor_/NC_/amor a primera vista_/ADV_/a~primera~vista "_/QT_/" ._/FS_/. 
[168]  lo_/ART_/el que_/CQUE_/que sé_/VLfin_/saber|ser del_/PDEL_/del amor_/NC_/amor lo_/PPC_/él ha_/VHfin_/haber estropeado_/VLadj_/estropear para_/PREP_/para mi_/PPO_/mi|mío ._/FS_/. 
[169]  asociadas_/VLadj_/asociar con_/PREP_/con el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico intenso_/ADJ_/intenso ,_/CM_/, 
[170]  y_/CC_/y preguntadas_/VLadj_/<unknown> acerca_/ADV_/acerca del_/PDEL_/del amor_/NC_/amor romántico_/ADJ_/romántico ._/FS_/. 
[171]  y_/CC_/y en_/PREP_/en algún_/QU_/alguno punto_/NC_/punto --_/DASH_/-- siempre_/ADV_/siempre habrá_/VHfin_/haber magia_/NC_/magia para_/PREP_/para el_/ART_/el amor_/NC_/amor ,_/CM_/, 
[172]  De_/ADJ_/<unknown> manera_/NC_/manera que_/CQUE_/que mi_/PPO_/mi|mío declaración_/NC_/declaración final_/ADJ_/final es_/VSfin_/ser :_/COLON_/: el_/ART_/el amor_/NC_/amor está_/VEfin_/estar en_/PREP_/en nosotros_/PPX_/nosotros ._/FS_/. 
[173]  del_/PDEL_/del hecho_/NC_/hecho de_/PREP_/de que_/CQUE_/que mi_/PPO_/mi|mío primer_/ORD_/nu|primero amor_/NC_/amor era_/VSfin_/ser una_/ART_/un chica_/NC_/chico ._/FS_/. 
[174]  donde_/ADV_/donde expertos_/ADJ_/experto en_/PREP_/en la_/ART_/el televisión_/NC_/televisión nacional_/ADJ_/nacional equiparaban_/VLfin_/<unknown> nuestro_/ADJ_/nuestro amor_/NC_/amor al_/PAL_/al bestialismo_/NC_/<unknown> 
[175]  Es_/ADV_/<unknown> mi_/PPO_/mi|mío bebé_/NC_/bebé y_/CC_/y está_/VEfin_/estar lleno_/ADJ_/lleno de_/PREP_/de amor_/NC_/amor ._/FS_/. "_/QT_/" 
[176]  Y_/ALFS_/Y es_/VSfin_/ser por_/PREP_/por todas_/QU_/todo esas_/DM_/ese cosas_/NC_/cosa diferentes_/ADJ_/diferente ..._/DOTS_/... el_/ART_/el amor_/NC_/amor a_/PREP_/a mi_/PPO_/mi|mío hijo_/NC_/hijo ._/FS_/. 
[177]  Y_/ALFS_/Y sabemos_/VLfin_/saber que_/CQUE_/que el_/ART_/el amor_/NC_/amor es_/VSfin_/ser lo_/ART_/el suficientemente_/ADV_/suficiente fuerte_/ADJ_/fuerte 
[178]  eso_/DM_/ese es_/VSfin_/ser lo_/ART_/el que_/CQUE_/que puede_/VMfin_/poder transformar_/VLinf_/transformar nuestras_/PPO_/nuestro historias_/NC_/historia en_/PREP_/en historias_/NC_/historia de_/PREP_/de amor_/NC_/amor 
[179]  estaban_/VEfin_/estar interesados_/VLadj_/interesar en_/PREP_/en el_/ART_/el amor_/NC_/amor ,_/CM_/, no_/NEG_/no en_/PREP_/en la_/ART_/el guerra_/NC_/guerra ._/FS_/. 
[180]  Obviamente_/ADV_/obvio ,_/CM_/, Venus_/NP_/Venus es_/VSfin_/ser la_/ART_/el diosa_/NC_/dios del_/PDEL_/del amor_/NC_/amor y_/CC_/y la_/ART_/el fertilidad_/NC_/fertilidad ,_/CM_/, 
[181]  Y_/ALFS_/Y vemos_/VLfin_/ver esto_/DM_/este cuando_/CSUBX_/cuando los_/PPC_/él amantes_/VLfin_/amantar caminan_/VLfin_/caminar por_/PREP_/por la_/ART_/el calle_/NC_/calle ,_/CM_/, 
[182]  cuando_/CSUBX_/cuando el_/ART_/el creyente_/ADJ_/creyente se_/SE_/se siente_/VLfin_/sentar|sentir uno_/CARD_/uno con_/PREP_/con el_/ART_/el amor_/NC_/amor de_/PREP_/de Dios_/NP_/Dios ._/FS_/. 
[183]  Y_/ALFS_/Y muchos_/QU_/mucho de_/PREP_/de nosotros_/PPX_/nosotros sentimos_/VLfin_/sentir el_/ART_/el amor_/NC_/amor 
[184]  cuando_/CSUBX_/cuando los_/PPC_/él amantes_/VLfin_/amantar se_/SE_/se funden_/VLfin_/fundar|fundir mutuamente_/ADV_/mutuo ._/FS_/. 
[185]  emociones_/NC_/emoción positivas_/ADJ_/positivo como_/CSUBX_/como curiosidad_/NC_/curiosidad o_/CC_/o amor_/NC_/amor ,_/CM_/, 
[186]  sino_/CCAD_/sino llenos_/ADJ_/lleno de_/PREP_/de amor_/NC_/amor ,_/CM_/, de_/PREP_/de un_/ART_/un amor_/NC_/amor intenso_/ADJ_/intenso por_/PREP_/por sus_/PPO_/suyo alumnos_/NC_/alumno ._/FS_/. 
[187]  Eso_/ADV_/<unknown> quiere_/VLfin_/querer decir_/VLinf_/decir que_/CQUE_/que combino_/VLfin_/<unknown> mi_/PPO_/mi|mío amor_/NC_/amor por_/PREP_/por las_/ART_/el matemáticas_/NC_/matemáticas y_/CC_/y la_/ART_/el magia_/NC_/magia 
[188]  a_/PREP_/a gente_/NC_/gente que_/CQUE_/que envía_/VLfin_/enviar su_/PPO_/suyo amor_/NC_/amor unos_/ART_/un a_/PREP_/a otros_/QU_/otro ._/FS_/. 
[189]  Voy_/NP_/<unknown> a_/PREP_/a empezar_/VLinf_/empezar con_/PREP_/con mi_/PPO_/mi|mío trabajo_/NC_/trabajo sobre_/PREP_/sobre el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico 
[190]  De_/VLinf_/<unknown> esas_/DM_/ese personas_/NC_/persona ,_/CM_/, 17_/CARD_/@card@ estaban_/VEfin_/estar locamente_/ADV_/loco enamoradas_/VLadj_/enamorar y_/CC_/y correspondidas_/VLadj_/corresponder 
[191]  y_/CC_/y luego_/CSUBF_/luego ahondaré_/VLfin_/ahondar sobre_/PREP_/sobre a_/PREP_/a dónde_/INT_/dónde creo_/VLfin_/crear|creer|creer que_/CQUE_/que se_/SE_/se dirige_/VLfin_/dirigir el_/ART_/el amor_/NC_/amor ._/FS_/. 
[192]  Shakespeare_/NP_/Shakespeare decía_/VLfin_/decir :_/COLON_/: "_/QT_/" ¿_/FS_/? Qué_/INT_/qué es_/VSfin_/ser esto_/DM_/este del_/PDEL_/del amor_/NC_/amor ?_/FS_/? "_/QT_/" 
[193]  Mi_/NP_/<unknown> intención_/NC_/intención inicial_/ADJ_/inicial fue_/VSfin_/ser entender_/VLinf_/entender qué_/INT_/qué era_/VSfin_/ser el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico 
[194]  "_/QT_/" El_/NP_/<unknown> amor_/NC_/amor consiste_/VLfin_/consistir en_/PREP_/en sobrestimar_/VLinf_/<unknown> las_/ART_/el diferencias_/NC_/diferencia entre_/PREP_/entre una_/ART_/un mujer_/NC_/mujer y_/CC_/y otra_/QU_/otro ._/FS_/. "_/QT_/" 
[195]  Como_/NC_/<unknown> dijo_/VLfin_/decir Chaucer_/VLinf_/<unknown> :_/COLON_/: "_/QT_/" El_/ART_/el amor_/NC_/amor es_/VSfin_/ser ciego_/ADJ_/ciego ._/FS_/. "_/QT_/" 
[196]  Al_/ADV_/<unknown> tratar_/VLinf_/tratar de_/CSUBI_/de entender_/VLinf_/entender el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico ,_/CM_/, 
[197]  decidí_/VLfin_/decidir que_/CQUE_/que leería_/VLfin_/leer poesía_/NC_/poesía de_/PREP_/de todo_/QU_/todo el_/ART_/el mundo_/NC_/mundo ,_/CM_/, 
[198]  Simple_/NP_/<unknown> ,_/CM_/, el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico es_/VSfin_/ser muy_/ADV_/muy simple_/ADJ_/simple ._/FS_/. 
[199]  Las_/VLinf_/<unknown> características_/NC_/característica principales_/ADJ_/principal del_/PDEL_/del amor_/NC_/amor romántico_/ADJ_/romántico son_/VSfin_/ser ansia_/NC_/ansia :_/COLON_/: 
[200]  Empecé_/NP_/<unknown> a_/PREP_/a darme_/VCLIinf_/dar cuenta_/NC_/cuenta que_/CQUE_/que el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico no_/NEG_/no es_/VSfin_/ser una_/ART_/un emoción_/NC_/emoción ,_/CM_/, 
[201]  La_/NP_/La gente_/NC_/gente vive_/VLfin_/vivir por_/PREP_/por amor_/NC_/amor ,_/CM_/, mata_/NC_/mata por_/PREP_/por amor_/NC_/amor ,_/CM_/, muere_/VLfin_/morir por_/PREP_/por amor_/NC_/amor ,_/CM_/, 
[202]  El_/VLinf_/<unknown> segundo_/NC_/segundo de_/PREP_/de estos_/DM_/este tres_/CARD_/tres sistemas_/NC_/sistema cerebrales_/ADJ_/cerebral es_/VSfin_/ser el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico :_/COLON_/: 
[203]  esa_/DM_/ese euforia_/NC_/euforia ,_/CM_/, esa_/DM_/ese obsesión_/NC_/obsesión del_/PDEL_/del amor_/NC_/amor fresco_/ADJ_/fresco ._/FS_/. 
[204]  Creo_/VLfin_/crear|creer|creer que_/CQUE_/que el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico evolucionó_/VLfin_/evolucionar para_/CSUBI_/para permitirte_/VLinf_/<unknown> enfocar_/VLinf_/enfocar tu_/PPO_/tú energía_/NC_/energía de_/PREP_/de apareamiento_/NC_/apareamiento 
[205]  la_/ART_/el lujuria_/NC_/lujuria ,_/CM_/, el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico y_/CC_/y el_/ART_/el apego_/NC_/apego profundo_/ADJ_/profundo a_/PREP_/a una_/ART_/un pareja_/NC_/pareja ._/FS_/. 
[206]  aquí_/ADV_/aquí sólo_/ADV_/sólo diré_/VLfin_/decir un_/ART_/un par_/NC_/par de_/PREP_/de cosas_/NC_/cosa ,_/CM_/, y_/CC_/y pasaré_/VLfin_/pasar al_/PAL_/al sexo_/NC_/sexo y_/CC_/y al_/PAL_/al amor_/NC_/amor ._/FS_/. 
[207]  Mencionaré_/VLfin_/mencionar unas_/ART_/un cuantas_/REL_/cuanto para_/CSUBI_/para continuar_/VLinf_/continuar con_/PREP_/con el_/ART_/el sexo_/NC_/sexo y_/CC_/y el_/ART_/el amor_/NC_/amor ._/FS_/. 
[208]  impacto_/NC_/impacto en_/PREP_/en el_/ART_/el sexo_/NC_/sexo ,_/CM_/, el_/ART_/el romance_/NC_/romance y_/CC_/y la_/ART_/el vida_/NC_/vida familiar_/ADJ_/familiar ._/FS_/. 
[209]  También_/ADV_/también estamos_/VEfin_/estar viendo_/VLger_/ver un_/ART_/un crecimiento_/NC_/crecimiento en_/PREP_/en el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico ,_/CM_/, 
[210]  Estos_/NP_/<unknown> tres_/CARD_/tres sistemas_/NC_/sistema cerebrales_/ADJ_/cerebral :_/COLON_/: lujuria_/NC_/lujuria ,_/CM_/, amor_/NC_/amor romántico_/ADJ_/romántico y_/CC_/y apego_/NC_/apego 
[211]  la_/ART_/el dopamina_/NC_/dopamina está_/VEfin_/estar asociada_/VLadj_/asociar con_/PREP_/con el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico ,_/CM_/, 
[212]  Pero_/NP_/<unknown> estos_/DM_/este tres_/CARD_/tres sistemas_/NC_/sistema cerebrales_/ADJ_/cerebral :_/COLON_/: lujuria_/NC_/lujuria ,_/CM_/, amor_/NC_/amor romántico_/ADJ_/romántico y_/CC_/y apego_/NC_/apego 
[213]  y_/CC_/y sentir_/VLinf_/sentir un_/ART_/un intenso_/ADJ_/intenso amor_/NC_/amor romántico_/ADJ_/romántico por_/PREP_/por otra_/QU_/otro persona_/NC_/persona ,_/CM_/, 
[214]  a_/PREP_/a sentimientos_/NC_/sentimiento profundos_/ADJ_/profundo de_/PREP_/de amor_/NC_/amor romántico_/ADJ_/romántico por_/PREP_/por otra_/QU_/otro ._/FS_/. 
[215]  la_/ART_/el dopamina_/NC_/dopamina está_/VEfin_/estar asociada_/VLadj_/asociar al_/PAL_/al amor_/NC_/amor romántico_/ADJ_/romántico ._/FS_/. 
[216]  Sólo_/NC_/<unknown> quiero_/VLfin_/querer decir_/VLinf_/decir que_/CQUE_/que un_/ART_/un mundo_/NC_/mundo sin_/PREP_/sin amor_/NC_/amor es_/VSfin_/ser una_/ART_/un lugar_/NC_/lugar muerto_/ADJ_/muerto ._/FS_/. 
[217]  He_/VLinf_/<unknown> estado_/NC_/estado estudiando_/VLger_/estudiar el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico ,_/CM_/, el_/ART_/el sexo_/NC_/sexo y_/CC_/y el_/ART_/el apego_/NC_/apego durante_/PREP_/durante 30_/CARD_/@card@ años_/NC_/año ._/FS_/. 
[218]  Te_/NP_/<unknown> enamoras_/VLfin_/<unknown> de_/PREP_/de alguien_/QU_/alguien que_/CQUE_/que se_/SE_/se ubica_/VLfin_/ubicar dentro de_/PREP_/dentro~de lo_/ART_/el que_/CQUE_/que llamo_/VLfin_/llamar "_/QT_/" el_/ART_/el mapa_/NC_/mapa del_/PDEL_/del amor_/NC_/amor ,_/CM_/, "_/QT_/" 
[219]  He_/NC_/<unknown> tratado_/VLadj_/tratar hasta ahora_/ADV_/hasta~ahora sobre_/PREP_/sobre la_/ART_/el biología_/NC_/biología del_/PDEL_/del amor_/NC_/amor ,_/CM_/, 
[220]  y_/CC_/y quizá_/ADV_/quizá disparar_/VLinf_/disparar el_/ART_/el sistema_/NC_/sistema cerebral_/ADJ_/cerebral del_/PDEL_/del amor_/NC_/amor romántico_/ADJ_/romántico (_/LP_/( risas_/NC_/risa )_/RP_/) 
[221]  ¡_/FS_/! La_/ART_/el magia_/NC_/magia del_/PDEL_/del amor_/NC_/amor !_/FS_/! 
[222]  el_/ART_/el impulso_/NC_/impulso sexual_/ADJ_/sexual ,_/CM_/, el_/ART_/el amor_/NC_/amor romántico_/ADJ_/romántico y_/CC_/y el_/ART_/el apego_/NC_/apego a_/PREP_/a una_/ART_/un pareja_/NC_/pareja de_/PREP_/de largo_/ADJ_/largo plazo_/NC_/plazo ._/FS_/. 
[223]  en_/PREP_/en otras_/QU_/otro palabras_/NC_/palabra ,_/CM_/, siendo_/VSger_/ser la_/ART_/el raíz_/NC_/raíz de_/PREP_/de la_/ART_/el palabra_/NC_/palabra el_/ART_/el amor_/NC_/amor y_/CC_/y la_/ART_/el pasión_/NC_/pasión ._/FS_/. 
[224]  Mi_/NP_/<unknown> amor_/NC_/amor es_/VSfin_/ser como_/ADV_/como doce_/CARD_/doce cabras_/NC_/cabra etíopes_/ADJ_/etíope 
[225]  No_/ADV_/<unknown> porque_/CSUBX_/porque estuviera_/VEfin_/estar encontrando_/VLger_/encontrar amor_/NC_/amor y_/CC_/y sexo_/NC_/sexo homosexual_/ADJ_/homosexual 
[226]  "_/QT_/" Aquí_/NP_/<unknown> está_/VEfin_/estar el_/ART_/el amor_/NC_/amor ,_/CM_/, todo_/QU_/todo el_/ART_/el amor_/NC_/amor "_/QT_/" ._/FS_/. 
[227]  No_/ADV_/<unknown> accederíamos_/VLfin_/acceder al_/PAL_/al amor_/NC_/amor 
[228]  "_/QT_/" ¡_/FS_/! No_/NEG_/no ,_/CM_/, no_/NEG_/no ,_/CM_/, no_/NEG_/no !_/FS_/! ¡_/FS_/! Debería_/VMfin_/deber ser_/VSinf_/ser un_/ART_/un sapo_/NC_/sapo !_/FS_/! "_/QT_/" 
[229]  "_/QT_/" No_/NP_/<unknown> ._/FS_/. ¡_/FS_/! Debería_/VMfin_/deber ser_/VSinf_/ser el_/ART_/el Inspector_/NC_/inspector Gadget_/NP_/<unknown> !_/FS_/! "_/QT_/" 
[230]  donde_/ADV_/donde cosas_/NC_/cosa pequeñas_/ADJ_/pequeño son_/VSfin_/ser hechas_/VLadj_/hacer por_/PREP_/por amor_/NC_/amor 
[231]  De_/NC_/<unknown> repente_/ADV_/repente grandes_/ADJ_/grande cosas_/NC_/cosa pueden_/VMfin_/poder ser_/VSinf_/ser hechas_/VLadj_/hacer por_/PREP_/por amor_/NC_/amor ._/FS_/. "_/QT_/" 
[232]  Ahora_/ADV_/ahora ,_/CM_/, todas_/QU_/todo estas_/DM_/este grandes_/ADJ_/grande cosas_/NC_/cosa de_/PREP_/de experimentos_/NC_/experimento de_/PREP_/de amor_/NC_/amor 
[233]  “_/QT_/“ Querida_/NP_/<unknown> madre_/NC_/madre biológica_/ADJ_/biológico :_/COLON_/: Tengo_/VLfin_/tener padres_/NC_/padre excelentes_/ADJ_/excelente ._/FS_/. 
[234]  Encontré_/VLfin_/encontrar amor_/NC_/amor ._/FS_/. Soy_/VSfin_/ser feliz_/ADJ_/feliz ._/FS_/. "_/QT_/" 
[235]  amor_/NC_/amor y_/CC_/y admiración_/NC_/admiración hacia_/PREP_/hacia él_/PPX_/él ._/FS_/. 
[236]  Y_/ALFS_/Y creo_/VLfin_/crear|creer|creer que_/CQUE_/que no_/NEG_/no hay_/VHfin_/haber amor_/NC_/amor o_/CC_/o admiración_/NC_/admiración viniendo_/VLger_/venir del_/PDEL_/del lado_/NC_/lado izquierdo_/ADJ_/izquierdo ._/FS_/. (_/LP_/( Risas_/NC_/risa )_/RP_/) 
[237]  es_/VSfin_/ser otro_/QU_/otro tipo_/NC_/tipo de_/PREP_/de historia_/NC_/historia de_/PREP_/de amor_/NC_/amor ._/FS_/. 
[238]  porque_/CSUBX_/porque esta_/DM_/este historia_/NC_/historia de_/PREP_/de amor_/NC_/amor ,_/CM_/, 
[239]  sobrellevaban_/VLfin_/<unknown> la_/ART_/el pérdida_/NC_/pérdida de_/PREP_/de su_/PPO_/suyo ser_/NC_/ser querido_/ADJ_/querido ?_/FS_/? 
[240]  como_/CSUBX_/como parte_/NC_/parte del_/PDEL_/del amor_/NC_/amor moderno_/ADJ_/moderno y_/CC_/y las_/ART_/el sociedades_/NC_/sociedad individualistas_/ADJ_/individualista ._/FS_/. 
[241]  ¿_/FS_/? Cuál_/INT_/cuál es_/VSfin_/ser la_/ART_/el relación_/NC_/relación entre_/PREP_/entre amor_/NC_/amor y_/CC_/y deseo_/NC_/deseo ?_/FS_/? 
[242]  Si_/NP_/<unknown> hay_/VHfin_/haber un_/ART_/un verbo_/NC_/verbo ,_/CM_/, para_/PREP_/para mí_/PPX_/yo ,_/CM_/, que_/CQUE_/que acompañe_/VLfin_/acompañar a_/PREP_/a amor_/NC_/amor es_/VSfin_/ser "_/QT_/" tener_/VLinf_/tener "_/QT_/" ._/FS_/. 
[243]  En_/VLfin_/<unknown> el_/ART_/el amor_/NC_/amor ,_/CM_/, queremos_/VLfin_/querer tener_/VLinf_/tener ,_/CM_/, queremos_/VLfin_/querer conocer_/VLinf_/conocer lo_/ART_/el amado_/VLadj_/amar ._/FS_/. 
[244]  El_/NC_/<unknown> cuidado_/ADJ_/cuidado es_/VSfin_/ser muy_/ADV_/muy amoroso_/ADJ_/amoroso ._/FS_/. Es_/VSfin_/ser un_/ART_/un potente_/ADJ_/potente antiafrodisiaco_/NC_/<unknown> ._/FS_/. 
[245]  Ahora_/ADV_/ahora ,_/CM_/, en_/PREP_/en esta_/DM_/este paradoja_/NC_/paradoja entre_/PREP_/entre el_/ART_/el amor_/NC_/amor y_/CC_/y el_/ART_/el deseo_/NC_/deseo ,_/CM_/, 
[246]  que_/CQUE_/que nutren_/VLfin_/nutrir el_/ART_/el amor_/NC_/amor —_/DASH_/-- mutualismo_/NC_/mutualismo ,_/CM_/, reciprocidad_/NC_/reciprocidad ,_/CM_/, 
[247]  que_/CQUE_/que no_/NEG_/no siempre_/ADV_/siempre favorecen_/VLfin_/favorecer el_/ART_/el amor_/NC_/amor :_/COLON_/: 
[248]  porque_/CSUBX_/porque pensamos_/VLfin_/pensar que_/CQUE_/que el_/ART_/el amor_/NC_/amor viene_/VLfin_/venir con_/PREP_/con abnegación_/NC_/abnegación ,_/CM_/, 
[249]  que_/CQUE_/que vendrá_/VLfin_/venir cargada_/NC_/cargada de_/PREP_/de preocupación_/NC_/preocupación extra_/ADJ_/extra ,_/CM_/, 
[250]  Las_/VLinf_/<unknown> parejas_/NC_/pareja eróticas_/ADJ_/erótico también_/ADV_/también entiendan_/VLfin_/entender que_/CQUE_/que la_/ART_/el pasión_/NC_/pasión aumenta_/VLfin_/aumentar y_/CC_/y disminuye_/VLfin_/disminuir ._/FS_/. 
[251]  vas_/VLfin_/ir a_/PREP_/a sentir_/VLinf_/sentir que_/CQUE_/que te_/PPX_/tú invade_/VLfin_/invadir una_/ART_/un sensación_/NC_/sensación de_/PREP_/de amor_/NC_/amor 
[252]  y_/CC_/y yo_/PPX_/yo estaba_/VEfin_/estar preparado_/VLadj_/preparar para_/PREP_/para esta_/DM_/este avalancha_/NC_/avalancha de_/PREP_/de amor_/NC_/amor 
[253]  Me_/NP_/yo sentí_/VLfin_/sentir abrumado_/VLadj_/abrumar de_/PREP_/de amor_/NC_/amor y_/CC_/y afecto_/NC_/afecto por_/PREP_/por mi_/PPO_/mi|mío esposa_/NC_/esposo 
[254]  Hemos_/VLfin_/haber graficado_/NC_/<unknown> 
[255]  No_/NP_/<unknown> está_/VEfin_/estar permitido_/VLadj_/permitir graficar_/VLinf_/<unknown> el_/ART_/el amor_/NC_/amor ._/FS_/. 
[256]  Y_/ALFS_/Y la_/ART_/el razón_/NC_/razón por_/PREP_/por la_/ART_/el que_/CQUE_/que no_/NEG_/no podemos_/VLfin_/podar|poder hacerlo_/VCLIinf_/hacer 
[257]  es_/VSfin_/ser porque_/CSUBX_/porque pensamos_/VLfin_/pensar en_/PREP_/en el_/ART_/el amor_/NC_/amor como_/CSUBX_/como algo_/QU_/algo binario_/ADJ_/binario ._/FS_/. 
[258]  Y_/ALFS_/Y yo_/PPX_/yo creo_/VLfin_/crear|creer|creer que_/CQUE_/que en realidad_/ADV_/en~realidad el_/ART_/el amor_/NC_/amor es_/VSfin_/ser un_/ART_/un proceso_/NC_/proceso ._/FS_/. 
[259]  como_/CSUBX_/como algo_/QU_/algo binario_/ADJ_/binario 
[260]  por_/PREP_/por un_/ART_/un amor_/NC_/amor fraudulento_/ADJ_/fraudulento ,_/CM_/, o_/CC_/o inadecuado_/ADJ_/inadecuado ,_/CM_/, o_/CC_/o lo_/ART_/el que_/CQUE_/que sea_/VSfin_/ser ._/FS_/. 
[261]  Había_/VLfin_/haber una_/ART_/un buena_/ADJ_/bueno correlación_/NC_/correlación entre_/PREP_/entre el_/ART_/el gusto_/NC_/gusto por_/PREP_/por el_/ART_/el Lego_/NP_/<unknown> 
[262]  No_/ADV_/<unknown> existía_/VLfin_/existir relación_/NC_/relación entre_/PREP_/entre el_/ART_/el gusto_/NC_/gusto por_/PREP_/por el_/ART_/el Lego_/NP_/<unknown> y_/CC_/y las_/ART_/el cantidades_/NC_/cantidad armadas_/VLadj_/armar ,_/CM_/, 
[263]  a punto de_/PREP_/a~punto~de casarse_/VCLIinf_/casarse ._/FS_/. Ella_/PPX_/él era_/VSfin_/ser el_/ART_/el amor_/NC_/amor de_/PREP_/de su_/PPO_/suyo vida_/NC_/vida ._/FS_/. 
[264]  "_/QT_/" ¿_/FS_/? De_/PREP_/de vacaciones_/NC_/vacación ,_/CM_/, señora_/NC_/señora ?_/FS_/? "_/QT_/" ._/FS_/. 
[265]  hecha_/VLadj_/hacer con_/PREP_/con amor_/NC_/amor ,_/CM_/, por lo general_/ADV_/por~lo~general ._/FS_/. 
[266]  "_/QT_/" De_/VLfin_/<unknown> 52_/CARD_/@card@ a_/PREP_/a 48_/CARD_/@card@ con_/PREP_/con amor_/NC_/amor "_/QT_/" 
[267]  "_/QT_/" Querido_/NP_/<unknown> 48_/CARD_/@card@ ,_/CM_/, prometo_/VLfin_/prometer escucharte_/VCLIinf_/escuchar ,_/CM_/, luchar_/VLinf_/luchar por_/PREP_/por ti_/PPX_/tú ,_/CM_/, respetarte_/VLfin_/<unknown> siempre_/ADV_/siempre "_/QT_/" ._/FS_/. 
[268]  Estoy_/NP_/<unknown> feliz_/ADJ_/feliz de_/CSUBI_/de compartir_/VLinf_/compartir amor_/NC_/amor con_/PREP_/con el_/ART_/el mundo_/NC_/mundo ,_/CM_/, 
[269]  "_/QT_/" No_/ADV_/<unknown> te_/PPX_/tú pongas_/VLfin_/poner ropa_/NC_/ropa interior_/ADJ_/interior bajo_/PREP_/bajo la_/ART_/el pijama_/NC_/pijama ,_/CM_/, 


