Language: de.tag

[1]  lass_/ADJD_/laß uns_/PPER_/wir Liebende_/NN_/Liebende und_/KON_/und Geliebte_/NN_/Geliebte sein_/VAINF_/sein ;_/$._/; 
[2]  dies_/PDS_/dies für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe taten_/VVFIN_/tun und_/KON_/und nicht_/PTKNEG_/nicht für_/APPR_/für den_/ART_/die Kampf_/NN_/Kampf ._/$._/. 
[3]  lass_/ADJD_/laß uns_/PPER_/wir Liebende_/NN_/Liebende und_/KON_/und Geliebte_/NN_/Geliebte sein_/VAINF_/sein ;_/$._/; 
[4]  dies_/PDS_/dies für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe taten_/VVFIN_/tun und_/KON_/und nicht_/PTKNEG_/nicht für_/APPR_/für den_/ART_/die Kampf_/NN_/Kampf ._/$._/. 
[5]  aber_/KON_/aber seine_/PPOSAT_/sein Liebe_/NN_/Liebe zur_/APPRART_/zu Musik_/NN_/Musik aufgegeben_/VVPP_/aufgeben hatte_/VAFIN_/haben ,_/$,_/, um_/KOUI_/um eine_/ART_/eine Karriere_/NN_/Karriere 
[6]  und_/KON_/und das_/ART_/die Wort_/NN_/Wort ,_/$,_/, meine_/PPOSAT_/mein Liebe_/NN_/Liebe ,_/$,_/, wird_/VAFIN_/werden "_/$(_/" basingstoke_/ADJD_/<unknown> "_/$(_/" sein_/VAINF_/sein ._/$._/. 
[7]  dann_/ADV_/dann erwarten_/VVFIN_/erwarten wir_/PPER_/wir nicht_/PTKNEG_/nicht ,_/$,_/, Liebe_/NN_/Liebe zu_/PTKZU_/zu finden_/VVINF_/finden ,_/$,_/, gesund_/ADJD_/gesund und_/KON_/und erfolgreich_/ADJD_/erfolgreich zu_/PTKZU_/zu sein_/VAINF_/sein ,_/$,_/, 
[8]  Liebe_/NN_/Liebe ,_/$,_/, Wertvorstellungen_/NN_/Wertvorstellung 
[9]  Und_/KON_/und das_/ART_/die alles_/PIS_/alle verpackt_/VVPP_/verpacken in_/APPR_/in die_/ART_/die bedingungslose_/ADJA_/bedingungslos Liebe_/NN_/Liebe ,_/$,_/, 
[10]  dies_/PDS_/dies ist_/VAFIN_/sein die_/ART_/die Liebe_/NN_/Liebe meines_/PPOSAT_/mein Lebens_/NN_/Leben ,_/$,_/, meine_/PPOSAT_/mein Tochter_/NN_/Tochter Jay_/NE_/Jay ._/$._/. 
[11]  Der_/ART_/die Dichter_/NN_/Dichter W._/APPR_/w. H._/NE_/H. Auden_/NE_/<unknown> hat_/VAFIN_/haben geschrieben_/VVPP_/schreiben :_/$._/: "_/$(_/" Tausende_/NN_/Tausend haben_/VAFIN_/haben ohne_/APPR_/ohne Liebe_/NN_/Liebe gelebt_/VVPP_/leben ._/$._/. 
[12]  Und_/KON_/und ein_/ART_/eine Letztes_/NN_/Letzte :_/$._/: Wenn_/KOUS_/wenn man_/PIS_/man Dingen_/NN_/Ding wie_/KOKOM_/wie Gesundheit_/NN_/Gesundheit ,_/$,_/, Liebe_/NN_/Liebe ,_/$,_/, Sex_/NN_/Sex und_/KON_/und 
[13]  über_/APPR_/über ihre_/PPOSAT_/ihr Liebenden_/ADJA_/liebend 
[14]  desto_/KON_/desto mehr_/ADV_/mehr denke_/VVFIN_/denken ich_/PPER_/ich ,_/$,_/, dass_/KOUS_/dass das_/PDS_/die wirklich_/ADJD_/wirklich eine_/ART_/eine Art_/NN_/Art von_/APPR_/von Liebe_/NN_/Liebe ist_/VAFIN_/sein ._/$._/. 
[15]  Und_/KON_/und meine_/PPOSAT_/mein Liebe_/NN_/Liebe zu_/APPR_/zu Science_/NN_/<unknown> Fiction_/NN_/<unknown> 
[16]  Und_/KON_/und meine_/PPOSAT_/mein Liebe_/NN_/Liebe zum_/APPRART_/zu Ozean_/NN_/Ozean hält_/VVFIN_/halten an_/PTKVZ_/an ,_/$,_/, 
[17]  Es_/NN_/Es wurde_/VAFIN_/werden ein_/ART_/eine Liebesfilm_/NN_/Liebesfilm epischen_/ADJA_/episch Ausmaßes_/NN_/Ausmaß ,_/$,_/, 
[18]  auch_/ADV_/auch die_/ART_/die Liebe_/NN_/Liebe selbst_/ADV_/selbst zu_/PTKZU_/zu widerlegen_/VVINF_/widerlegen --_/$(_/-- 
[19]  Würden_/NN_/Würde wir_/PPER_/wir das_/PDS_/die unseren_/PPOSAT_/unser Lieben_/NN_/Liebe|Lieben einsetzen_/VVFIN_/einsetzen ,_/$,_/, unseren_/PPOSAT_/unser eigenen_/ADJA_/eigen Kindern_/NN_/Kind ,_/$,_/, 
[20]  aber_/KON_/aber er_/PPER_/er hatte_/VAFIN_/haben eine_/ART_/eine Freundin_/NN_/Freundin in_/APPR_/in England_/NE_/England ._/$._/. Die_/ART_/die Liebe_/NN_/Liebe seines_/PPOSAT_/sein Lebens_/NN_/Leben ,_/$,_/, Sarah_/NE_/Sarah ._/$._/. 
[21]  Die_/ART_/die kleine_/ADJA_/klein Geste_/NN_/Geste der_/ART_/die Liebe_/NN_/Liebe meines_/PPOSAT_/mein Pflegevaters_/NN_/Pflegevater 
[22]  sogar_/ADV_/sogar durch_/APPR_/durch die_/ART_/die allerkleinste_/ADJA_/allerklein Geste_/NN_/Geste der_/ART_/die Liebe_/NN_/Liebe ._/$._/. 
[23]  Ihre_/PPOSAT_/ihr Geste_/NN_/Geste der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Fürsorge_/NN_/Fürsorge 
[24]  dass_/KOUS_/dass meine_/PPOSAT_/mein Liebe_/NN_/Liebe zu_/APPR_/zu Autos_/NN_/Auto und_/KON_/und LKWs_/NN_/<unknown> jemals_/ADV_/jemals 
[25]  Es_/NN_/Es war_/VAFIN_/sein ein_/ART_/eine Sommer_/NN_/Sommer der_/ART_/die Liebe_/NN_/Liebe ,_/$,_/, wenn_/KOUS_/wenn man_/PIS_/man weiß_/ADJD_/weiß war_/VAFIN_/sein ._/$._/. 
[26]  Meine_/PPOSAT_/mein Pflegemutter_/NN_/Pflegemutter bat_/VVFIN_/bitten mich_/PRF_/ich ,_/$,_/, über_/APPR_/über Liebe_/NN_/Liebe und_/KON_/und ihre_/PPOSAT_/ihr Bedeutung_/NN_/Bedeutung 
[27]  von_/APPR_/von der_/ART_/die Suche_/NN_/Suche nach_/APPR_/nach Liebe_/NN_/Liebe war_/VAFIN_/sein ,_/$,_/, ein_/ART_/eine Ort_/NN_/Ort ,_/$,_/, wo_/PWAV_/wo Störungen_/NN_/Störung 
[28]  Friede_/NN_/Friede und_/KON_/und Liebe_/NN_/Liebe wurden_/VAFIN_/werden erwähnt_/VVPP_/erwähnen ._/$._/. 
[29]  CA_/NE_/<unknown> :_/$._/: Friede_/NN_/Friede und_/KON_/und Liebe_/NN_/Liebe wurden_/VAFIN_/werden erwähnt_/VVPP_/erwähnen ._/$._/. 
[30]  Mapendo_/NN_/<unknown> heißt_/VVFIN_/heißen auf_/APPR_/auf Suaheli_/NE_/<unknown> „_/ADJA_/<unknown> große_/ADJA_/groß Liebe_/NN_/Liebe “_/ADJD_/<unknown> ._/$._/. 
[31]  "_/$(_/" Liebe_/NN_/Liebe und_/KON_/und Mitgefühl_/NN_/Mitgefühl sind_/VAFIN_/sein Notwendigkeiten_/NN_/Notwendigkeit ._/$._/. 
[32]  in_/APPR_/in den_/ART_/die Buddha_/NN_/Buddha der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Güte_/NN_/Güte ._/$._/. 
[33]  Es_/NN_/Es war_/VAFIN_/sein Energie_/NN_/Energie ,_/$,_/, Liebe_/NN_/Liebe und_/KON_/und Freude_/NN_/Freude ._/$._/. 
[34]  sind_/VAFIN_/sein Liebe_/NN_/Liebe und_/KON_/und Arbeit_/NN_/Arbeit ._/$._/. 
[35]  Liebe_/NN_/Liebe :_/$._/: Erfolgreich_/ADJD_/erfolgreich 
[36]  Die_/ART_/die Luft_/NN_/Luft ist_/VAFIN_/sein von_/APPR_/von Liebe_/NN_/Liebe erfüllt_/VVPP_/erfüllen ._/$._/. 
[37]  Es_/NN_/Es ist_/VAFIN_/sein auch_/ADV_/auch meine_/PPOSAT_/mein große_/ADJA_/groß ,_/$,_/, lebenslange_/ADJA_/lebenslang Liebe_/NN_/Liebe und_/KON_/und Faszination_/NN_/Faszination ._/$._/. 
[38]  einfach_/ADJD_/einfach dafür_/PAV_/dafür ,_/$,_/, dass_/KOUS_/dass Du_/PPER_/du die_/ART_/die bloße_/ADJA_/bloß menschliche_/ADJA_/menschlich Liebe_/NN_/Liebe und_/KON_/und den_/ART_/die Eigensinn_/NN_/Eigensinn hast_/VAFIN_/haben ,_/$,_/, 
[39]  Dieses_/PDAT_/dies Projekt_/NN_/Projekt heisst_/VVFIN_/heißen "_/$(_/" Liebesnest_/NN_/Liebesnest "_/$(_/" ._/$._/. 
[40]  Diese_/PDAT_/dies Werk_/NN_/Werk heisst_/VVFIN_/heißen "_/$(_/" Liebesschloss_/NN_/<unknown> Nest_/NN_/Nest "_/$(_/" 
[41]  Dieses_/PDAT_/dies trägt_/VVFIN_/tragen den_/ART_/die Titel_/NN_/Titel "_/$(_/" Mixtape_/NN_/<unknown> Liebes_/ADJA_/lieb Lieder_/NN_/Lied Nest_/NN_/Nest "_/$(_/" ._/$._/. 
[42]  und_/KON_/und dieses_/PDAT_/dies "_/$(_/" Liebe_/ADJA_/lieb Machen_/NN_/Machen Nest_/NN_/Nest "_/$(_/" 
[43]  Vor_/APPR_/vor allem_/PIS_/alle lernte_/VVFIN_/lernen ich_/PPER_/ich Liebe_/NN_/Liebe kennen_/VVINF_/kennen ._/$._/. 
[44]  Liebe_/NN_/Liebe ist_/VAFIN_/sein etwas_/ADV_/etwas ,_/$,_/, das_/PRELS_/die Sie_/PPER_/Sie|sie entdecken_/VVFIN_/entdecken ._/$._/. 
[45]  Es_/NN_/Es ist_/VAFIN_/sein zuerst_/ADV_/zuerst Liebe_/NN_/Liebe ._/$._/. 
[46]  was_/PIS_/was wir_/PPER_/wir Liebe_/NN_/Liebe nennen_/VVINF_/nennen ._/$._/. 
[47]  und_/KON_/und die_/ART_/die Fürsorge_/NN_/Fürsorge ,_/$,_/, die_/ART_/die Liebe_/NN_/Liebe und_/KON_/und den_/ART_/die Respekt_/NN_/Respekt ,_/$,_/, 
[48]  und_/KON_/und manchmal_/ADV_/manchmal sogar_/ADV_/sogar Elementen_/NN_/Element von_/APPR_/von Liebe_/NN_/Liebe ._/$._/. 
[49]  "_/$(_/" Lieber_/NN_/Liebe ein_/ART_/eine ausgefülltes_/ADJA_/ausgefüllt 
[50]  Mitgefühl_/NN_/Mitgefühl und_/KON_/und Liebe_/NN_/Liebe ,_/$,_/, 
[51]  Liebe_/NN_/Liebe ,_/$,_/, Mitgefühl_/NN_/Mitgefühl ,_/$,_/, Phantasie_/NN_/Phantasie ,_/$,_/, 
[52]  und_/KON_/und Gemeinschaft_/NN_/Gemeinschaft und_/KON_/und Liebe_/NN_/Liebe ._/$._/. 
[53]  Anonymous_/NN_/<unknown> :_/$._/: Liebe_/ADJA_/lieb Fox_/NN_/<unknown> News_/NN_/<unknown> ,_/$,_/, 
[54]  Ich_/PPER_/ich möchte_/VMFIN_/mögen Ihnen_/PPER_/Sie|sie von_/APPR_/von verrückter_/ADJA_/verrückt Liebe_/NN_/Liebe erzählen_/VVINF_/erzählen ,_/$,_/, 
[55]  einer_/ART_/eine psychologischen_/ADJA_/psychologisch Falle_/NN_/Falle ,_/$,_/, getarnt_/ADJD_/getarnt als_/KOKOM_/als Liebe_/NN_/Liebe ,_/$,_/, 
[56]  Ich_/PPER_/ich war_/VAFIN_/sein in_/APPR_/in der_/ART_/die Lage_/NN_/Lage ,_/$,_/, meine_/PPOSAT_/mein eigene_/ADJA_/eigen Geschichte_/NN_/Geschichte verrückter_/ADJA_/verrückt Liebe_/NN_/Liebe zu_/PTKZU_/zu beenden_/VVINF_/beenden ,_/$,_/, 
[57]  ein_/ART_/eine Liebesgedicht_/NN_/Liebesgedicht ,_/$,_/, anderes_/PIS_/andere als_/KOKOM_/als ich_/PPER_/ich je_/APPR_/je eins_/CARD_/eins gehört_/VVPP_/gehören|hören hatte_/VAFIN_/haben ._/$._/. 
[58]  das_/ART_/die Versprechen_/NN_/Versprechen von_/APPR_/von Sex_/NN_/Sex und_/KON_/und Liebe_/NN_/Liebe ist_/VAFIN_/sein ._/$._/. 
[59]  aberLiebe_/NN_/<unknown> war_/VAFIN_/sein nicht_/PTKNEG_/nicht darunter_/PTKVZ_/darunter !_/$._/! 
[60]  ihr_/PPOSAT_/ihr Name_/NN_/Name bedeutet_/VVFIN_/bedeuten Liebe_/NN_/Liebe --_/$(_/-- 
[61]  und_/KON_/und Liebe_/NN_/Liebe und_/KON_/und Mitleid_/NN_/Mitleid führen_/VVFIN_/fahren|führen zu_/APPR_/zu Respekt_/NN_/Respekt vor_/APPR_/vor allen_/PIAT_/alle Lebensformen_/NN_/Lebensform ._/$._/. 
[62]  so_/ADV_/so viel_/PIAT_/viele Liebe_/NN_/Liebe und_/KON_/und Mitgefühl_/NN_/Mitgefühl ._/$._/. 
[63]  den_/ART_/die besten_/ADJA_/gut Rechner_/NN_/Rechner hast_/VAFIN_/haben du_/PPER_/du gegegeben_/ADJD_/<unknown> ._/$._/. Du_/PPER_/du hast_/VAFIN_/haben ihnen_/PPER_/sie Liebe_/NN_/Liebe gegeben_/VVPP_/geben ,_/$,_/, 
[64]  den_/ART_/die Rest_/NN_/Rest ihres_/PPOSAT_/ihr Lebens_/NN_/Leben mit_/APPR_/mit all_/PIAT_/alle dieser_/PDAT_/dies Liebe_/NN_/Liebe ,_/$,_/, Ausbildung_/NN_/Ausbildung ,_/$,_/, Geld_/NN_/Geld 
[65]  und_/KON_/und dort_/ADV_/dort die_/ART_/die Liebe_/NN_/Liebe seines_/PPOSAT_/sein Lebens_/NN_/Leben kennenlernt_/VVFIN_/kennenlernen ._/$._/. 
[66]  Das_/PDS_/die ist_/VAFIN_/sein es_/PPER_/es ,_/$,_/, was_/PWS_/was wir_/PPER_/wir wirklich_/ADJD_/wirklich brauchen_/VVFIN_/brauchen :_/$._/: Beziehung_/NN_/Beziehung und_/KON_/und Liebe_/NN_/Liebe -_/$(_/- das_/ART_/die vierte_/ADJA_/viert Bedürfnis_/NN_/Bedürfnis ._/$._/. 
[67]  weil_/KOUS_/weil Liebe_/NN_/Liebe zu_/APPR_/zu viel_/PIAT_/viele Angst_/NN_/Angst macht_/VVFIN_/machen ._/$._/. Nicht_/PTKNEG_/nicht verletzt_/VVPP_/verletzen werden_/VAINF_/werden wollen_/VMFIN_/wollen ._/$._/. 
[68]  Liebe_/NN_/Liebe ?_/$._/? Wir_/PPER_/wir alle_/PIS_/alle brauchen_/VVFIN_/brauchen alle_/PIAT_/alle sechs_/CARD_/sechs ,_/$,_/, aber_/KON_/aber was_/PWS_/was auch_/ADV_/auch immer_/ADV_/immer 
[69]  Jeder_/PIS_/jede von_/APPR_/von ihnen_/PPER_/sie ist_/VAFIN_/sein in_/APPR_/in einer_/ART_/eine Liebesbeziehung_/NN_/Liebesbeziehung 
[70]  ein_/ART_/eine Leben_/NN_/Leben in_/APPR_/in Arbeit_/NN_/Arbeit ,_/$,_/, Elternschaft_/NN_/Elternschaft ,_/$,_/, Liebe_/NN_/Liebe ,_/$,_/, Freizeit_/NN_/Freizeit ,_/$,_/, die_/ART_/die Zeit_/NN_/Zeit bleibt_/VVFIN_/bleiben für_/APPR_/für Sie_/PPER_/Sie|sie stehen_/VVINF_/stehen ._/$._/. 
[71]  Aber_/KON_/aber auf_/APPR_/auf dem_/ART_/die dritten_/ADJA_/dritt großen_/ADJA_/groß Schauplatz_/NN_/Schauplatz des_/ART_/die Lebens_/NN_/Leben ,_/$,_/, der_/ART_/die Liebe_/NN_/Liebe ,_/$,_/, ist_/VAFIN_/sein Len_/NE_/Len ein_/ART_/eine Totalversager_/NN_/<unknown> ._/$._/. 
[72]  Seine_/PPOSAT_/sein Arbeit_/NN_/Arbeit umzugestalten_/VVIZU_/umgestalten ,_/$,_/, seine_/PPOSAT_/sein Liebe_/NN_/Liebe ,_/$,_/, 
[73]  Erez_/NE_/<unknown> Lieberman_/NE_/<unknown> Aiden_/NN_/<unknown> :_/$._/: Jeder_/PIS_/jede weiß_/VVFIN_/wissen ,_/$,_/, 
[74]  ihrer_/PPOSAT_/ihr ewigen_/ADJA_/ewig Liebe_/NN_/Liebe und_/KON_/und Hingabe_/NN_/Hingabe 
[75]  Er_/PPER_/er schrieb_/VVFIN_/schreiben dies_/PDS_/dies an_/APPR_/an seine_/PPOSAT_/sein Liebe_/NN_/Liebe ,_/$,_/, 
[76]  Es_/NN_/Es ist_/VAFIN_/sein ein_/ART_/eine Ausdruck_/NN_/Ausdruck von_/APPR_/von Liebe_/NN_/Liebe ._/$._/. Es_/PPER_/es ist_/VAFIN_/sein ein_/ART_/eine Ausdruck_/NN_/Ausdruck von_/NE_/von ..._/$._/... 
[77]  Kunst_/NN_/Kunst ,_/$,_/, im_/APPRART_/in platonischen_/ADJA_/platonisch Sinne_/NN_/Sinn ,_/$,_/, ist_/VAFIN_/sein Wahrheit_/NN_/Wahrheit ,_/$,_/, Schönheit_/NN_/Schönheit und_/KON_/und Liebe_/NN_/Liebe ._/$._/. 
[78]  Wir_/PPER_/wir haben_/VAFIN_/haben nicht_/PTKNEG_/nicht wirklich_/ADJD_/wirklich ein_/ART_/eine Problem_/NN_/Problem damit_/PAV_/damit ,_/$,_/, über_/APPR_/über Liebe_/NN_/Liebe zu_/PTKZU_/zu sprechen_/VVINF_/sprechen ._/$._/. 
[79]  Nun_/ADV_/nun ,_/$,_/, für_/APPR_/für mich_/PRF_/ich sind_/VAFIN_/sein die_/ART_/die Erfahrung_/NN_/Erfahrung von_/APPR_/von Liebe_/NN_/Liebe und_/KON_/und die_/ART_/die Erfahrung_/NN_/Erfahrung von_/APPR_/von Design_/NN_/Design 
[80]  Ich_/PPER_/ich entdeckte_/VVFIN_/entdecken etwas_/ADV_/etwas über_/APPR_/über Liebe_/NN_/Liebe und_/KON_/und Design_/NN_/Design durch_/APPR_/durch ein_/ART_/eine Projekt_/NN_/Projekt mit_/APPR_/mit Namen_/NN_/Name Deep_/NE_/<unknown> Blue_/NN_/<unknown> ._/$._/. 
[81]  Sie_/PPER_/Sie|sie wissen_/VVFIN_/wissen schon_/ADV_/schon ,_/$,_/, wir_/PPER_/wir könnten_/VMFIN_/können das_/ART_/die Wort_/NN_/Wort Liebe_/NN_/Liebe aus_/APPR_/aus einer_/ART_/eine Menge_/NN_/Menge Dinge_/NN_/Ding in_/APPR_/in unserer_/PPOSAT_/unser Gesellschaft_/NN_/Gesellschaft herausnehmen_/VVFIN_/herausnehmen 
[82]  Er_/PPER_/er sagte_/VVFIN_/sagen :_/$._/: "_/$(_/" Liebe_/NN_/Liebe ist_/VAFIN_/sein nicht_/PTKNEG_/nicht eigennützig_/ADJD_/eigennützig "_/$(_/" ,_/$,_/, das_/PDS_/die hatte_/VAFIN_/haben er_/PPER_/er gesagt_/VVPP_/sagen ,_/$,_/, 
[83]  "_/$(_/" Liebe_/NN_/Liebe ist_/VAFIN_/sein ,_/$,_/, nicht_/PTKNEG_/nicht nachzählen_/VVINF_/nachzählen wie_/KOKOM_/wie oft_/ADV_/oft jemand_/PIS_/jemand "_/$(_/" Ich_/PPER_/ich liebe_/VVFIN_/lieben dich_/PPER_/du "_/$(_/" sagt_/VVFIN_/sagen ,_/$,_/, 
[84]  Liebe_/NN_/Liebe ist_/VAFIN_/sein nicht_/PTKNEG_/nicht eigennützig_/ADJD_/eigennützig "_/$(_/" ._/$._/. Und_/KON_/und ich_/PPER_/ich dachte_/VVFIN_/denken darüber_/PAV_/darüber nach_/PTKVZ_/nach ,_/$,_/, ich_/PPER_/ich dachte_/VVFIN_/denken :_/$._/: 
[85]  "_/$(_/" Wisst_/VVIMP_/<unknown> ihr_/PPER_/ihr ,_/$,_/, ich_/PPER_/ich zeige_/VVFIN_/zeigen hier_/ADV_/hier keine_/PIAT_/keine Liebe_/NN_/Liebe ._/$._/. Ich_/PPER_/ich bin_/VAFIN_/sein ganz_/ADV_/ganz ernsthaft_/ADJD_/ernsthaft dabei_/PAV_/dabei ,_/$,_/, keine_/PIAT_/keine Liebe_/NN_/Liebe zu_/PTKZU_/zu zeigen_/VVINF_/zeigen ._/$._/. 
[86]  Wir_/PPER_/wir stellten_/VVFIN_/stellen das_/ART_/die Auto_/NN_/Auto wieder_/ADV_/wieder in_/APPR_/in den_/ART_/die Fokus_/NN_/Fokus unserer_/PPOSAT_/unser Gedanken_/NN_/Gedanke ,_/$,_/, und_/KON_/und wir_/PPER_/wir stellten_/VVFIN_/stellen Liebe_/NN_/Liebe ,_/$,_/, 
[87]  Über_/APPR_/über Vertrauen_/NN_/Vertrauen und_/KON_/und Liebe_/NN_/Liebe lässt_/VVFIN_/lassen sich_/PRF_/sich vieles_/PIS_/viele sagen_/VVINF_/sagen ,_/$,_/, 
[88]  Es_/NN_/Es ist_/VAFIN_/sein wahr_/ADJD_/wahr ,_/$,_/, dass_/KOUS_/dass Vertrauen_/NN_/Vertrauen und_/KON_/und Liebe_/NN_/Liebe es_/PPER_/es die_/ART_/die Sache_/NN_/Sache wert_/ADJD_/wert machen_/VVFIN_/machen ._/$._/. 
[89]  "_/$(_/" Lieber_/NN_/Liebe Jesus_/NE_/Jesus ,_/$,_/, dieses_/PDAT_/dies Kind_/NN_/Kind hat_/VAFIN_/haben jetzt_/ADV_/jetzt schon_/ADV_/schon angefangen_/VVPP_/anfangen ._/$._/. "_/$(_/" 
[90]  und_/KON_/und es_/PPER_/es war_/VAFIN_/sein Liebe_/NN_/Liebe auf_/APPR_/auf den_/ART_/die ersten_/ADJA_/erst Blick_/NN_/Blick ._/$._/. 
[91]  Wie_/KOKOM_/wie es_/PPER_/es begann_/VVFIN_/beginnen ?_/$._/? Schauen_/VVFIN_/schauen Sie_/PPER_/Sie|sie sich_/PRF_/sich Joses_/NE_/Joses Liebe_/NN_/Liebe zum_/APPRART_/zu Detail_/NN_/Detail an_/PTKVZ_/an ._/$._/. 
[92]  Aber_/KON_/aber am_/PTKA_/am wichtigsten_/ADJD_/wichtig :_/$._/: Lieben_/VVFIN_/lieben Sie_/PPER_/Sie|sie sie_/PPER_/sie einfach_/ADJD_/einfach ._/$._/. 
[93]  Nichts_/NN_/Nichts funktioniert_/VVFIN_/funktionieren so_/ADV_/so sehr_/ADV_/sehr ,_/$,_/, wie_/KOKOM_/wie bedingungslose_/ADJA_/bedingungslos Liebe_/NN_/Liebe ._/$._/. 
[94]  weiß_/VVFIN_/wissen ,_/$,_/, dass_/KOUS_/dass Liebe_/NN_/Liebe die_/ART_/die Dinge_/NN_/Ding richtig_/ADJD_/richtig macht_/VVFIN_/machen ._/$._/. 
[95]  Leidenschaft_/NN_/Leidenschaft ist_/VAFIN_/sein Ihre_/PPOSAT_/ihr größte_/ADJA_/groß Liebe_/NN_/Liebe ._/$._/. 
[96]  Ihre_/PPOSAT_/ihr größte_/ADJA_/groß Liebe_/NN_/Liebe im_/APPRART_/in Vergleich_/NN_/Vergleich zu_/APPR_/zu allen_/PIAT_/alle anderen_/PIS_/andere Dingen_/NN_/Ding gefunden_/VVPP_/finden ,_/$,_/, 
[97]  über_/APPR_/über seine_/PPOSAT_/sein Liebe_/NN_/Liebe zu_/APPR_/zu ihr_/PPER_/ihr ._/$._/. 
[98]  auszusetzen_/VVIZU_/aussetzen :_/$._/: Liebe_/ADJA_/lieb ,_/$,_/, 
[99]  Liebe_/NN_/Liebe zu_/APPR_/zu dem_/ART_/die Land_/NN_/Land ,_/$,_/, für_/APPR_/für das_/ART_/die Bergauf_/NN_/<unknown> 
[100]  Liebe_/NN_/Liebe und_/KON_/und Respekt_/NN_/Respekt 
[101]  Liebe_/NN_/Liebe und_/KON_/und Respekt_/NN_/Respekt für_/APPR_/für sich_/PRF_/sich selbst_/ADV_/selbst ,_/$,_/, 
[102]  Sie_/PPER_/Sie|sie tun_/VVFIN_/tun es_/PPER_/es nämlich_/ADV_/nämlich aus_/APPR_/aus Liebe_/NN_/Liebe zu_/APPR_/zu der_/ART_/die Sache_/NN_/Sache ._/$._/. 
[103]  lass_/ADJD_/laß uns_/PPER_/wir Liebende_/NN_/Liebende und_/KON_/und Geliebte_/NN_/Geliebte sein_/VAINF_/sein ;_/$._/; 
[104]  nachdem_/KOUS_/nachdem sie_/PPER_/sie eine_/ART_/eine Anzeige_/NN_/Anzeige von_/APPR_/von U.S._/NE_/U.S. Senator_/NN_/Senator Joe_/NE_/Joe Lieberman_/NE_/<unknown> erhalten_/VVPP_/erhalten haben_/VAFIN_/haben ,_/$,_/, 
[105]  lehren_/VVFIN_/lehren uns_/PPER_/wir auch_/ADV_/auch Liebe_/NN_/Liebe und_/KON_/und Vertrauen_/NN_/Vertrauen ._/$._/. 
[106]  und_/KON_/und man_/PIS_/man konnte_/VMFIN_/können diese_/PDAT_/dies Liebe_/NN_/Liebe in_/APPR_/in seinen_/PPOSAT_/sein Predigten_/NN_/Predigt spüren_/VVINF_/spüren ,_/$,_/, 
[107]  darum_/PAV_/darum bat_/VVFIN_/bitten ,_/$,_/, ob_/KOUS_/ob sie_/PPER_/sie für_/APPR_/für Eintrittskarten_/NN_/Eintrittskarte und_/KON_/und Liebe_/ADJA_/lieb 
[108]  45_/CARD_/45 Jahre_/NN_/Jahr alte_/ADJA_/alt Liebesgeschichte_/NN_/Liebesgeschichte 
[109]  und_/KON_/und legen_/VVFIN_/legen sicherlich_/ADV_/sicherlich keine_/PIAT_/keine Liebe_/NN_/Liebe ihnen_/PPER_/sie gegenüber_/PTKVZ_/gegenüber an_/APPR_/an den_/ART_/die Tag_/NN_/Tag ._/$._/. 
[110]  Und_/KON_/und wir_/PPER_/wir haben_/VAFIN_/haben Angst_/NN_/Angst ,_/$,_/, wie_/KOKOM_/wie junge_/ADJA_/jung Liebende_/NN_/Liebende ,_/$,_/, 
[111]  denn_/ADV_/denn ,_/$,_/, wenn_/KOUS_/wenn Sie_/PPER_/Sie|sie Leute_/NN_/Leute nach_/APPR_/nach der_/ART_/die Liebe_/NN_/Liebe fragen_/VVFIN_/fragen ,_/$,_/, 
[112]  sie_/PPER_/sie haben_/VAFIN_/haben ein_/ART_/eine starkes_/ADJA_/stark Gefühl_/NN_/Gefühl der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Zugehörigkeit_/NN_/Zugehörigkeit --_/$(_/-- 
[113]  die_/ART_/die ein_/ART_/eine starkes_/ADJA_/stark Gefühl_/NN_/Gefühl der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Zugehörigkeit_/NN_/Zugehörigkeit haben_/VAFIN_/haben ,_/$,_/, 
[114]  ein_/ART_/eine starkes_/ADJA_/stark Gefühl_/NN_/Gefühl der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Zugehörigkeit_/NN_/Zugehörigkeit haben_/VAFIN_/haben ,_/$,_/, 
[115]  glauben_/VVFIN_/glauben ,_/$,_/, dass_/KOUS_/dass sie_/PPER_/sie der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Zugehörigkeit_/NN_/Zugehörigkeit würdig_/ADJD_/würdig sind_/VAFIN_/sein ._/$._/. 
[116]  von_/APPR_/von Zugehörigkeit_/NN_/Zugehörigkeit ,_/$,_/, von_/APPR_/von Liebe_/NN_/Liebe ._/$._/. 
[117]  aber_/KON_/aber du_/PPER_/du bist_/VAFIN_/sein der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Zugehörigkeit_/NN_/Zugehörigkeit würdig_/ADJD_/würdig ._/$._/. "_/$(_/" 
[118]  "_/$(_/" Fand_/NN_/<unknown> wahre_/ADJA_/wahr Liebe_/NN_/Liebe ,_/$,_/, heiratete_/VVFIN_/heiraten jemand_/PIS_/jemand anderen_/PIS_/andere ._/$._/. "_/$(_/" 
[119]  Lieben_/ADJA_/lieb Menschen_/NN_/Mensch ihre_/PPOSAT_/ihr Töchter_/NN_/Tochter 
[120]  ja_/ADV_/ja ,_/$,_/, dann_/ADV_/dann ,_/$,_/, denke_/VVFIN_/denken ich_/PPER_/ich ,_/$,_/, könnte_/VMFIN_/können man_/PIS_/man wahrscheinlich_/ADJD_/wahrscheinlich Liebe_/NN_/Liebe empfinden_/VVINF_/empfinden ._/$._/. 
[121]  voller_/ADJA_/voll Lachen_/NN_/Lache|Lachen und_/KON_/und Liebe_/NN_/Liebe ,_/$,_/, in_/APPR_/in einer_/ART_/eine sehr_/ADV_/sehr eng_/ADJD_/eng verbundenen_/ADJA_/verbunden Familie_/NN_/Familie ._/$._/. 
[122]  wie_/KOKOM_/wie Liebe_/NN_/Liebe und_/KON_/und Güte_/NN_/Güte 
[123]  ein_/ART_/eine bosnisches_/ADJA_/bosnisch Liebeslied_/NN_/Liebeslied ,_/$,_/, 
[124]  und_/KON_/und in_/APPR_/in Beziehungen_/NN_/Beziehung zwischen_/APPR_/zwischen Liebenden_/NN_/Liebende ,_/$,_/, und_/KON_/und Eltern_/NN_/Eltern ,_/$,_/, 
[125]  würde_/VAFIN_/werden ich_/PPER_/ich einen_/ART_/eine Raum_/NN_/Raum der_/ART_/die Liebe_/NN_/Liebe widmen_/VVINF_/widmen ,_/$,_/, einen_/PIS_/eine der_/ART_/die Großzügigkeit_/NN_/Großzügigkeit ._/$._/. 
[126]  Ich_/PPER_/ich weiß_/VVFIN_/wissen ,_/$,_/, es_/PPER_/es klingt_/VVFIN_/klingen nach_/APPR_/nach strenger_/ADJA_/streng Liebe_/NN_/Liebe ,_/$,_/, 
[127]  (_/$(_/( Video_/NN_/Video )_/$(_/) John_/NE_/John Edwards_/NE_/Edward :_/$._/: Liebend_/ADJD_/liebend gern_/ADV_/gern werde_/VAFIN_/werden ich_/PPER_/ich mich_/PRF_/ich einem_/ART_/eine Test_/NN_/Test unterziehen_/VVINF_/unterziehen ._/$._/. 
[128]  oder_/KON_/oder Respekt_/NN_/Respekt oder_/KON_/oder Liebe_/NN_/Liebe ._/$._/. 
[129]  Sie_/PPER_/Sie|sie würden_/VAFIN_/werden aufwachsen_/VVINF_/aufwachsen und_/KON_/und ihre_/PPOSAT_/ihr erste_/ADJA_/erst Liebe_/NN_/Liebe aus_/APPR_/aus der_/ART_/die Schulzeit_/NN_/Schulzeit heiraten_/VVFIN_/heiraten 
[130]  aus_/APPR_/aus einem_/ART_/eine Liebesgedicht_/NN_/Liebesgedicht gekommen_/VVPP_/kommen zu_/PTKZU_/zu sein_/VAINF_/sein scheint_/VVFIN_/scheinen ,_/$,_/, 
[131]  Sie_/PPER_/Sie|sie könnten_/VMFIN_/können von_/APPR_/von Liebe_/NN_/Liebe zu_/APPR_/zu Hass_/NN_/Hass wechseln_/VVINF_/wechseln ._/$._/. 
[132]  Publikum_/NN_/Publikum :_/$._/: Party_/NN_/Party ._/$._/. Alles_/PIAT_/alle Liebe_/NN_/Liebe ._/$._/. 
[133]  Noch_/ADV_/noch nach_/APPR_/nach 1.300_/CARD_/@card@ Jahren_/NN_/Jahr können_/VMFIN_/können sich_/PRF_/sich diese_/PDS_/diese zwei_/CARD_/zwei Liebenden_/ADJA_/liebend 
[134]  Sie_/PPER_/Sie|sie singen_/VVFIN_/singen aus_/APPR_/aus Liebe_/NN_/Liebe ,_/$,_/, sie_/PPER_/sie tanzen_/VVFIN_/tanzen aus_/APPR_/aus Liebe_/NN_/Liebe ,_/$,_/, 
[135]  sie_/PPER_/sie dichten_/ADJA_/dicht Verse_/NN_/Vers und_/KON_/und schreiben_/VVFIN_/schreiben Geschichten_/NN_/Geschichte über_/APPR_/über die_/ART_/die Liebe_/NN_/Liebe ._/$._/. 
[136]  Sie_/PPER_/Sie|sie erzählen_/VVFIN_/erzählen Mythen_/NN_/Mythos und_/KON_/und Legenden_/NN_/Legende über_/APPR_/über die_/ART_/die Liebe_/NN_/Liebe ._/$._/. 
[137]  Sie_/PPER_/Sie|sie sehnen_/VVFIN_/sehnen sich_/PRF_/sich nach_/APPR_/nach Liebe_/NN_/Liebe ,_/$,_/, sie_/PPER_/sie leben_/VVFIN_/leben für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe ,_/$,_/, 
[138]  sie_/PPER_/sie töten_/VVFIN_/töten für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe und_/KON_/und sie_/PPER_/sie sterben_/VVFIN_/sterben für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe ._/$._/. 
[139]  Anthropologen_/NN_/Anthropologe haben_/VAFIN_/haben Hinweise_/NN_/Hinweis auf_/APPR_/auf romantische_/ADJA_/romantisch Liebe_/NN_/Liebe in_/APPR_/in 170_/CARD_/170 Völkern_/NN_/Volk gefunden_/VVPP_/finden ._/$._/. 
[140]  Aber_/KON_/aber Liebe_/NN_/Liebe ist_/VAFIN_/sein nicht_/PTKNEG_/nicht immer_/ADV_/immer eine_/ART_/eine positive_/ADJA_/positiv Erfahrung_/NN_/Erfahrung ._/$._/. 
[141]  viele_/PIAT_/viele Fragen_/NN_/Frage über_/APPR_/über die_/ART_/die Liebe_/NN_/Liebe gestellt_/VVPP_/stellen ,_/$,_/, 
[142]  Fast_/ADV_/fast niemand_/PIS_/niemand kommt_/VVFIN_/kommen aus_/APPR_/aus der_/ART_/die Liebe_/NN_/Liebe unversehrt_/ADJD_/unversehrt hinaus_/PTKVZ_/hinaus ._/$._/. 
[143]  wie_/KOKOM_/wie ich_/PPER_/ich finde_/VVFIN_/finden ,_/$,_/, eindrucksvollste_/ADJA_/eindrucksvoll Liebesgedicht_/NN_/Liebesgedicht der_/ART_/die Welt_/NN_/Welt vorlesen_/VVFIN_/vorlesen 
[144]  Schmerz_/NN_/Schmerz durchzieht_/VVFIN_/durchziehen meinen_/PPOSAT_/mein Körper_/NN_/Körper mit_/APPR_/mit dem_/ART_/die Feuer_/NN_/Feuer meiner_/PPOSAT_/mein Liebe_/NN_/Liebe zu_/APPR_/zu dir._/NE_/<unknown> 
[145]  Schmerz_/NN_/Schmerz wie_/KOKOM_/wie ein_/ART_/eine Geschwür_/NN_/Geschwür ,_/$,_/, das_/PRELS_/die zu_/PTKZU_/zu platzen_/VVINF_/platzen droht_/VVFIN_/drohen in_/APPR_/in meiner_/PPOSAT_/mein Liebe_/NN_/Liebe zu_/APPR_/zu dir_/PPER_/du ,_/$,_/, 
[146]  vom_/APPRART_/von Feuer_/NN_/Feuer verzehrt_/VVFIN_/verzehren in_/APPR_/in meiner_/PPOSAT_/mein Liebe_/NN_/Liebe zu_/APPR_/zu dir._/ADJA_/<unknown> 
[147]  ich_/PPER_/ich denke_/VVFIN_/denken an_/APPR_/an deine_/PPOSAT_/dein Liebe_/NN_/Liebe zu_/APPR_/zu mir_/PPER_/ich ,_/$,_/, 
[148]  ich_/PPER_/ich bin_/VAFIN_/sein durch_/APPR_/durch deine_/PPOSAT_/dein Liebe_/NN_/Liebe für_/APPR_/für mich_/PRF_/ich zerrissen_/VVPP_/zerreißen ._/$._/. 
[149]  wohin_/PWAV_/wohin gehst_/VVFIN_/gehen du_/PPER_/du mit_/APPR_/mit meiner_/PPOSAT_/mein Liebe_/NN_/Liebe ?_/$._/? 
[150]  Erinnere_/NN_/<unknown> dich_/PPER_/du meiner_/PPOSAT_/mein Worte_/NN_/Wort ,_/$,_/, meine_/PPOSAT_/mein Liebe_/NN_/Liebe ._/$._/. 
[151]  Lebewohl_/NN_/Lebewohl ,_/$,_/, meine_/PPOSAT_/mein Liebe_/NN_/Liebe ,_/$,_/, lebewohl_/ADJD_/<unknown> ._/$._/. "_/$(_/" 
[152]  Romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ist_/VAFIN_/sein eines_/PIS_/eine der_/ART_/die mächstigsten_/ADJA_/<unknown> Gefühle_/NN_/Gefühl der_/ART_/die Welt_/NN_/Welt ._/$._/. 
[153]  Aber_/KON_/aber romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ist_/VAFIN_/sein viel_/ADV_/viel mehr_/ADV_/mehr als_/KOKOM_/als ein_/ART_/eine Kokain-Rausch_/NN_/<unknown> -_/$(_/- 
[154]  Romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ist_/VAFIN_/sein Besessenheit_/NN_/<unknown> ._/$._/. Sie_/PPER_/Sie|sie hält_/VVFIN_/halten dich_/PPER_/du fest_/PTKVZ_/fest ._/$._/. 
[155]  Wild_/NN_/Wild ist_/VAFIN_/sein die_/ART_/die Liebe_/NN_/Liebe ._/$._/. 
[156]  die_/ART_/die mit_/APPR_/mit intensiver_/ADJA_/intensiv romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe assoziiert_/VVPP_/assoziieren ist_/VAFIN_/sein ._/$._/. 
[157]  er_/PPER_/er sagte:"Je_/VVFIN_/<unknown> kleiner_/ADJD_/klein meine_/PPOSAT_/mein Hoffnung_/NN_/Hoffnung ,_/$,_/, umso_/ADV_/umso heißer_/ADJD_/heiß meine_/PPOSAT_/mein Liebe_/NN_/Liebe ._/$._/. "_/$(_/" 
[158]  Wenn_/KOUS_/wenn Sie_/PPER_/Sie|sie in_/APPR_/in der_/ART_/die Liebe_/NN_/Liebe abgelehnt_/VVPP_/ablehnen wurden_/VAFIN_/werden ,_/$,_/, 
[159]  sind_/VAFIN_/sein Sie_/PPER_/Sie|sie nicht_/PTKNEG_/nicht nur_/ADV_/nur gefangen_/PTKVZ_/gefangen in_/APPR_/in den_/ART_/die Gefühlen_/NN_/Gefühl der_/ART_/die romantischen_/ADJA_/romantisch Liebe_/NN_/Liebe ,_/$,_/, 
[160]  dass_/KOUS_/dass romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ein_/ART_/eine Trieb_/NN_/Trieb ist_/VAFIN_/sein ,_/$,_/, ein_/ART_/eine einfacher_/ADJA_/einfach Paarungstrieb_/NN_/<unknown> ._/$._/. 
[161]  Romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ermöglicht_/VVFIN_/ermöglichen es_/PPER_/es Ihnen_/PPER_/Sie|sie ,_/$,_/, Ihre_/PPOSAT_/ihr Paarungsenergie_/NN_/<unknown> 
[162]  Ich_/PPER_/ich glaube_/VVFIN_/glauben ,_/$,_/, von_/APPR_/von all_/PIAT_/alle der_/ART_/die Poesie_/NN_/Poesie ,_/$,_/, die_/PRELS_/die ich_/PPER_/ich zum_/APPRART_/zu Thema_/NN_/Thema romantische_/ADJA_/romantisch Liebe_/NN_/Liebe gelesen_/VVPP_/lesen habe_/VAFIN_/haben ,_/$,_/, 
[163]  Er_/PPER_/er sagte_/VVFIN_/sagen :_/$._/: "_/$(_/" Der_/ART_/die Gott_/NN_/Gott der_/ART_/die Liebe_/NN_/Liebe lebt_/VVFIN_/leben in_/APPR_/in einem_/ART_/eine Zustand_/NN_/Zustand der_/ART_/die Bedürftigkeit_/NN_/Bedürftigkeit ._/$._/. 
[164]  Ich_/PPER_/ich glaube_/VVFIN_/glauben zudem_/ADV_/zudem ,_/$,_/, dass_/KOUS_/dass romantische_/ADJA_/romantisch Liebe_/NN_/Liebe eine_/ART_/eine Sucht_/NN_/Sucht ist_/VAFIN_/sein :_/$._/: 
[165]  Ich_/PPER_/ich habe_/VAFIN_/haben eine_/ART_/eine Freundin_/NN_/Freundin ,_/$,_/, die_/PRELS_/die gerade_/ADV_/gerade eine_/ART_/eine schreckliche_/ADJA_/schrecklich Liebesaffaire_/NN_/<unknown> verarbeitet_/VVPP_/verarbeiten ,_/$,_/, 
[166]  romantische_/ADJA_/romantisch Liebe_/NN_/Liebe eine_/PIS_/eine der_/ART_/die stärksten_/ADJA_/stark abhängig_/ADJD_/abhängig machenden_/ADJA_/machend Drogen_/NN_/Droge auf_/APPR_/auf Erden_/NN_/Erde|Erden ist_/VAFIN_/sein ._/$._/. 
[167]  für_/APPR_/für das_/PDS_/die ,_/$,_/, was_/PWS_/was Sie_/PPER_/Sie|sie und_/KON_/und ich_/PPER_/ich "_/$(_/" Liebe_/NN_/Liebe auf_/APPR_/auf den_/ART_/die ersten_/ADJA_/erst Blick_/NN_/Blick "_/$(_/" nennen_/VVFIN_/nennen ._/$._/. 
[168]  was_/PRELS_/was ich_/PPER_/ich über_/APPR_/über Liebe_/NN_/Liebe weiß_/VVFIN_/wissen ,_/$,_/, sie_/PPER_/sie mir_/PPER_/ich vergrault_/VVPP_/vergraulen hat_/VAFIN_/haben ._/$._/. 
[169]  die_/ART_/die mit_/APPR_/mit intensiver_/ADJA_/intensiv romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe verknüpft_/VVPP_/verknüpfen sind_/VAFIN_/sein ,_/$,_/, 
[170]  und_/KON_/und gefragt_/VVPP_/fragen werden_/VAINF_/werden müssen_/VMFIN_/müssen über_/APPR_/über die_/ART_/die romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ._/$._/. 
[171]  und_/KON_/und ab_/APPR_/ab einem_/ART_/eine gewissen_/ADJA_/gewiss Punkt_/NN_/Punkt -_/$(_/- Liebe_/NN_/Liebe wird_/VAFIN_/werden immer_/ADV_/immer magisch_/ADJD_/magisch sein_/VAINF_/sein ,_/$,_/, 
[172]  Ich_/PPER_/ich komme_/VVFIN_/kommen zu_/APPR_/zu folgendem_/ADJA_/folgend Schluss_/NN_/Schluss :_/$._/: Liebe_/NN_/Liebe ist_/VAFIN_/sein in_/APPR_/in uns._/ADJA_/<unknown> 
[173]  dass_/KOUS_/dass meine_/PPOSAT_/mein erste_/ADJA_/erst Liebe_/NN_/Liebe ein_/ART_/eine Mädchen_/NN_/Mädchen war_/VAFIN_/sein ._/$._/. 
[174]  wo_/PWAV_/wo Experten_/NN_/Experte unsere_/PPOSAT_/unser Liebe_/NN_/Liebe im_/APPRART_/in landesweiten_/ADJA_/landesweit Fernsehen_/NN_/Fernsehen mit_/APPR_/mit Sodomie_/NN_/Sodomie gleichsetzten_/VVFIN_/gleichsetzen ._/$._/. 
[175]  Es_/NN_/Es ist_/VAFIN_/sein mein_/PPOSAT_/mein Baby_/NN_/Baby und_/KON_/und es_/PPER_/es ist_/VAFIN_/sein voll_/ADJD_/voll Liebe_/NN_/Liebe ._/$._/. "_/$(_/" 
[176]  Und_/KON_/und das_/PDS_/die ist_/VAFIN_/sein all_/PIAT_/alle diese_/PDAT_/dies anderen_/PIAT_/andere Sachen_/NN_/Sache --_/$(_/-- die_/ART_/die Liebe_/NN_/Liebe zu_/APPR_/zu meinem_/PPOSAT_/mein Kind_/NN_/Kind ._/$._/. 
[177]  Und_/KON_/und ich_/PPER_/ich denke_/VVFIN_/denken ,_/$,_/, dass_/KOUS_/dass wir_/PPER_/wir wissen_/VVFIN_/wissen ,_/$,_/, dass_/KOUS_/dass Liebe_/NN_/Liebe robust_/ADJD_/robust genug_/ADV_/genug ist_/VAFIN_/sein ,_/$,_/, 
[178]  dies_/PDS_/dies ist_/VAFIN_/sein es_/PPER_/es ,_/$,_/, was_/PRELS_/was unsere_/PPOSAT_/unser Geschichten_/NN_/Geschichte in_/APPR_/in Liebesgeschichten_/NN_/Liebesgeschichte verwandeln_/VVINF_/verwandeln kann_/VMFIN_/können 
[179]  dies_/PDS_/dies für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe taten_/VVFIN_/tun und_/KON_/und nicht_/PTKNEG_/nicht für_/APPR_/für den_/ART_/die Kampf_/NN_/Kampf ._/$._/. 
[180]  Venus_/NN_/Venus ist_/VAFIN_/sein die_/ART_/die Göttin_/NN_/Göttin der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Fruchtbarkeit_/NN_/Fruchtbarkeit ,_/$,_/, 
[181]  Und_/KON_/und wir_/PPER_/wir sehen_/VVFIN_/sehen das_/PDS_/die ,_/$,_/, wenn_/KOUS_/wenn ein_/ART_/eine Liebespaar_/NN_/Liebespaar eine_/ART_/eine Straße_/NN_/Straße entlanggeht_/VVFIN_/entlanggehen ,_/$,_/, 
[182]  wenn_/KOUS_/wenn ein_/ART_/eine Gläubiger_/NN_/Gläubiger sich_/PRF_/sich eins_/CARD_/eins fühlt_/VVFIN_/fühlen mit_/APPR_/mit der_/ART_/die Liebe_/ADJA_/lieb Gottes_/NN_/Gott ._/$._/. 
[183]  Und_/KON_/und viele_/PIS_/viele von_/APPR_/von uns_/PPER_/wir fühlen_/VVFIN_/fühlen das_/PDS_/die in_/APPR_/in der_/ART_/die Liebe_/NN_/Liebe ,_/$,_/, 
[184]  wenn_/KOUS_/wenn Liebende_/NN_/Liebende sich_/PRF_/sich eins_/CARD_/eins fühlen_/VVFIN_/fühlen ._/$._/. 
[185]  positive_/ADJA_/positiv Gefühle_/NN_/Gefühl wie_/KOKOM_/wie Neugier_/NN_/Neugier oder_/KON_/oder Liebe_/NN_/Liebe auszulösen_/VVIZU_/auslösen ,_/$,_/, 
[186]  sie_/PPER_/sie blitzten_/VVFIN_/blitzen in_/APPR_/in Liebe_/ADJA_/lieb ,_/$,_/, leidenschaftliche_/ADJA_/leidenschaftlich Liebe_/NN_/Liebe für_/APPR_/für seine_/PPOSAT_/sein Studierenden_/NN_/Studierende ._/$._/. 
[187]  Das_/PDS_/die bedeutet_/VVFIN_/bedeuten ,_/$,_/, dass_/KOUS_/dass ich_/PPER_/ich meine_/PPOSAT_/mein Liebe_/NN_/Liebe zu_/APPR_/zu Mathe_/NN_/Mathe und_/KON_/und zur_/APPRART_/zu Magie_/NN_/Magie verbinde_/VVFIN_/verbinden 
[188]  Menschen_/NN_/Mensch ,_/$,_/, die_/PRELS_/die einander_/PRF_/einander ihre_/PPOSAT_/ihr Liebe_/NN_/Liebe senden_/VVFIN_/senden ._/$._/. 
[189]  Beginnen_/NN_/Beginnen möchte_/VMFIN_/mögen ich_/PPER_/ich aber_/ADV_/aber mit_/APPR_/mit meiner_/PPOSAT_/mein Arbeit_/NN_/Arbeit über_/APPR_/über die_/ART_/die romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ,_/$,_/, 
[190]  17_/CARD_/17 die_/ART_/die sehr_/ADV_/sehr verliebt_/ADJD_/verliebt waren_/VAFIN_/sein und_/KON_/und deren_/PDAT_/die Liebe_/NN_/Liebe erwidert_/VVPP_/erwidern wurde_/VAFIN_/werden 
[191]  und_/KON_/und dann_/ADV_/dann darüber_/PAV_/darüber ,_/$,_/, wohin_/PWAV_/wohin ich_/PPER_/ich denke_/VVFIN_/denken die_/ART_/die Liebe_/NN_/Liebe geht_/VVFIN_/gehen ._/$._/. 
[192]  "_/$(_/" Was_/PWS_/was ist_/VAFIN_/sein die_/ART_/die Liebe_/NN_/Liebe ?_/$._/? "_/$(_/" ,_/$,_/, schrieb_/VVFIN_/schreiben Shakespeare_/NE_/Shakespeare ._/$._/. 
[193]  Ich_/PPER_/ich begann_/VVFIN_/beginnen zu_/PTKVZ_/zu verstehen_/VVFIN_/verstehen was_/PIAT_/was romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ist_/VAFIN_/sein ,_/$,_/, indem_/KOUS_/indem ich_/PPER_/ich mir_/PPER_/ich 
[194]  Er_/PPER_/er sagte_/VVFIN_/sagen :_/$._/: "_/$(_/" Liebe_/NN_/Liebe besteht_/VVFIN_/bestehen darin_/PAV_/darin die_/ART_/die Unterschiede_/NN_/Unterschied zwischen_/APPR_/zwischen der_/ART_/die einen_/ADJA_/einen Frau_/NN_/Frau und_/KON_/und der_/ART_/die anderen_/PIS_/andere zu_/PTKZU_/zu überschätzen_/VVINF_/überschätzen "_/$(_/" 
[195]  So_/ADV_/so wie_/KOKOM_/wie Chaucer_/NN_/<unknown> sagte_/VVFIN_/sagen :_/$._/: "_/$(_/" Liebe_/NN_/Liebe ist_/VAFIN_/sein blind_/ADJD_/blind ._/$._/. "_/$(_/" 
[196]  Um_/APPR_/um romantische_/ADJA_/romantisch Liebe_/NN_/Liebe zu_/PTKZU_/zu verstehen_/VVINF_/verstehen ,_/$,_/, 
[197]  beschloss_/VVFIN_/beschließen ich_/PPER_/ich Liebesgedichte_/NN_/Liebesgedicht ,_/$,_/, aus_/APPR_/aus der_/ART_/die ganzen_/ADJA_/ganz Welt_/NN_/Welt zu_/PTKZU_/zu lesen_/VVINF_/lesen ,_/$,_/, 
[198]  Ganz_/ADV_/ganz einfach_/ADJD_/einfach ._/$._/. Romantische_/ADJA_/romantisch Liebe_/NN_/Liebe ist_/VAFIN_/sein sehr_/ADV_/sehr einfach_/ADJD_/einfach ._/$._/. 
[199]  Aber_/KON_/aber die_/ART_/die Hauptcharakteristik_/NN_/Hauptcharakteristik romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe ist_/VAFIN_/sein Verlangen_/NN_/Verlangen ,_/$,_/, 
[200]  Ich_/PPER_/ich fing_/VVFIN_/fangen an_/PTKVZ_/an zu_/PTKZU_/zu verstehen_/VVINF_/verstehen ,_/$,_/, dass_/KOUS_/dass romantische_/ADJA_/romantisch Liebe_/NN_/Liebe keine_/PIAT_/keine Emotion_/NN_/Emotion ist_/VAFIN_/sein ._/$._/. 
[201]  Menschen_/NN_/Mensch leben_/VVFIN_/leben für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe ._/$._/. Sie_/PPER_/Sie|sie sterben_/VVFIN_/sterben für_/APPR_/für die_/ART_/die Liebe_/NN_/Liebe ._/$._/. 
[202]  Der_/ART_/die zweite_/ADJA_/zweit der_/ART_/die drei_/CARD_/drei Prozesse_/NN_/Prozess ist_/VAFIN_/sein die_/ART_/die romantische_/ADJA_/romantisch Liebe_/NN_/Liebe :_/$._/: 
[203]  die_/ART_/die Begeisterung_/NN_/Begeisterung ,_/$,_/, Bessessenheit_/NN_/<unknown> der_/ART_/die anfänglichen_/ADJA_/anfänglich Liebe_/NN_/Liebe ._/$._/. 
[204]  Ich_/PPER_/ich denke_/VVFIN_/denken ,_/$,_/, dass_/KOUS_/dass romantische_/ADJA_/romantisch Liebe_/NN_/Liebe entstand_/VVFIN_/entstehen ,_/$,_/, um_/KOUI_/um seine_/PPOSAT_/sein ganze_/ADJA_/ganz Energie_/NN_/Energie 
[205]  Lust_/NN_/Lust ,_/$,_/, romantische_/ADJA_/romantisch Liebe_/NN_/Liebe und_/KON_/und Bindung_/NN_/Bindung an_/APPR_/an einen_/ART_/eine Partner_/NN_/Partner ._/$._/. 
[206]  Ein_/ART_/eine Wort_/NN_/Wort noch_/ADV_/noch ,_/$,_/, dann_/ADV_/dann gehe_/VVFIN_/gehen ich_/PPER_/ich über_/APPR_/über zu_/APPR_/zu Sex_/NN_/Sex und_/KON_/und Liebe_/NN_/Liebe ._/$._/. 
[207]  Ich_/PPER_/ich nenne_/VVFIN_/nennen nur_/ADV_/nur einige_/PIS_/einige und_/KON_/und fahre_/VVFIN_/fahren dann_/ADV_/dann fort_/ADV_/fort mit_/APPR_/mit Sex_/NN_/Sex und_/KON_/und Liebe_/NN_/Liebe ._/$._/. 
[208]  auf_/APPR_/auf Sex_/NN_/Sex ,_/$,_/, das_/ART_/die Liebesleben_/NN_/Liebesleben und_/KON_/und das_/ART_/die Familienleben_/NN_/Familienleben ._/$._/. 
[209]  Auch_/ADV_/auch sehen_/VVFIN_/sehen wir_/PPER_/wir die_/ART_/die Wiederkehr_/NN_/Wiederkehr romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe ._/$._/. 
[210]  Diese_/PDAT_/dies drei_/CARD_/drei Gehirnstrukturen_/NN_/Gehirnstruktur :_/$._/: Lust_/NN_/Lust ,_/$,_/, romantische_/ADJA_/romantisch Liebe_/NN_/Liebe und_/KON_/und Bindung_/NN_/Bindung 
[211]  Dopamin_/NN_/<unknown> ist_/VAFIN_/sein assoziert_/ADJD_/<unknown> mit_/APPR_/mit romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe ,_/$,_/, 
[212]  Aber_/KON_/aber diese_/PDAT_/dies Strukturen_/NN_/Struktur :_/$._/: Lust_/NN_/Lust ,_/$,_/, romantische_/ADJA_/romantisch Liebe_/NN_/Liebe und_/KON_/und Bindung_/NN_/Bindung 
[213]  gleichzeitig_/ADJD_/gleichzeitig aber_/ADV_/aber intensive_/ADJA_/intensiv romantische_/ADJA_/romantisch Liebe_/NN_/Liebe für_/APPR_/für Jemand_/NN_/Jemand anderen_/PIS_/andere fühlen_/VVFIN_/fühlen ,_/$,_/, 
[214]  zu_/APPR_/zu Gefühlen_/NN_/Gefühl starker_/ADJA_/stark romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe zu_/APPR_/zu einer_/ART_/eine anderen_/PIS_/andere schwingen_/VVINF_/schwingen ._/$._/. 
[215]  Dopamin_/NN_/<unknown> ist_/VAFIN_/sein verantwortlich_/ADJD_/verantwortlich für_/APPR_/für Empfindungen_/NN_/Empfindung romantischer_/ADJA_/romantisch Liebe_/NN_/Liebe ._/$._/. 
[216]  Ich_/PPER_/ich sage_/VVFIN_/sagen nur_/ADV_/nur ,_/$,_/, dass_/KOUS_/dass eine_/ART_/eine Welt_/NN_/Welt ohne_/APPR_/ohne Liebe_/NN_/Liebe ein_/ART_/eine tödlicher_/ADJA_/tödlich Ort_/NN_/Ort ist_/VAFIN_/sein ._/$._/. 
[217]  Ich_/PPER_/ich habe_/VAFIN_/haben die_/ART_/die romantische_/ADJA_/romantisch Liebe_/NN_/Liebe nun_/ADV_/nun seit_/APPR_/seit 30_/CARD_/30 Jahren_/NN_/Jahr erforscht_/VVPP_/erforschen ._/$._/. 
[218]  Man_/PIS_/man verliebt_/VVFIN_/verlieben sich_/PRF_/sich in_/APPR_/in Menschen_/NN_/Mensch ,_/$,_/, die_/PRELS_/die in_/APPR_/in den_/ART_/die "_/$(_/" Liebes-Plan_/NN_/<unknown> "_/$(_/" passen_/VVFIN_/passen ,_/$,_/, 
[219]  Ich_/PPER_/ich habe_/VAFIN_/haben Ihnen_/PPER_/Sie|sie über_/APPR_/über die_/ART_/die Biologie_/NN_/Biologie der_/ART_/die Liebe_/NN_/Liebe erzählt_/VVFIN_/erzählen ._/$._/. 
[220]  Und_/KON_/und somit_/ADV_/somit vielleicht_/ADV_/vielleicht das_/ART_/die Hirnsystem_/NN_/<unknown> für_/APPR_/für romantische_/ADJA_/romantisch Liebe_/NN_/Liebe aktiviert_/VVPP_/aktivieren ._/$._/. 
[221]  Liebe_/NN_/Liebe ist_/VAFIN_/sein wie_/KOKOM_/wie Magie_/NN_/Magie !_/$._/! 
[222]  den_/ART_/die Sextrieb_/NN_/<unknown> ,_/$,_/, die_/ART_/die romantische_/ADJA_/romantisch Liebe_/NN_/Liebe und_/KON_/und die_/ART_/die Fähigkeit_/NN_/Fähigkeit der_/ART_/die Bindung_/NN_/Bindung an_/APPR_/an einen_/ART_/eine Langzeitpartner_/NN_/<unknown> ._/$._/. 
[223]  mit_/APPR_/mit anderen_/PIS_/andere Worten_/NN_/Wort ,_/$,_/, die_/ART_/die Wurzel_/NN_/Wurzel des_/ART_/die Wortes_/NN_/Wort sind_/VAFIN_/sein Liebe_/NN_/Liebe und_/KON_/und Leidenschaft_/NN_/Leidenschaft ._/$._/. 
[224]  "_/$(_/" Meine_/PPOSAT_/mein Liebe_/NN_/Liebe gleicht_/VVFIN_/gleichen zwölf_/CARD_/zwölf äthiopischen_/ADJA_/äthiopisch Ziegen_/NN_/Ziege 
[225]  Nicht_/PTKNEG_/nicht weil_/KOUS_/weil ich_/PPER_/ich zum_/APPRART_/zu ersten_/ADJA_/erst Mal_/NN_/Mal homosexueller_/ADJA_/homosexuell Liebe_/NN_/Liebe und_/KON_/und Sex_/NN_/Sex 
[226]  "_/$(_/" Das_/PDS_/die hier_/ADV_/hier ist_/VAFIN_/sein Liebe_/NN_/Liebe ,_/$,_/, ganz_/ADV_/ganz und_/KON_/und gar_/ADV_/gar ._/$._/. "_/$(_/" 
[227]  Wir_/PPER_/wir hätten_/VAFIN_/haben keinen_/PIAT_/keine Zugang_/NN_/Zugang zu_/APPR_/zu Liebe_/ADJA_/lieb 
[228]  "_/$(_/" Nein_/PTKANT_/nein ,_/$,_/, nein_/PTKANT_/nein ,_/$,_/, nein_/PTKANT_/nein !_/$._/! Lieber_/ADJD_/lieb ein_/ART_/eine Frosch_/NN_/Frosch !_/$._/! "_/$(_/" 
[229]  "_/$(_/" Nein_/PTKANT_/nein ._/$._/. Lieber_/ADJD_/lieb Go_/VVFIN_/<unknown> Go_/ADJA_/<unknown> Gadget_/NN_/<unknown> !_/$._/! "_/$(_/" 
[230]  in_/APPR_/in der_/ART_/die kleine_/ADJA_/klein Dinge_/NN_/Ding aus_/APPR_/aus Liebe_/NN_/Liebe und_/KON_/und 
[231]  Plötzlich_/ADJD_/plötzlich kann_/VMFIN_/können man_/PIS_/man große_/ADJA_/groß Dinge_/NN_/Ding aus_/APPR_/aus Liebe_/NN_/Liebe tun_/VVINF_/tun ._/$._/. "_/$(_/" 
[232]  Nun_/ADV_/nun ,_/$,_/, nicht_/PTKNEG_/nicht alle_/PIAT_/alle diese_/PDAT_/dies Versuche_/NN_/Versuch ,_/$,_/, Großes_/NN_/Große aus_/APPR_/aus Liebe_/NN_/Liebe zu_/PTKZU_/zu tun_/VVINF_/tun ,_/$,_/, 
[233]  "_/$(_/" Liebe_/NN_/Liebe leibliche_/ADJA_/leiblich Mutter_/NN_/Mutter ,_/$,_/, ich_/PPER_/ich habe_/VAFIN_/haben großartige_/ADJA_/großartig Eltern_/NN_/Eltern ._/$._/. 
[234]  Ich_/PPER_/ich habe_/VAFIN_/haben Liebe_/NN_/Liebe gefunden_/VVPP_/finden ._/$._/. Ich_/PPER_/ich bin_/VAFIN_/sein glücklich_/ADJD_/glücklich ._/$._/. "_/$(_/" 
[235]  Liebe_/NN_/Liebe und_/KON_/und Bewunderung_/NN_/Bewunderung aus_/PTKVZ_/aus ._/$._/. 
[236]  Ich_/PPER_/ich glaube_/VVFIN_/glauben ,_/$,_/, ich_/PPER_/ich sehe_/VVFIN_/sehen hier_/ADV_/hier links_/ADV_/links keine_/PIAT_/keine Liebe_/NN_/Liebe und_/KON_/und Bewunderung_/NN_/Bewunderung ._/$._/. (_/$(_/( Lachen_/VVINF_/lachen )_/$(_/) 
[237]  das_/PDS_/die ist_/VAFIN_/sein eine_/ART_/eine andere_/PIAT_/andere Art_/NN_/Art von_/APPR_/von Liebesgeschichte_/NN_/Liebesgeschichte ._/$._/. 
[238]  denn_/KON_/denn diese_/PDAT_/dies Liebesgeschichte_/NN_/Liebesgeschichte ,_/$,_/, 
[239]  mit_/APPR_/mit dem_/ART_/die Verlust_/NN_/Verlust ihrer_/PPOSAT_/ihr Lieben_/NN_/Liebe|Lieben umgehen_/VVINF_/umgehen ?_/$._/? 
[240]  in_/APPR_/in moderner_/ADJA_/modern Liebe_/NN_/Liebe und_/KON_/und in_/APPR_/in einer_/ART_/eine individualistischen_/ADJA_/individualistisch Gesellschaft_/NN_/Gesellschaft ein_/PTKVZ_/ein ._/$._/. 
[241]  Welche_/PIAT_/welche Beziehung_/NN_/Beziehung besteht_/VVFIN_/bestehen zwischen_/APPR_/zwischen Liebe_/NN_/Liebe und_/KON_/und Verlangen_/NN_/Verlangen ?_/$._/? 
[242]  Für_/APPR_/für mich_/PRF_/ich gibt_/VVFIN_/geben es_/PPER_/es ein_/ART_/eine Verb_/NN_/Verb für_/APPR_/für Liebe_/NN_/Liebe ,_/$,_/, und_/KON_/und das_/PDS_/die ist_/VAFIN_/sein "_/$(_/" haben_/VAFIN_/haben "_/$(_/" ._/$._/. 
[243]  In_/APPR_/in der_/ART_/die Liebe_/NN_/Liebe wollen_/VMFIN_/wollen wir_/PPER_/wir haben_/VAINF_/haben ,_/$,_/, da_/KOUS_/da wollen_/VMFIN_/wollen wir_/PPER_/wir den_/ART_/die Geliebten_/NN_/Geliebte kennen_/VVINF_/kennen ,_/$,_/, 
[244]  Fürsorglichkeit_/NN_/Fürsorglichkeit ist_/VAFIN_/sein mächtige_/ADJA_/mächtig Liebe_/NN_/Liebe und_/KON_/und ein_/ART_/eine starkes_/ADJA_/stark Anti-Aphrodisiakum_/NN_/<unknown> ._/$._/. 
[245]  In_/APPR_/in diesem_/PDAT_/dies Paradoxon_/NN_/Paradoxon zwischen_/APPR_/zwischen Liebe_/NN_/Liebe und_/KON_/und Verlangen_/NN_/Verlangen 
[246]  die_/ART_/die Liebe_/NN_/Liebe nähren_/VVFIN_/nähren –_/ADJA_/<unknown> Gegenseitigkeit_/NN_/Gegenseitigkeit ,_/$,_/, Wechselseitigkeit_/NN_/Wechselseitigkeit ,_/$,_/, 
[247]  die_/ART_/die der_/ART_/die Liebe_/NN_/Liebe nicht_/PTKNEG_/nicht immer_/ADV_/immer förderlich_/ADJD_/förderlich sind_/VAFIN_/sein :_/$._/: 
[248]  denn_/ADV_/denn wir_/PPER_/wir denken_/VVFIN_/denken ,_/$,_/, dass_/KOUS_/dass Liebe_/ADJA_/lieb Selbstlosigkeit_/NN_/Selbstlosigkeit ist_/VAFIN_/sein ,_/$,_/, 
[249]  belaste_/VVFIN_/belasten Liebe_/NN_/Liebe mit_/APPR_/mit übermäßiger_/ADJA_/übermäßig Sorge_/NN_/Sorge ,_/$,_/, 
[250]  Erotische_/ADJA_/erotisch Liebespaare_/NN_/Liebespaar wissen_/VVFIN_/wissen ,_/$,_/, dass_/KOUS_/dass Leidenschaft_/NN_/Leidenschaft kommt_/VVFIN_/kommen und_/KON_/und geht_/VVFIN_/gehen ._/$._/. 
[251]  wird_/VAFIN_/werden dich_/PPER_/du ein_/ART_/eine Gefühl_/NN_/Gefühl von_/APPR_/von Liebe_/NN_/Liebe überkommen_/ADJD_/überkommen ,_/$,_/, 
[252]  und_/KON_/und ich_/PPER_/ich war_/VAFIN_/sein bereit_/ADJD_/bereit für_/APPR_/für diese_/PDAT_/dies Truckladung_/NN_/<unknown> voll_/ADJD_/voll Liebe_/NN_/Liebe ,_/$,_/, 
[253]  Ich_/PPER_/ich war_/VAFIN_/sein überwältigt_/ADJD_/überwältigt von_/APPR_/von dem_/ART_/die Gefühl_/NN_/Gefühl der_/ART_/die Liebe_/NN_/Liebe und_/KON_/und Zuneigung_/NN_/Zuneigung für_/APPR_/für meine_/PPOSAT_/mein Frau_/NN_/Frau 
[254]  Wir_/PPER_/wir haben_/VAFIN_/haben die_/ART_/die Entwicklung_/NN_/Entwicklung der_/ART_/die Liebe_/ADJA_/lieb 
[255]  Man_/PIS_/man darf_/VMFIN_/dürfen Liebe_/NN_/Liebe nicht_/PTKNEG_/nicht als_/KOKOM_/als Graph_/NN_/Graph darstellen_/VVINF_/darstellen ._/$._/. 
[256]  Der_/ART_/die Grund_/NN_/Grund ,_/$,_/, warum_/PWAV_/warum wir_/PPER_/wir Liebe_/NN_/Liebe nicht_/PTKNEG_/nicht grafisch_/ADJD_/grafisch darstellen_/VVINF_/darstellen dürfen_/VMFIN_/dürfen ,_/$,_/, 
[257]  ist_/VAFIN_/sein ,_/$,_/, dass_/KOUS_/dass wir_/PPER_/wir Liebe_/NN_/Liebe als_/KOKOM_/als binäre_/ADJA_/binär Kategorie_/NN_/Kategorie wahrnehmen_/VVFIN_/wahrnehmen ._/$._/. 
[258]  Meiner_/PPOSAT_/mein Meinung_/NN_/Meinung nach_/APPO_/nach ist_/VAFIN_/sein Liebe_/NN_/Liebe in_/APPR_/in Wahrheit_/NN_/Wahrheit ein_/ART_/eine Prozess_/NN_/Prozess ._/$._/. 
[259]  Liebe_/NN_/Liebe als_/KOKOM_/als etwas_/PIAT_/etwas Binäres_/NN_/<unknown> zu_/PTKZU_/zu betrachten_/VVINF_/betrachten ,_/$,_/, 
[260]  dass_/KOUS_/dass Liebe_/NN_/Liebe betrügerisch_/ADJD_/betrügerisch ,_/$,_/, inadäquat_/ADJD_/inadäquat oder_/KON_/oder ähnliches_/ADJA_/ähnlich sei_/VAFIN_/sein ._/$._/. 
[261]  Es_/NN_/Es gab_/VVFIN_/geben eine_/ART_/eine sehr_/ADV_/sehr schöne_/ADJA_/schön Beziehung_/NN_/Beziehung zwischen_/APPR_/zwischen der_/ART_/die Liebe_/NN_/Liebe zu_/APPR_/zu Lego_/NE_/<unknown> 
[262]  Es_/NN_/Es gab_/VVFIN_/geben keine_/PIAT_/keine Beziehung_/NN_/Beziehung zwischen_/APPR_/zwischen der_/ART_/die Liebe_/NN_/Liebe zu_/APPR_/zu Lego_/NE_/<unknown> und_/KON_/und der_/ART_/die Anzahl_/NN_/Anzahl der_/ART_/die gebauten_/ADJA_/gebaut Figuren_/NN_/Figur ,_/$,_/, 
[263]  die_/PRELS_/die er_/PPER_/er bald_/ADV_/bald heiraten_/VVINF_/heiraten wollte_/VMFIN_/wollen ._/$._/. Sie_/PPER_/Sie|sie war_/VAFIN_/sein die_/ART_/die Liebe_/NN_/Liebe seines_/PPOSAT_/sein Lebens_/NN_/Leben ._/$._/. 
[264]  "_/$(_/" Sind_/VAFIN_/sein Sie_/PPER_/Sie|sie im_/APPRART_/in Urlaub_/NN_/Urlaub hier_/ADV_/hier ,_/$,_/, meine_/PPOSAT_/mein Liebe_/NN_/Liebe ?_/$._/? "_/$(_/" 
[265]  mit_/APPR_/mit Liebe_/NN_/Liebe gemacht_/VVPP_/machen ._/$._/. 
[266]  "_/$(_/" Von_/APPR_/von 52_/CARD_/52 an_/APPR_/an 48_/CARD_/48 in_/APPR_/in Liebe_/NN_/Liebe "_/$(_/" ,_/$,_/, 
[267]  "_/$(_/" Liebe_/NN_/Liebe 48_/CARD_/48 ,_/$,_/, ich_/PPER_/ich verspreche_/VVFIN_/versprechen hiermit_/PAV_/hiermit ,_/$,_/, euch_/PPER_/ihr zuzuhören_/VVIZU_/zuhören ,_/$,_/, für_/APPR_/für euch_/PPER_/ihr zu_/PTKZU_/zu kämpfen_/VVINF_/kämpfen und_/KON_/und euch_/PPER_/ihr jeder_/PIAT_/jede Zeit_/NN_/Zeit zu_/PTKZU_/zu respektieren_/VVINF_/respektieren ._/$._/. "_/$(_/" 
[268]  Ich_/PPER_/ich bringe_/VVFIN_/bringen gern_/ADV_/gern ein_/ART_/eine wenig_/ADV_/wenig mehr_/ADV_/mehr Liebe_/NN_/Liebe in_/APPR_/in die_/ART_/die Welt_/NN_/Welt ,_/$,_/, 
[269]  "_/$(_/" Liebes_/ADJA_/lieb ,_/$,_/, trag_/VVIMP_/tragen keine_/PIAT_/keine Unterwäsche_/NN_/Unterwäsche unter_/APPR_/unter Deinem_/PPOSAT_/dein Pyjama_/NN_/Pyjama ,_/$,_/, 


