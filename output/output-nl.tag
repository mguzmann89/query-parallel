Language: nl.tag

[1]  laat_/verbpressg_/laten ons_/det__poss_/ons minnaars_/nounpl_/minnaar en_/conjcoord_/en beminden_/nounpl_/beminde zijn_/verbprespl_/zijn ;_/punc_/; 
[2]  geïnteresseerd_/verbpapa_/interesseren waren_/verbpastpl_/wezen|zijn in_/prep_/in de_/det__art_/de liefde_/nounsg_/liefde ,_/punc_/, niet_/adv_/niet in_/prep_/in oorlog_/nounsg_/oorlog ._/$._/. 
[3]  laat_/verbpressg_/laten ons_/det__poss_/ons minnaars_/nounpl_/minnaar en_/conjcoord_/en beminden_/nounpl_/beminde zijn_/verbprespl_/zijn ;_/punc_/; 
[4]  geïnteresseerd_/verbpapa_/interesseren waren_/verbpastpl_/wezen|zijn in_/prep_/in de_/det__art_/de liefde_/nounsg_/liefde ,_/punc_/, niet_/adv_/niet in_/prep_/in oorlog_/nounsg_/oorlog ._/$._/. 
[5]  maar_/conjcoord_/maar de_/det__art_/de muziek_/nounsg_/muziek had_/verbpastsg_/hebben opgegeven_/verbpapa_/opgeven om_/conjsubo_/om carrière_/nounsg_/carrière te_/partte_/te maken_/verbinf_/maken 
[6]  Dat_/adj_/<unknown> woord_/nounsg_/woord ,_/punc_/, mijn_/det__poss_/mijn liefste_/nounsg_/liefste ,_/punc_/, zal_/verbpressg_/zullen '_/punc_/' Basingstoke_/nounsg_/<unknown> '_/punc_/' zijn_/verbprespl_/zijn ._/$._/. '_/punc_/' 
[7]  als_/conjsubo_/als we_/pronpers_/we niet_/adv_/niet verwachten_/verbinf_/verwachten om_/conjsubo_/om liefde_/nounsg_/liefde ,_/punc_/, gezondheid_/nounsg_/gezondheid en_/conjcoord_/en succes_/nounsg_/succes te_/partte_/te vinden_/verbinf_/vinden 
[8]  liefde_/nounsg_/liefde ,_/punc_/, normen_/nounpl_/norm en_/conjcoord_/en waarden_/nounpl_/waard|waarde ,_/punc_/, 
[9]  Dat_/nounsg_/<unknown> alles_/pronindef_/alles verpakt_/verbpapa_/verpakken in_/prep_/in de_/det__art_/de onvoorwaardelijke_/adj_/onvoorwaardelijk liefde_/nounsg_/liefde 
[10]  dit_/prondemo_/dit is_/verbpressg_/zijn de_/det__art_/de liefde_/nounsg_/liefde van_/prep_/van mijn_/det__poss_/mijn leven_/nounsg_/leven ,_/punc_/, mijn_/det__poss_/mijn dochter_/nounsg_/dochter Jay_/nounsg_/<unknown> ._/$._/. 
[11]  De_/nounpl_/<unknown> dichter_/adj_/dicht Auden_/nounsg_/<unknown> zei_/verbpastsg_/zeggen :_/$._/: "_/punc_/" Duizenden_/num__card_/duizend hebben_/verbprespl_/hebben geleefd_/verbpapa_/leven zonder_/prep_/zonder liefde_/nounsg_/liefde ._/$._/. 
[12]  Nog_/nounsg_/<unknown> één_/det__art_/één laatste_/adj_/laat ding_/nounsg_/ding :_/$._/: als_/conjsubo_/als je_/pronpers_/je waarde_/nounsg_/waarde hecht_/verbpressg_/hechten aan_/adv_/aan dingen_/verbinf_/dingen zoals_/conjsubo_/zoals gezondheid_/nounsg_/gezondheid ,_/punc_/, 
[13]  over_/prep_/over hun_/det__poss_/hun geliefden_/adj_/geliefd 
[14]  hoe_/pronadv_/hoe meer_/adv_/meer ik_/pronpers_/ik het_/pronpers_/het als_/conjsubo_/als een_/det__art_/een soort_/nounsg_/soort liefde_/nounsg_/liefde zie_/verbpressg_/zien ._/$._/. 
[15]  En_/nounpl_/<unknown> m'n_/pronposs_/m'n liefde_/nounsg_/liefde voor_/prep_/voor science_/adj_/<unknown> fiction_/nounsg_/<unknown> ,_/punc_/, 
[16]  En_/nounpl_/<unknown> m'n_/pronposs_/m'n doorlopende_/adj_/doorlopend liefde_/nounsg_/liefde voor_/prep_/voor de_/det__art_/de oceaan_/nounsg_/oceaan 
[17]  Het_/nounsg_/<unknown> wordt_/verbpressg_/worden een_/det__art_/een epische_/adj_/episch romantische_/adj_/romantisch ,_/punc_/, 
[18]  ..._/$._/... de_/det__art_/de mogelijkheid_/nounsg_/mogelijkheid van_/prep_/van liefde_/nounsg_/liefde --_/punc_/-- 
[19]  Ben_/nounsg_/<unknown> je_/pronpers_/je bereid_/verbpressg_/bereiden dit_/prondemo_/dit in_/adv_/in te_/partte_/te brengen_/verbinf_/brengen in_/prep_/in je_/pronpers_/je geliefde_/adj_/geliefd ,_/punc_/, je_/pronpers_/je eigen_/adj_/eigen kind_/nounsg_/kind ,_/punc_/, 
[20]  maar_/conjcoord_/maar hij_/pronpers_/hij had_/verbpastsg_/hebben een_/det__art_/een vriendin_/nounsg_/vriendin in_/prep_/in Engeland_/nounprop_/Engeland ._/$._/. Ze_/pronpers_/ze was_/verbpastsg_/wezen|zijn de_/det__art_/de liefde_/nounsg_/liefde van_/prep_/van zijn_/det__poss_/zijn leven_/nounsg_/leven ,_/punc_/, Sarah_/nounsg_/<unknown> ._/$._/. 
[21]  De_/nounpl_/<unknown> kleine_/adj_/klein daad_/nounsg_/daad van_/prep_/van liefde_/nounsg_/liefde van_/prep_/van mijn_/det__poss_/mijn pleegvader_/nounsg_/pleegvader 
[22]  met_/prep_/met zelfs_/adv_/zelfs de_/det__art_/de kleinste_/adj_/klein daad_/nounsg_/daad van_/prep_/van liefde_/nounsg_/liefde ._/$._/. 
[23]  dat_/conjsubo_/dat je_/pronpers_/je daad_/nounsg_/daad van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en zorg_/nounsg_/zorg 
[24]  voorstellen_/verbinf_/voorstellen dat_/conjsubo_/dat m'n_/pronposs_/m'n liefde_/nounsg_/liefde voor_/prep_/voor auto's_/nounpl_/auto en_/conjcoord_/en 
[25]  Het_/nounsg_/<unknown> was_/verbpastsg_/wezen|zijn een_/det__art_/een zomer_/nounsg_/zomer van_/prep_/van liefde_/nounsg_/liefde als_/conjsubo_/als je_/pronpers_/je blank_/adj_/blank was_/verbpastsg_/wezen|zijn ._/$._/. 
[26]  Mijn_/nounsg_/mijn pleegmoeder_/nounsg_/pleegmoeder vroeg_/verbpastsg_/vragen me_/pronpers_/me om_/conjsubo_/om eens_/adv_/eens na_/adv_/na te_/partte_/te denken_/verbinf_/denken over_/prep_/over wat_/pronrel_/wat liefde_/nounsg_/liefde is_/verbpressg_/zijn ,_/punc_/, 
[27]  bij_/prep_/bij het_/det__art_/het zoeken_/nounsg_/zoeken naar_/prep_/naar liefde_/nounsg_/liefde ,_/punc_/, een_/det__art_/een plek_/nounsg_/plek waar_/pronadv_/waar dysfunctie_/nounsg_/<unknown> 
[28]  Liefde_/nounsg_/liefde en_/conjcoord_/en vrede_/nounsg_/vrede zijn_/verbprespl_/zijn vermeld_/verbpapa_/vermelden ._/$._/. 
[29]  CA_/nounpl_/<unknown> :_/$._/: Liefde_/nounsg_/liefde en_/conjcoord_/en vrede_/nounsg_/vrede zijn_/verbprespl_/zijn vermeld_/verbpapa_/vermelden 
[30]  Mapendo_/adv_/<unknown> betekent_/verbpressg_/betekenen in_/prep_/in Swahili_/nounprop_/Swahili grote_/adj_/groot liefde_/nounsg_/liefde ._/$._/. 
[31]  "_/punc_/" Liefde_/nounsg_/liefde en_/conjcoord_/en medeleven_/nounsg_/medeleven zijn_/verbprespl_/zijn noodzakelijke_/adj_/noodzakelijk dingen_/verbinf_/dingen ._/$._/. 
[32]  in_/prep_/in de_/det__art_/de Boeddha_/nounprop_/Boeddha van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en genegenheid_/nounsg_/genegenheid ._/$._/. 
[33]  Het_/nounsg_/<unknown> was_/verbpastsg_/wezen|zijn energie_/nounsg_/energie ,_/punc_/, liefde_/nounsg_/liefde en_/conjcoord_/en vreugde_/nounsg_/vreugde ._/$._/. 
[34]  zijn_/det__poss_/zijn liefde_/nounsg_/liefde en_/conjcoord_/en werk_/nounsg_/werk ._/$._/. 
[35]  Liefde_/nounsg_/liefde :_/$._/: het_/det__art_/het succesvol_/adj_/succesvol onderhouden_/verbpapa_/onderhouden 
[36]  Er_/nounsg_/<unknown> hangt_/verbpressg_/hangen veel_/adv_/veel liefde_/nounsg_/liefde in_/prep_/in de_/det__art_/de lucht_/nounsg_/lucht ._/$._/. 
[37]  Mijn_/nounsg_/mijn hele_/adj_/heel leven_/nounsg_/leven lang_/adj_/lang ben_/verbpressg_/zijn ik_/pronpers_/ik al_/adv_/al bezeten_/verbpapa_/bezitten van_/prep_/van schrijven_/nounsg_/schrijven 
[38]  omdat_/conjsubo_/omdat we_/pronpers_/we uit_/prep_/uit pure_/adj_/puur liefde_/nounsg_/liefde voor_/prep_/voor de_/det__art_/de mens_/nounsg_/mens ,_/punc_/, uit_/prep_/uit eigenwijsheid_/adj_/<unknown> 
[39]  Dit_/adj_/<unknown> project_/nounsg_/project heet_/verbpressg_/heten "_/punc_/" Liefdesnestjes_/adj_/<unknown> ”_/nounpl_/<unknown> ._/$._/. 
[40]  Deze_/nounsg_/<unknown> heet_/verbpressg_/heten “_/adj_/<unknown> Liefdeslokjesnest_/adj_/<unknown> ”_/nounpl_/<unknown> ._/$._/. 
[41]  Deze_/nounsg_/<unknown> heet_/verbpressg_/heten "_/punc_/" Mixtape-liefdesliedsnest_/adj_/<unknown> ”_/nounpl_/<unknown> ._/$._/. 
[42]  Deze_/nounsg_/<unknown> heet_/verbpressg_/heten "_/punc_/" Vrijersnest_/nounprop_/<unknown> "_/punc_/" ._/$._/. 
[43]  Maar_/nounsg_/maar ik_/pronpers_/ik leerde_/verbpastsg_/leren vooral_/adv_/vooral over_/prep_/over liefde_/nounsg_/liefde ._/$._/. 
[44]  Liefde_/nounsg_/liefde ontdek_/verbpressg_/ontdekken je_/pronpers_/je ._/$._/. 
[45]  Het_/nounsg_/<unknown> gaat_/verbpressg_/gaan eerst_/adv_/eerst om_/conjsubo_/om de_/det__art_/de liefde_/nounsg_/liefde ._/$._/. 
[46]  hetgeen_/pronrel_/hetgeen wij_/pronpers_/wij liefde_/nounsg_/liefde noemen_/verbinf_/noemen ._/$._/. 
[47]  en_/conjcoord_/en deze_/det__demo_/deze betrokkenheid_/nounsg_/betrokkenheid ,_/punc_/, deze_/det__demo_/deze liefde_/nounsg_/liefde ,_/punc_/, dit_/prondemo_/dit respect_/nounsg_/respect 
[48]  en_/conjcoord_/en soms_/adv_/soms zelfs_/adv_/zelfs op_/prep_/op aspecten_/nounpl_/aspect van_/prep_/van liefde_/nounsg_/liefde ._/$._/. 
[49]  "_/punc_/" Diepgang_/nounsg_/diepgang is_/verbpressg_/zijn beter_/adj_/beter|goed 
[50]  mededogen_/nounsg_/mededogen en_/conjcoord_/en liefde_/nounsg_/liefde ,_/punc_/, 
[51]  liefde_/nounsg_/liefde ,_/punc_/, mededogen_/nounsg_/mededogen ,_/punc_/, verbeelding_/nounsg_/verbeelding ,_/punc_/, 
[52]  en_/conjcoord_/en van_/prep_/van de_/det__art_/de mensen_/nounpl_/mens om_/conjsubo_/om je_/pronpers_/je heen_/adv_/heen en_/conjcoord_/en van_/prep_/van liefde_/nounsg_/liefde ._/$._/. 
[53]  Anonymous_/nounpl_/<unknown> :_/$._/: Beste_/adj_/goed Fox_/adj_/<unknown> News_/nounpl_/<unknown> ,_/punc_/, 
[54]  Ik_/nounsg_/ik ben_/verbpressg_/zijn hier_/adv_/hier om_/conjsubo_/om het_/det__art_/het verhaal_/nounsg_/verhaal te_/partte_/te vertellen_/verbinf_/vertellen van_/prep_/van geschifte_/adj_/geschift liefde_/nounsg_/liefde ,_/punc_/, 
[55]  een_/det__art_/een psychologische_/adj_/psychologisch val_/nounsg_/val vermomd_/verbpapa_/vermommen als_/conjsubo_/als liefde_/nounsg_/liefde ,_/punc_/, 
[56]  Ik_/nounpl_/<unknown> kon_/verbpastsg_/kunnen mijn_/det__poss_/mijn geschifte_/adj_/geschift liefdesverhaal_/nounsg_/<unknown> beëindigen_/verbprespl_/beëindigen 
[57]  een_/det__art_/een liefdesgedicht_/nounsg_/liefdesgedicht zoals_/conjsubo_/zoals ik_/pronpers_/ik nog_/adv_/nog nooit_/adv_/nooit had_/verbpastsg_/hebben gehoord_/verbpapa_/horen ._/$._/. 
[58]  loopt_/verbpressg_/lopen over_/prep_/over een_/det__art_/een belofte_/nounsg_/belofte van_/prep_/van seks_/nounsg_/seks en_/conjcoord_/en liefde_/nounsg_/liefde ._/$._/. 
[59]  maar_/conjcoord_/maar liefde_/nounsg_/liefde was_/verbpastsg_/wezen|zijn daar_/adv_/daar niet_/adv_/niet bij_/adv_/bij ._/$._/. 
[60]  haar_/det__poss_/haar naam_/nounsg_/naam betekent_/verbpressg_/betekenen liefde_/nounsg_/liefde --_/punc_/-- 
[61]  en_/conjcoord_/en liefde_/nounsg_/liefde en_/conjcoord_/en mededogen_/nounsg_/mededogen ,_/punc_/, die_/pronrel_/die leiden_/verbprespl_/leiden tot_/prep_/tot respect_/nounsg_/respect voor_/prep_/voor alle_/det__indef_/alle leven_/nounsg_/leven ._/$._/. 
[62]  zoveel_/num__card_/zoveel liefde_/nounsg_/liefde en_/conjcoord_/en mededogen_/nounsg_/mededogen ._/$._/. 
[63]  Je_/nounpl_/<unknown> gaf_/verbpastsg_/geven ze_/pronpers_/ze liefde_/nounsg_/liefde en_/conjcoord_/en vreugde_/nounsg_/vreugde ,_/punc_/, je_/pronpers_/je was_/verbpastsg_/wezen|zijn er_/pronadv_/er ..._/$._/... 
[64]  met_/prep_/met al_/adv_/al deze_/det__demo_/deze liefde_/nounsg_/liefde ,_/punc_/, opvoeding_/nounsg_/opvoeding ,_/punc_/, geld_/nounsg_/geld en_/conjcoord_/en achtergrond_/nounsg_/achtergrond 
[65]  en_/conjcoord_/en werd_/verbpastsg_/worden je_/pronpers_/je daar_/adv_/daar verliefd_/adj_/verliefd ._/$._/. 
[66]  Dit_/nounsg_/<unknown> hebben_/verbprespl_/hebben we_/pronpers_/we echt_/adv_/echt nodig_/adj_/nodig :_/$._/: verbinding_/nounsg_/verbinding en_/conjcoord_/en liefde_/nounsg_/liefde --_/punc_/-- de_/det__art_/de vierde_/num__ord_/vier behoefte_/nounsg_/behoefte ._/$._/. 
[67]  omdat_/conjsubo_/omdat liefde_/nounsg_/liefde te_/partte_/te eng_/adj_/eng is_/verbpressg_/zijn ._/$._/. Ze_/pronpers_/ze willen_/verbprespl_/willen niet_/adv_/niet gekwetst_/verbpapa_/kwetsen worden_/verbinf_/worden ._/$._/. 
[68]  of_/conjcoord_/of liefde_/nounsg_/liefde ?_/$._/? We_/pronpers_/we hebben_/verbprespl_/hebben ze_/pronpers_/ze alle_/det__indef_/alle zes_/num__card_/zes nodig_/adj_/nodig ,_/punc_/, maar_/conjcoord_/maar wat_/pronrel_/wat ook_/adv_/ook 
[69]  Elk_/nounsg_/<unknown> van_/prep_/van hen_/pronpers_/hen heeft_/verbpressg_/hebben een_/det__art_/een romantische_/adj_/romantisch relatie_/nounsg_/relatie 
[70]  een_/det__art_/een leven_/nounsg_/leven waarin_/pronadv_/waarin op_/prep_/op je_/pronpers_/je werk_/nounsg_/werk ,_/punc_/, in_/prep_/in je_/pronpers_/je ouderschap_/nounsg_/ouderschap ,_/punc_/, in_/prep_/in de_/det__art_/de liefde_/nounsg_/liefde en_/conjcoord_/en in_/prep_/in je_/pronpers_/je vrije_/adj_/vrij tijd_/nounsg_/tijd ,_/punc_/, de_/det__art_/de tijd_/nounsg_/tijd voor_/prep_/voor je_/pronpers_/je stopt_/verbpressg_/stoppen ._/$._/. 
[71]  Maar_/nounpl_/<unknown> in_/prep_/in de_/det__art_/de derde_/num__ord_/drie grote_/adj_/groot arena_/nounsg_/arena van_/prep_/van het_/det__art_/het leven_/nounsg_/leven ,_/punc_/, de_/det__art_/de liefde_/nounsg_/liefde ,_/punc_/, is_/verbpressg_/zijn Len_/adj_/<unknown> een_/det__art_/een verschrikkelijke_/adj_/verschrikkelijk mislukking_/nounsg_/mislukking ._/$._/. 
[72]  Je_/nounsg_/<unknown> kan_/verbpressg_/kunnen je_/pronpers_/je werk_/nounsg_/werk ,_/punc_/, je_/pronpers_/je liefdesleven_/nounsg_/liefdesleven ,_/punc_/, 
[73]  Erez_/nounsg_/<unknown> Lieberman_/nounsg_/<unknown> Aiden_/nounsg_/<unknown> :_/$._/: Iedereen_/pronindef_/iedereen weet_/verbpressg_/weten 
[74]  ooit_/adv_/ooit een_/det__art_/een schaduw_/nounsg_/schaduw zal_/verbpressg_/zullen werpen_/verbinf_/werpen 
[75]  Hij_/nounsg_/<unknown> schreef_/verbpastsg_/schrijven dit_/prondemo_/dit aan_/prep_/aan zijn_/det__poss_/zijn geliefde_/nounsg_/geliefde ,_/punc_/, 
[76]  Het_/nounsg_/<unknown> is_/verbpressg_/zijn een_/det__art_/een uiting_/nounsg_/uiting van_/prep_/van liefde_/nounsg_/liefde ._/$._/. --_/punc_/-- 
[77]  Kunst_/nounsg_/kunst in_/prep_/in de_/det__art_/de platonische_/adj_/platonisch betekenis_/nounsg_/betekenis is_/verbpressg_/zijn waarheid_/nounsg_/waarheid ,_/punc_/, het_/pronpers_/het is_/verbpressg_/zijn schoonheid_/nounsg_/schoonheid en_/conjcoord_/en liefde_/nounsg_/liefde ._/$._/. 
[78]  We_/nounpl_/<unknown> hebben_/verbprespl_/hebben niet_/adv_/niet echt_/adv_/echt een_/det__art_/een probleem_/nounsg_/probleem met_/prep_/met spreken_/nounsg_/spreken over_/prep_/over liefde_/nounsg_/liefde ._/$._/. 
[79]  Voor_/nounsg_/voor mij_/pronpers_/mij zijn_/verbprespl_/zijn deze_/det__demo_/deze ervaring_/nounsg_/ervaring van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en de_/det__art_/de ervaring_/nounsg_/ervaring van_/prep_/van ontwerp_/nounsg_/ontwerp 
[80]  Ik_/nounpl_/<unknown> ontdekte_/verbpastsg_/ontdekken iets_/adv_/iets over_/prep_/over liefde_/nounsg_/liefde en_/conjcoord_/en ontwerp_/nounsg_/ontwerp door_/prep_/door een_/det__art_/een project_/nounsg_/project met_/prep_/met de_/det__art_/de naam_/nounsg_/naam Deep_/nounsg_/<unknown> Blue_/nounsg_/<unknown> ._/$._/. 
[81]  Je_/nounpl_/<unknown> kan_/verbpressg_/kunnen het_/det__art_/het woord_/nounsg_/woord '_/punc_/' liefde_/nounsg_/liefde '_/punc_/' uit_/adv_/uit heel_/adv_/heel wat_/pronindef_/wat dingen_/verbinf_/dingen in_/prep_/in onze_/det__poss_/ons maatschappij_/nounsg_/maatschappij weghalen_/verbprespl_/weghalen 
[82]  Hij_/nounsg_/<unknown> zei_/verbpastsg_/zeggen :_/$._/: "_/punc_/" Liefde_/nounsg_/liefde is_/verbpressg_/zijn niet_/adv_/niet egoïstisch_/adj_/egoïstisch ._/$._/. 
[83]  Liefde_/nounsg_/liefde betekent_/verbpressg_/betekenen niet_/adv_/niet tellen_/verbinf_/tellen hoeveel_/num__card_/hoeveel keer_/nounsg_/keer ik_/pronpers_/ik zeg_/verbpressg_/zeggen '_/punc_/' Ik_/adj_/<unknown> hou_/nounsg_/<unknown> van_/prep_/van je_/pronpers_/je '_/punc_/' ._/$._/. 
[84]  Liefde_/nounsg_/liefde is_/verbpressg_/zijn niet_/adv_/niet egoïstisch_/adj_/egoïstisch ._/$._/. "_/punc_/" Daar_/adv_/daar dacht_/verbpastsg_/denken ik_/pronpers_/ik aan_/adv_/aan ,_/punc_/, en_/conjcoord_/en ik_/pronpers_/ik bedacht_/verbpastsg_/bedenken :_/$._/: 
[85]  "_/punc_/" Ik_/adj_/<unknown> toon_/nounsg_/toon hier_/adv_/hier geen_/det__indef_/geen liefde_/nounsg_/liefde ._/$._/. Ik_/pronpers_/ik toon_/verbpressg_/tonen hier_/adv_/hier allesbehalve_/adv_/allesbehalve liefde_/nounsg_/liefde ._/$._/. 
[86]  We_/nounsg_/<unknown> zetten_/verbinf_/zetten de_/det__art_/de auto_/nounsg_/auto opnieuw_/adv_/opnieuw in_/prep_/in het_/det__art_/het midden_/nounsg_/midden van_/prep_/van onze_/det__poss_/ons gedachten_/nounpl_/gedachte ._/$._/. We_/pronpers_/we stelden_/verbpastpl_/stellen liefde_/nounsg_/liefde 
[87]  Er_/nounsg_/<unknown> kan_/verbpressg_/kunnen veel_/adv_/veel gezegd_/verbpapa_/zeggen worden_/verbprespl_/worden over_/prep_/over vertrouwen_/nounsg_/vertrouwen en_/conjcoord_/en liefde_/nounsg_/liefde ,_/punc_/, 
[88]  Het_/nounsg_/<unknown> vertrouwen_/verbinf_/vertrouwen en_/conjcoord_/en de_/det__art_/de liefde_/nounsg_/liefde maken_/verbprespl_/maken het_/pronpers_/het de_/det__art_/de moeite_/nounsg_/moeite ._/$._/. 
[89]  "_/punc_/" Lieve_/adj_/lief Jezus_/int_/Jezus ,_/punc_/, dit_/det__demo_/dit kind_/nounsg_/kind is_/verbpressg_/zijn al_/adv_/al begonnen_/verbpapa_/beginnen ._/$._/. "_/punc_/" 
[90]  en_/conjcoord_/en het_/pronpers_/het was_/verbpastsg_/wezen|zijn liefde_/nounsg_/liefde op_/prep_/op het_/det__art_/het eerste_/num__ord_/een zicht_/nounsg_/zicht ._/$._/. 
[91]  Hoe_/nounpl_/<unknown> begint_/verbpressg_/beginnen het_/pronpers_/het ?_/$._/? Bekijk_/nounsg_/bekijk José's_/nounpl_/<unknown> aandacht_/nounsg_/aandacht voor_/prep_/voor detail_/nounsg_/detail ._/$._/. 
[92]  Maar_/nounpl_/<unknown> belangrijker_/adj_/belangrijk ,_/punc_/, hou_/adj_/<unknown> van_/prep_/van hen_/pronpers_/hen ._/$._/. 
[93]  Niets_/nounsg_/niets werkt_/verbpressg_/werken zo_/adv_/zo goed_/adj_/goed als_/conjsubo_/als onvoorwaardelijke_/adj_/onvoorwaardelijk liefde_/nounsg_/liefde ._/$._/. 
[94]  weet_/verbpressg_/weten dat_/conjsubo_/dat liefde_/nounsg_/liefde dingen_/verbinf_/dingen tot_/prep_/tot leven_/nounsg_/leven brengt_/verbpressg_/brengen ._/$._/. 
[95]  Passie_/nounsg_/passie is_/verbpressg_/zijn je_/pronpers_/je grootste_/adj_/groot|groots liefde_/nounsg_/liefde ._/$._/. 
[96]  grootste_/adj_/groot|groots liefde_/nounsg_/liefde gevonden_/verbpapa_/vinden ,_/punc_/, vergeleken_/verbpapa_/vergelijken met_/prep_/met alles_/pronindef_/alles 
[97]  van_/prep_/van zijn_/det__poss_/zijn liefde_/nounsg_/liefde voor_/prep_/voor haar_/det__poss_/haar ._/$._/. 
[98]  aan_/adv_/aan slechts_/adv_/slechts een_/det__art_/een ding_/nounsg_/ding :_/$._/: liefde_/nounsg_/liefde ,_/punc_/, 
[99]  liefde_/nounsg_/liefde voor_/prep_/voor het_/det__art_/het land_/nounsg_/land ,_/punc_/, voor_/prep_/voor de_/det__art_/de heuvel_/nounsg_/heuvel 
[100]  liefde_/nounsg_/liefde en_/conjcoord_/en respect_/nounsg_/respect 
[101]  liefde_/nounsg_/liefde en_/conjcoord_/en respect_/nounsg_/respect voor_/prep_/voor jezelf_/pronrefl_/jezelf ._/$._/. 
[102]  dat_/pronrel_/dat wil_/nounsg_/wil zeggen_/verbprespl_/zeggen :_/$._/: mensen_/nounpl_/mens die_/pronrel_/die het_/pronpers_/het doen_/verbprespl_/doen voor_/prep_/voor hun_/det__poss_/hun plezier_/nounsg_/plezier ,_/punc_/, 
[103]  laat_/verbpressg_/laten ons_/det__poss_/ons minnaars_/nounpl_/minnaar en_/conjcoord_/en beminden_/nounpl_/beminde zijn_/verbprespl_/zijn ;_/punc_/; 
[104]  na_/prep_/na een_/det__art_/een klacht_/nounsg_/klacht van_/prep_/van VS-senator_/nounsg_/<unknown> Joe_/adj_/<unknown> Lieberman_/nounsg_/<unknown> ,_/punc_/, 
[105]  leren_/verbprespl_/leren ons_/pronpers_/ons ook_/adv_/ook om_/conjsubo_/om lief_/adj_/lief te_/partte_/te hebben_/verbinf_/hebben en_/conjcoord_/en vertrouwen_/verbprespl_/vertrouwen te_/partte_/te hebben_/verbinf_/hebben ._/$._/. 
[106]  en_/conjcoord_/en die_/det__demo_/die liefde_/nounsg_/liefde was_/verbpastsg_/wezen|zijn voelbaar_/adj_/voelbaar in_/prep_/in de_/det__art_/de preken_/nounpl_/preek die_/pronrel_/die hij_/pronpers_/hij elke_/det__indef_/elk week_/nounsg_/week gaf_/verbpastsg_/geven ,_/punc_/, 
[107]  die_/pronrel_/die tevens_/adv_/tevens fan_/nounsg_/fan waren_/verbpastpl_/wezen|zijn ,_/punc_/, of_/conjcoord_/of ze_/pronpers_/ze in_/prep_/in ruil_/nounsg_/ruil voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, tickets_/nounpl_/ticket en_/conjcoord_/en bier_/nounsg_/bier 
[108]  een_/det__art_/een 45-jarig_/adj_/<unknown> liefdesverhaal_/nounsg_/<unknown> 
[109]  en_/conjcoord_/en tonen_/verbprespl_/tonen er_/pronadv_/er zeker_/adj_/zeker geen_/det__indef_/geen liefde_/nounsg_/liefde voor_/adv_/voor ._/$._/. 
[110]  We_/nounpl_/<unknown> zijn_/verbprespl_/zijn bang_/adj_/bang ,_/punc_/, net_/adv_/net zoals_/conjsubo_/zoals prille_/adj_/pril geliefden_/nounpl_/geliefde ,_/punc_/, 
[111]  als_/conjsubo_/als je_/pronpers_/je mensen_/nounpl_/mens vraagt_/verbpressg_/vragen naar_/prep_/naar liefde_/nounsg_/liefde ,_/punc_/, 
[112]  deze_/det__demo_/deze mensen_/nounpl_/mens hebben_/verbprespl_/hebben een_/det__art_/een heel_/adj_/heel sterk_/adj_/sterk besef_/nounsg_/besef van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en erbij_/pronadv_/erbij horen_/nounpl_/horen --_/punc_/-- 
[113]  een_/det__art_/een sterk_/adj_/sterk besef_/nounsg_/besef van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en erbij_/pronadv_/erbij horen_/nounpl_/horen hebben_/verbprespl_/hebben 
[114]  een_/det__art_/een sterk_/adj_/sterk besef_/nounsg_/besef van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en erbij_/pronadv_/erbij horen_/nounpl_/horen hebben_/verbprespl_/hebben 
[115]  geloven_/verbinf_/geloven dat_/conjsubo_/dat ze_/pronpers_/ze liefde_/nounsg_/liefde en_/conjcoord_/en erbij_/pronadv_/erbij horen_/nounpl_/horen waard_/adj_/waard zijn_/verbprespl_/zijn ._/$._/. 
[116]  van_/prep_/van erbij_/pronadv_/erbij horen_/verbinf_/horen ,_/punc_/, van_/prep_/van liefde_/nounsg_/liefde ._/$._/. 
[117]  maar_/conjcoord_/maar je_/pronpers_/je bent_/verbpressg_/zijn het_/det__art_/het waard_/nounsg_/waard om_/conjsubo_/om lief_/adj_/lief te_/partte_/te hebben_/verbinf_/hebben en_/conjcoord_/en erbij_/pronadv_/erbij te_/partte_/te horen_/verbinf_/horen ._/$._/. "_/punc_/" 
[118]  "_/punc_/" Ware_/adj_/waar liefde_/nounsg_/liefde gevonden_/verbpapa_/vinden ,_/punc_/, huwde_/verbpastsg_/huwen een_/det__art_/een ander_/adj_/ander ._/$._/. "_/punc_/" 
[119]  Houden_/adj_/<unknown> mensen_/nounpl_/mens in_/prep_/in deze_/det__demo_/deze systemen_/nounpl_/systeem 
[120]  dan_/conjsubo_/dan kun_/nounsg_/<unknown> je_/pronpers_/je waarschijnlijk_/adj_/waarschijnlijk spreken_/verbprespl_/spreken van_/prep_/van de_/det__art_/de emotie_/nounsg_/emotie liefde_/nounsg_/liefde ._/$._/. 
[121]  vol_/adj_/vol met_/prep_/met pret_/nounsg_/pret en_/conjcoord_/en liefde_/nounsg_/liefde ,_/punc_/, in_/prep_/in een_/det__art_/een zeer_/adj_/zeer hechte_/adj_/hecht familie_/nounsg_/familie ._/$._/. 
[122]  als_/conjsubo_/als liefde_/nounsg_/liefde en_/conjcoord_/en vriendelijkheid_/nounsg_/vriendelijkheid 
[123]  een_/det__art_/een Bosnisch_/adj_/Bosnisch liefdeslied_/nounsg_/liefdeslied 
[124]  en_/conjcoord_/en in_/prep_/in de_/det__art_/de relaties_/nounpl_/relatie tussen_/prep_/tussen geliefden_/nounpl_/geliefde en_/conjcoord_/en ouders_/nounpl_/ouder 
[125]  zou_/verbpastsg_/zullen ik_/pronpers_/ik een_/det__art_/een ruimte_/nounsg_/ruimte maken_/verbinf_/maken voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, voor_/prep_/voor gulheid_/nounsg_/gulheid ..._/$._/... 
[126]  Ik_/nounsg_/ik weet_/verbpressg_/weten dat_/conjsubo_/dat dit_/prondemo_/dit nogal_/adv_/nogal cru_/adj_/cru klinkt_/verbpressg_/klinken ,_/punc_/, 
[127]  (_/punc_/( Video_/nounpl_/<unknown> )_/punc_/) John_/adj_/<unknown> Edwards_/nounpl_/<unknown> :_/$._/: Ik_/pronpers_/ik werk_/verbpressg_/werken er_/pronadv_/er graag_/adv_/graag aan_/prep_/aan mee_/adv_/mee ._/$._/. 
[128]  of_/conjcoord_/of respect_/nounsg_/respect of_/conjcoord_/of liefde_/nounsg_/liefde ._/$._/. 
[129]  op_/adv_/op te_/partte_/te groeien_/verbinf_/groeien en_/conjcoord_/en te_/partte_/te trouwen_/verbinf_/trouwen met_/prep_/met je_/pronpers_/je liefje_/nounsg_/liefje van_/prep_/van de_/det__art_/de middelbare_/adj_/middelbaar school_/nounsg_/school ,_/punc_/, 
[130]  weggedreven_/nounsg_/<unknown> lijkt_/verbpressg_/lijken uit_/prep_/uit een_/det__art_/een liefdesgedicht_/nounsg_/liefdesgedicht 
[131]  Je_/nounsg_/<unknown> kunt_/verbpressg_/kunnen van_/prep_/van liefde_/nounsg_/liefde naar_/prep_/naar haat_/nounsg_/haat gaan_/verbinf_/gaan ._/$._/. 
[132]  Publiek_/nounsg_/publiek :_/$._/: '_/punc_/' Feest_/nounsg_/feest ._/$._/. Liefde_/nounsg_/liefde ._/$._/. '_/punc_/' 
[133]  Na_/nounsg_/<unknown> 1300_/num__ord_/@card@ jaar_/nounsg_/jaar ,_/punc_/, strelen_/verbprespl_/strelen en_/conjcoord_/en kussen_/verbprespl_/kussen 
[134]  Ze_/nounpl_/<unknown> zingen_/verbprespl_/zingen voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, ze_/pronpers_/ze dansen_/verbprespl_/dansen voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, 
[135]  ze_/pronpers_/ze schrijven_/verbinf_/schrijven gedichten_/nounpl_/gedicht en_/conjcoord_/en verhalen_/nounpl_/verhaal over_/prep_/over liefde_/nounsg_/liefde ._/$._/. 
[136]  Ze_/nounpl_/<unknown> vertellen_/verbprespl_/vertellen mythes_/nounpl_/mythe en_/conjcoord_/en legendes_/nounpl_/legende over_/prep_/over liefde_/nounsg_/liefde ._/$._/. 
[137]  Ze_/nounpl_/<unknown> lijden_/verbinf_/lijden voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, ze_/pronpers_/ze leven_/nounsg_/leven voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, 
[138]  ze_/pronpers_/ze doden_/verbinf_/doden voor_/prep_/voor liefde_/nounsg_/liefde ,_/punc_/, en_/conjcoord_/en ze_/pronpers_/ze sterven_/verbprespl_/sterven voor_/prep_/voor liefde_/nounsg_/liefde ._/$._/. 
[139]  Antropologisten_/nounpl_/<unknown> hebben_/verbprespl_/hebben bewijs_/nounsg_/bewijs gevonden_/verbpapa_/vinden van_/prep_/van romantische_/adj_/romantisch liefde_/nounsg_/liefde in_/prep_/in 170_/num__card_/@card@ samenlevingen_/nounpl_/samenleving ._/$._/. 
[140]  Maar_/adj_/<unknown> liefde_/nounsg_/liefde is_/verbpressg_/zijn niet_/adv_/niet altijd_/adv_/altijd een_/det__art_/een blije_/adj_/blij ervaring_/nounsg_/ervaring ._/$._/. 
[141]  stelden_/verbpastpl_/stellen ze_/pronpers_/ze verschillende_/adj_/verschillend vragen_/nounpl_/vraag over_/prep_/over liefde_/nounsg_/liefde ,_/punc_/, 
[142]  Bijna_/nounpl_/<unknown> niemand_/pronindef_/niemand overleeft_/verbpressg_/overleven liefde_/nounsg_/liefde ._/$._/. 
[143]  wat_/pronrel_/wat volgens_/prep_/volgens mij_/pronpers_/mij het_/det__art_/het meest_/adj_/meest krachtige_/adj_/krachtig liefdesgedicht_/nounsg_/liefdesgedicht op_/prep_/op aarde_/nounsg_/aarde is_/verbpressg_/zijn ._/$._/. 
[144]  pijn_/nounsg_/pijn loopt_/verbpressg_/lopen door_/prep_/door mijn_/det__poss_/mijn lichaam_/nounsg_/lichaam met_/prep_/met het_/det__art_/het voor_/adv_/voor van_/prep_/van mijn_/det__poss_/mijn liefde_/nounsg_/liefde voor_/prep_/voor jou_/pronpers_/jou ,_/punc_/, 
[145]  Pijn_/nounsg_/pijn als_/conjsubo_/als een_/det__art_/een op_/prep_/op barsten_/nounsg_/barsten staande_/prep_/staande zweer_/nounsg_/zweer met_/prep_/met mijn_/det__poss_/mijn liefde_/nounsg_/liefde voor_/prep_/voor jou_/pronpers_/jou ,_/punc_/, 
[146]  opgebruikt_/adj_/<unknown> door_/prep_/door vuur_/nounsg_/vuur van_/prep_/van mijn_/det__poss_/mijn liefde_/nounsg_/liefde voor_/prep_/voor jou_/pronpers_/jou ,_/punc_/, 
[147]  Ik_/nounsg_/ik denk_/verbpressg_/denken aan_/prep_/aan jou_/pronpers_/jou liefde_/nounsg_/liefde voor_/prep_/voor mij_/pronpers_/mij ,_/punc_/, 
[148]  Ik_/nounsg_/ik word_/verbpressg_/worden verscheurd_/verbpapa_/verscheuren door_/prep_/door jou_/pronpers_/jou liefde_/nounsg_/liefde voor_/prep_/voor mij_/pronpers_/mij ._/$._/. 
[149]  waar_/pronadv_/waar ga_/verbpressg_/gaan je_/pronpers_/je met_/prep_/met mijn_/det__poss_/mijn liefde_/nounsg_/liefde heen_/adv_/heen ?_/$._/? 
[150]  Herinner_/nounsg_/<unknown> wat_/pronrel_/wat ik_/pronpers_/ik zei_/verbpastsg_/zeggen ,_/punc_/, mijn_/det__poss_/mijn lief_/nounsg_/lief ._/$._/. 
[151]  Vaarwel_/nounsg_/vaarwel ,_/punc_/, mijn_/det__poss_/mijn lief_/nounsg_/lief ,_/punc_/, vaarwel_/nounsg_/vaarwel ._/$._/. "_/punc_/" 
[152]  Romantische_/adj_/romantisch liefde_/nounsg_/liefde is_/verbpressg_/zijn één_/det__art_/één van_/prep_/van de_/det__art_/de meest_/adj_/meest krachtige_/adj_/krachtig gevoelens_/nounpl_/gevoelen op_/prep_/op aarde_/nounsg_/aarde ._/$._/. 
[153]  Maar_/nounpl_/<unknown> romantische_/adj_/romantisch liefde_/nounsg_/liefde is_/verbpressg_/zijn veel_/adv_/veel meer_/adv_/meer dan_/conjsubo_/dan een_/det__art_/een cocaïne_/nounsg_/cocaïne high_/adj_/high --_/punc_/-- 
[154]  Romantische_/adj_/romantisch liefde_/nounsg_/liefde is_/verbpressg_/zijn een_/det__art_/een obsessie_/nounsg_/obsessie ._/$._/. Het_/pronpers_/het beheerst_/verbpressg_/beheersen je_/pronpers_/je ._/$._/. 
[155]  Wild_/nounsg_/wild is_/verbpressg_/zijn liefde_/nounsg_/liefde ._/$._/. 
[156]  verbonden_/verbpapa_/verbinden met_/prep_/met intense_/adj_/intens romantische_/adj_/romantisch liefde_/nounsg_/liefde ._/$._/. 
[157]  hij_/pronpers_/hij zei_/verbpastsg_/zeggen :_/$._/: "_/punc_/" Hoe_/pronadv_/hoe minder_/adj_/min mijn_/det__poss_/mijn hoop_/nounsg_/hoop ,_/punc_/, hoe_/pronadv_/hoe heter_/adj_/heet mijn_/det__poss_/mijn liefde_/nounsg_/liefde ._/$._/. "_/punc_/" 
[158]  Wanneer_/nounsg_/<unknown> je_/pronpers_/je afgewezen_/verbpapa_/afwijzen wordt_/verbpressg_/worden in_/prep_/in liefde_/nounsg_/liefde ,_/punc_/, 
[159]  wordt_/verbpressg_/worden je_/pronpers_/je niet_/adv_/niet alleen_/adv_/alleen overspoeld_/verbpapa_/overspoelen door_/prep_/door gevoelens_/nounpl_/gevoelen van_/prep_/van romantische_/adj_/romantisch liefde_/nounsg_/liefde ,_/punc_/, 
[160]  dat_/conjsubo_/dat romantische_/adj_/romantisch liefde_/nounsg_/liefde een_/det__art_/een drijfveer_/nounsg_/drijfveer is_/verbpressg_/zijn ,_/punc_/, een_/det__art_/een primitieve_/adj_/primitief voortplantingsdrang_/nounsg_/<unknown> ._/$._/. 
[161]  Romantische_/adj_/romantisch liefde_/nounsg_/liefde stelt_/verbpressg_/stellen je_/pronpers_/je in_/prep_/in staat_/nounsg_/staat te_/partte_/te je_/pronposs_/je voortplantingsenergie_/nounsg_/<unknown> te_/partte_/te richten_/verbinf_/richten 
[162]  Ik_/nounpl_/<unknown> denk_/verbpressg_/denken dat_/pronrel_/dat van_/prep_/van alle_/det__indef_/alle poëzie_/nounsg_/poëzie die_/pronrel_/die ik_/pronpers_/ik over_/prep_/over romantische_/adj_/romantisch liefde_/nounsg_/liefde las_/verbpressg_/lassen ,_/punc_/, 
[163]  Hij_/nounsg_/<unknown> zei_/verbpastsg_/zeggen :_/$._/: "_/punc_/" De_/det__art_/de god_/nounsg_/god van_/prep_/van liefde_/nounsg_/liefde leeft_/verbpressg_/leven in_/prep_/in een_/det__art_/een toestand_/nounsg_/toestand van_/prep_/van behoefte_/nounsg_/behoefte ._/$._/. 
[164]  Ik_/nounsg_/ik ben_/verbpressg_/zijn ook_/adv_/ook gaan_/verbinf_/gaan geloven_/verbinf_/geloven dat_/conjsubo_/dat romantische_/adj_/romantisch liefde_/nounsg_/liefde een_/det__art_/een verslaving_/nounsg_/verslaving is_/verbpressg_/zijn :_/$._/: 
[165]  Ik_/nounpl_/<unknown> heb_/verbpressg_/hebben een_/det__art_/een vriendin_/nounsg_/vriendin die_/pronrel_/die net_/adv_/net over_/prep_/over een_/det__art_/een verschrikkelijke_/adj_/verschrikkelijk liefdesaffaire_/nounsg_/<unknown> probeert_/verbpressg_/proberen te_/partte_/te raken_/verbinf_/raken ,_/punc_/, 
[166]  romantische_/adj_/romantisch liefde_/nounsg_/liefde één_/det__art_/één van_/prep_/van de_/det__art_/de meest_/adj_/meest verslavende_/adj_/verslavend stoffen_/nounpl_/stof op_/prep_/op aarde_/nounsg_/aarde is_/verbpressg_/zijn ._/$._/. 
[167]  van_/prep_/van wat_/pronrel_/wat jij_/pronpers_/jij en_/conjcoord_/en ik_/pronpers_/ik "_/punc_/" Liefde_/nounsg_/liefde op_/prep_/op het_/det__art_/het eerste_/num__ord_/een gezicht_/nounsg_/gezicht "_/punc_/" noemen_/verbinf_/noemen ._/$._/. 
[168]  wat_/pronrel_/wat ik_/pronpers_/ik weet_/verbpressg_/weten over_/prep_/over liefde_/nounsg_/liefde het_/pronpers_/het verpest_/verbpapa_/verpesten heeft_/verbpressg_/hebben voor_/prep_/voor mij_/pronpers_/mij ._/$._/. 
[169]  verbonden_/verbpapa_/verbinden met_/prep_/met intense_/adj_/intens romantische_/adj_/romantisch liefde_/nounsg_/liefde ,_/punc_/, 
[170]  ongestelde_/adj_/ongesteld vragen_/nounpl_/vraag over_/prep_/over romantische_/adj_/romantisch liefde_/nounsg_/liefde ._/$._/. 
[171]  en_/conjcoord_/en op_/prep_/op een_/det__art_/een bepaald_/verbpapa_/bepalen punt_/nounsg_/punt --_/punc_/-- er_/pronadv_/er zal_/verbpressg_/zullen altijd_/adv_/altijd magie_/nounsg_/magie zijn_/verbprespl_/zijn in_/prep_/in liefde_/nounsg_/liefde ,_/punc_/, 
[172]  Dus_/nounsg_/<unknown> dit_/prondemo_/dit is_/verbpressg_/zijn mijn_/det__poss_/mijn conclusie_/nounsg_/conclusie :_/$._/: Liefde_/nounsg_/liefde zit_/verbpressg_/zitten in_/prep_/in ons_/pronpers_/ons ._/$._/. 
[173]  van_/prep_/van het_/det__art_/het feit_/nounsg_/feit dat_/pronrel_/dat mijn_/det__poss_/mijn eerste_/num__ord_/een liefde_/nounsg_/liefde een_/det__art_/een meisje_/nounsg_/meisje was_/verbpastsg_/wezen|zijn ._/$._/. 
[174]  waar_/pronadv_/waar experts_/nounpl_/expert onze_/det__poss_/ons liefde_/nounsg_/liefde op_/prep_/op de_/det__art_/de nationale_/adj_/nationaal televisie_/nounsg_/televisie met_/prep_/met bestialiteit_/nounsg_/<unknown> vergeleken_/verbpapa_/vergelijken ._/$._/. 
[175]  Het_/nounsg_/<unknown> is_/verbpressg_/zijn mijn_/det__poss_/mijn baby_/nounsg_/baby en_/conjcoord_/en hij_/pronpers_/hij is_/verbpressg_/zijn een_/pronindef_/een en_/conjcoord_/en al_/det__indef_/al liefde_/nounsg_/liefde ._/$._/. "_/punc_/" 
[176]  Zo_/nounsg_/<unknown> gaat_/verbpressg_/gaan dat_/conjsubo_/dat met_/prep_/met die_/det__demo_/die dingen_/nounpl_/ding ._/$._/. Neem_/verbpressg_/nemen nu_/adv_/nu de_/det__art_/de liefde_/nounsg_/liefde voor_/prep_/voor mijn_/det__poss_/mijn kind_/nounsg_/kind ._/$._/. 
[177]  En_/nounsg_/<unknown> we_/pronpers_/we weten_/verbinf_/weten dat_/conjsubo_/dat liefde_/nounsg_/liefde voor_/prep_/voor ons_/pronpers_/ons veerkrachtig_/adj_/veerkrachtig genoeg_/adv_/genoeg is_/verbpressg_/zijn 
[178]  dat_/prondemo_/dat is_/verbpressg_/zijn wat_/pronrel_/wat van_/prep_/van onze_/det__poss_/ons verhalen_/nounpl_/verhaal liefdesverhalen_/nounsg_/<unknown> kan_/verbpressg_/kunnen maken_/verbinf_/maken 
[179]  geïnteresseerd_/verbpapa_/interesseren waren_/verbpastpl_/wezen|zijn in_/prep_/in de_/det__art_/de liefde_/nounsg_/liefde ,_/punc_/, niet_/adv_/niet in_/prep_/in oorlog_/nounsg_/oorlog ._/$._/. 
[180]  Venus_/nounprop_/Venus is_/verbpressg_/zijn natuurlijk_/adj_/natuurlijk de_/det__art_/de godin_/nounsg_/godin van_/prep_/van de_/det__art_/de liefde_/nounsg_/liefde en_/conjcoord_/en de_/det__art_/de vruchtbaarheid_/nounsg_/vruchtbaarheid ._/$._/. 
[181]  We_/nounpl_/<unknown> zien_/verbprespl_/zien dit_/prondemo_/dit wanneer_/conjsubo_/wanneer we_/pronpers_/we geliefden_/verbpastpl_/gelieven over_/prep_/over straat_/nounsg_/straat zien_/verbinf_/zien lopen_/verbinf_/lopen ,_/punc_/, 
[182]  wanneer_/conjsubo_/wanneer een_/det__art_/een gelovige_/nounsg_/gelovige zich_/pronrefl_/zich een_/det__art_/een voelt_/verbpressg_/voelen met_/prep_/met God's_/nounpl_/<unknown> liefde_/nounsg_/liefde ._/$._/. 
[183]  Velen_/nounpl_/<unknown> van_/prep_/van ons_/pronpers_/ons voelen_/verbprespl_/voelen het_/det__art_/het in_/prep_/in de_/det__art_/de liefde_/nounsg_/liefde 
[184]  wanneer_/conjsubo_/wanneer geliefden_/nounpl_/geliefde zich_/pronrefl_/zich samengesmolten_/verbpapa_/samensmelten voelen_/verbprespl_/voelen ._/$._/. 
[185]  kan_/verbpressg_/kunnen opwekken_/verbinf_/opwekken --_/punc_/-- zoals_/conjsubo_/zoals nieuwsgierigheid_/nounsg_/nieuwsgierigheid of_/conjcoord_/of liefde_/nounsg_/liefde ,_/punc_/, 
[186]  maar_/conjcoord_/maar ze_/pronpers_/ze knipperden_/verbpastpl_/knipperen uit_/prep_/uit warmte_/nounsg_/warmte ,_/punc_/, intense_/adj_/intens warmte_/nounsg_/warmte voor_/prep_/voor haar_/det__poss_/haar leerlingen_/nounpl_/leerling|leerlinge ._/$._/. 
[187]  Dat_/nounsg_/<unknown> betekent_/verbpressg_/betekenen dat_/conjsubo_/dat ik_/pronpers_/ik mijn_/det__poss_/mijn passie_/nounsg_/passie voor_/prep_/voor wiskunde_/nounsg_/wiskunde en_/conjcoord_/en magie_/nounsg_/magie combineer_/verbpressg_/combineren 
[188]  Aan_/adj_/<unknown> mensen_/nounpl_/mens die_/pronrel_/die elkaar_/pronrefl_/elkaar hun_/det__poss_/hun liefde_/nounsg_/liefde overbrengen_/verbinf_/overbrengen ._/$._/. 
[189]  Maar_/nounsg_/maar ik_/pronpers_/ik wil_/verbpressg_/willen starten_/verbinf_/starten met_/prep_/met mijn_/det__poss_/mijn werk_/nounsg_/werk over_/prep_/over romantische_/adj_/romantisch liefde_/nounsg_/liefde ,_/punc_/, 
[190]  17_/num__ord_/@card@ verliefden_/nounpl_/<unknown> wiens_/det__rel_/wie liefde_/nounsg_/liefde werd_/verbpastsg_/worden geaccepteerd_/verbpapa_/accepteren ;_/punc_/; 
[191]  en_/conjcoord_/en dan_/adv_/dan ingaan_/verbinf_/ingaan op_/prep_/op waar_/pronadv_/waar mijns_/adj_/<unknown> inziens_/nounsg_/inzien liefde_/nounsg_/liefde naartoe_/adj_/<unknown> gaat_/verbpressg_/gaan ._/$._/. 
[192]  "_/punc_/" Wat_/nounsg_/<unknown> is_/verbpressg_/zijn houden_/verbinf_/houden van_/prep_/van ?_/$._/? "_/punc_/" zei_/verbpastsg_/zeggen Shakespeare_/nounsg_/<unknown> ._/$._/. 
[193]  In_/nounpl_/<unknown> het_/det__art_/het begin_/nounsg_/begin probeerde_/verbpastsg_/proberen ik_/pronpers_/ik te_/partte_/te begrijpen_/verbinf_/begrijpen wat_/pronrel_/wat romantische_/adj_/romantisch liefde_/nounsg_/liefde was_/verbpastsg_/wezen|zijn 
[194]  Hij_/nounsg_/<unknown> zei_/verbpastsg_/zeggen ,_/punc_/, "_/punc_/" Liefde_/nounsg_/liefde is_/verbpressg_/zijn de_/det__art_/de verschillen_/nounpl_/verschil tussen_/prep_/tussen de_/det__art_/de ene_/num__card_/een vrouw_/nounsg_/vrouw en_/conjcoord_/en de_/det__art_/de ander_/adj_/ander overschatten_/nounsg_/overschatten ._/$._/. "_/punc_/" 
[195]  Zoals_/nounpl_/<unknown> Chaucer_/nounsg_/<unknown> zei_/verbpastsg_/zeggen ,_/punc_/, "_/punc_/" Liefde_/nounsg_/liefde is_/verbpressg_/zijn blind_/adj_/blind ._/$._/. "_/punc_/" 
[196]  In_/nounsg_/<unknown> de_/det__art_/de poging_/nounsg_/poging romantische_/adj_/romantisch liefde_/nounsg_/liefde te_/partte_/te begrijpen_/verbinf_/begrijpen ,_/punc_/, 
[197]  besloot_/verbpastsg_/besluiten ik_/pronpers_/ik poëzie_/nounsg_/poëzie van_/prep_/van over_/prep_/over de_/det__art_/de hele_/adj_/heel wereld_/nounsg_/wereld te_/partte_/te gaan_/verbinf_/gaan lezen_/verbinf_/lezen ,_/punc_/, 
[198]  Eenvoudig_/adj_/eenvoudig ._/$._/. Romantische_/adj_/romantisch liefde_/nounsg_/liefde is_/verbpressg_/zijn zeer_/adj_/zeer eenvoudig_/adj_/eenvoudig ._/$._/. 
[199]  De_/adj_/<unknown> hoofdkarakteristiek_/nounsg_/<unknown> van_/prep_/van romantische_/adj_/romantisch liefde_/nounsg_/liefde is_/verbpressg_/zijn verlangen_/verbinf_/verlangen :_/$._/: 
[200]  Ik_/nounsg_/ik begon_/verbpastsg_/beginnen te_/partte_/te beseffen_/verbinf_/beseffen dat_/conjsubo_/dat romantische_/adj_/romantisch liefde_/nounsg_/liefde geen_/det__indef_/geen emotie_/nounsg_/emotie is_/verbpressg_/zijn ._/$._/. 
[201]  Mensen_/nounpl_/mens leven_/verbinf_/leven voor_/prep_/voor liefde_/nounsg_/liefde ._/$._/. Ze_/pronpers_/ze doden_/verbprespl_/doden voor_/prep_/voor liefde_/nounsg_/liefde ._/$._/. Ze_/pronpers_/ze sterven_/verbprespl_/sterven voor_/prep_/voor liefde_/nounsg_/liefde ._/$._/. 
[202]  De_/nounsg_/<unknown> tweede_/num__ord_/twee van_/prep_/van deze_/det__demo_/deze drie_/num__card_/drie breinsystemen_/nounpl_/<unknown> is_/verbpressg_/zijn romantische_/adj_/romantisch liefde_/nounsg_/liefde :_/$._/: 
[203]  die_/det__demo_/die verrukking_/nounsg_/verrukking ,_/punc_/, obsessie_/nounsg_/obsessie van_/prep_/van prille_/adj_/pril liefde_/nounsg_/liefde ._/$._/. 
[204]  Ik_/nounpl_/<unknown> denk_/verbpressg_/denken dat_/det__demo_/dat romantische_/adj_/romantisch liefde_/nounsg_/liefde geëvolueerd_/verbpapa_/evolueren is_/verbpressg_/zijn om_/conjsubo_/om je_/pronpers_/je in_/prep_/in staat_/nounsg_/staat te_/partte_/te stellen_/verbinf_/stellen je_/pronpers_/je paringsenergie_/nounsg_/<unknown> te_/partte_/te focussen_/verbinf_/focussen op_/prep_/op slechts_/adv_/slechts één_/det__art_/één individu_/nounsg_/individu per_/prep_/per keer_/nounsg_/keer ,_/punc_/, 
[205]  lust_/nounsg_/lust ,_/punc_/, romantische_/adj_/romantisch liefde_/nounsg_/liefde en_/conjcoord_/en diepe_/adj_/diep gehechtheid_/nounsg_/<unknown> aan_/prep_/aan een_/det__art_/een partner_/nounsg_/partner ._/$._/. 
[206]  Ik_/nounsg_/ik zal_/verbpressg_/zullen slechts_/adv_/slechts een_/det__art_/een paar_/nounsg_/paar dingen_/verbinf_/dingen zeggen_/verbinf_/zeggen ,_/punc_/, en_/conjcoord_/en dan_/adv_/dan doorgaan_/verbinf_/doorgaan naar_/prep_/naar seks_/nounsg_/seks en_/conjcoord_/en liefde_/nounsg_/liefde ._/$._/. 
[207]  Ik_/nounpl_/<unknown> zal_/verbpressg_/zullen er_/pronadv_/er slechts_/adv_/slechts een_/det__art_/een paar_/nounsg_/paar gebruiken_/verbinf_/gebruiken en_/conjcoord_/en dan_/adv_/dan doorgaan_/verbinf_/doorgaan naar_/prep_/naar seks_/nounsg_/seks en_/conjcoord_/en liefde_/nounsg_/liefde ._/$._/. 
[208]  op_/prep_/op seks_/nounsg_/seks en_/conjcoord_/en romantiek_/nounsg_/romantiek en_/conjcoord_/en gezinsleven_/nounsg_/gezinsleven ._/$._/. 
[209]  We_/nounpl_/<unknown> zien_/verbprespl_/zien ook_/adv_/ook een_/det__art_/een stijging_/nounsg_/stijging van_/prep_/van romantische_/adj_/romantisch liefde_/nounsg_/liefde ._/$._/. 
[210]  In_/nounpl_/<unknown> deze_/det__demo_/deze drie_/num__card_/drie breinsystemen_/nounpl_/<unknown> gaan_/verbprespl_/gaan lust_/nounsg_/lust ,_/punc_/, romantische_/adj_/romantisch liefde_/nounsg_/liefde en_/conjcoord_/en 
[211]  Dopamine_/nounsg_/<unknown> wordt_/verbpressg_/worden geassocieerd_/verbpapa_/associëren met_/prep_/met romantische_/adj_/romantisch liefde_/nounsg_/liefde ,_/punc_/, 
[212]  Maar_/nounpl_/<unknown> deze_/det__demo_/deze drie_/num__card_/drie breinsystemen_/nounpl_/<unknown> :_/$._/: lust_/nounsg_/lust ,_/punc_/, romantische_/adj_/romantisch liefde_/nounsg_/liefde en_/conjcoord_/en gehechtheid_/nounsg_/<unknown> ,_/punc_/, 
[213]  terwijl_/conjsubo_/terwijl je_/pronpers_/je intense_/adj_/intens romantische_/adj_/romantisch liefde_/nounsg_/liefde voor_/prep_/voor iemand_/pronindef_/iemand anders_/adj_/anders voelt_/verbpressg_/voelen ,_/punc_/, 
[214]  naar_/prep_/naar diepe_/adj_/diep gevoelens_/nounpl_/gevoelen van_/prep_/van romantische_/adj_/romantisch liefde_/nounsg_/liefde voor_/prep_/voor iemand_/pronindef_/iemand anders_/adj_/anders ._/$._/. 
[215]  Dopamine_/nounsg_/<unknown> wordt_/verbpressg_/worden geassocieerd_/verbpapa_/associëren met_/prep_/met romantische_/adj_/romantisch liefde_/nounsg_/liefde ._/$._/. 
[216]  Ik_/nounsg_/ik zeg_/verbpressg_/zeggen simpelweg_/adv_/simpelweg dat_/conjsubo_/dat een_/det__art_/een wereld_/nounsg_/wereld zonder_/prep_/zonder liefde_/nounsg_/liefde een_/det__art_/een dooie_/nounsg_/dooie boel_/nounsg_/boel is_/verbpressg_/zijn ._/$._/. 
[217]  Ik_/nounsg_/ik bestudeer_/verbpressg_/bestuderen romantische_/adj_/romantisch liefde_/nounsg_/liefde en_/conjcoord_/en seks_/nounsg_/seks en_/conjcoord_/en gehechtheid_/nounsg_/<unknown> nu_/adv_/nu al_/conjsubo_/al 30_/num__card_/@card@ jaar_/nounsg_/jaar ._/$._/. 
[218]  Je_/nounsg_/<unknown> wordt_/verbpressg_/worden verliefd_/adj_/verliefd op_/prep_/op iemand_/pronindef_/iemand die_/pronrel_/die past_/verbpressg_/passen op_/prep_/op wat_/pronrel_/wat ik_/pronpers_/ik noem_/verbpressg_/noemen je_/pronpers_/je "_/punc_/" liefdeskaart_/nounsg_/<unknown> ,_/punc_/, "_/punc_/" 
[219]  Ik_/nounpl_/<unknown> heb_/verbpressg_/hebben het_/pronpers_/het hier_/adv_/hier de_/det__art_/de hele_/adj_/heel tijd_/nounsg_/tijd over_/prep_/over de_/det__art_/de biologie_/nounsg_/biologie van_/prep_/van de_/det__art_/de liefde_/nounsg_/liefde ._/$._/. 
[220]  En_/nounsg_/<unknown> wellicht_/adv_/wellicht het_/det__art_/het breinsysteem_/nounsg_/<unknown> voor_/prep_/voor romantische_/adj_/romantisch liefde_/nounsg_/liefde kunt_/verbpressg_/kunnen ontketenen_/verbinf_/ontketenen ._/$._/. (_/punc_/( Gelach_/nounsg_/gelach )_/punc_/) 
[221]  Er_/nounsg_/<unknown> zit_/verbpressg_/zitten magie_/nounsg_/magie aan_/prep_/aan liefde_/nounsg_/liefde !_/$._/! 
[222]  de_/det__art_/de seks_/nounsg_/seks drijfveer_/nounsg_/drijfveer ,_/punc_/, romantische_/adj_/romantisch liefde_/nounsg_/liefde en_/conjcoord_/en gehechtheid_/nounsg_/<unknown> aan_/prep_/aan een_/det__art_/een langetermijnpartner_/nounsg_/<unknown> ._/$._/. 
[223]  dat_/conjsubo_/dat wil_/nounsg_/wil zeggen_/verbinf_/zeggen :_/$._/: de_/det__art_/de oorsprong_/nounsg_/oorsprong van_/prep_/van het_/det__art_/het woord_/nounsg_/woord is_/verbpressg_/zijn liefde_/nounsg_/liefde en_/conjcoord_/en passie_/nounsg_/passie ._/$._/. 
[224]  Mijn_/nounsg_/mijn liefde_/nounsg_/liefde is_/verbpressg_/zijn als_/conjsubo_/als twaalf_/num__card_/twaalf Ethiopische_/nounsg_/Ethiopische geiten_/nounpl_/geit 
[225]  Niet_/nounsg_/<unknown> omdat_/conjsubo_/omdat ik_/pronpers_/ik geconfronteerd_/verbpapa_/confronteren werd_/verbpastsg_/worden met_/prep_/met homoseksuele_/adj_/homoseksueel seks_/nounsg_/seks en_/conjcoord_/en liefde_/nounsg_/liefde 
[226]  "_/punc_/" Hier_/nounsg_/<unknown> is_/verbpressg_/zijn liefde_/nounsg_/liefde ,_/punc_/, alles_/pronindef_/alles ._/$._/. "_/punc_/" 
[227]  We_/nounpl_/<unknown> zouden_/verbpastpl_/zullen geen_/det__indef_/geen toegang_/nounsg_/toegang hebben_/verbprespl_/hebben tot_/prep_/tot liefde_/nounsg_/liefde ,_/punc_/, 
[228]  "_/punc_/" Nee_/adj_/<unknown> ,_/punc_/, nee_/int_/nee ,_/punc_/, nee_/int_/nee !_/$._/! Een_/det__art_/een kikker_/nounsg_/kikker !_/$._/! "_/punc_/" 
[229]  "_/punc_/" Nee_/nounsg_/<unknown> ._/$._/. Het_/pronpers_/het moet_/verbpressg_/moeten Go_/adj_/<unknown> Go_/nounpl_/<unknown> Gadget_/nounsg_/<unknown> worden_/verbprespl_/worden !_/$._/! "_/punc_/" 
[230]  waar_/pronadv_/waar kleine_/adj_/klein dingen_/nounpl_/ding worden_/verbprespl_/worden gedaan_/verbpapa_/doen voor_/prep_/voor de_/det__art_/de liefde_/nounsg_/liefde 
[231]  En_/adj_/<unknown> plots_/nounpl_/plot kan_/verbpressg_/kunnen men_/pronpers_/men grote_/adj_/groot dingen_/nounpl_/ding doen_/verbinf_/doen voor_/prep_/voor de_/det__art_/de liefde_/nounsg_/liefde ._/$._/. "_/punc_/" 
[232]  Niet_/nounsg_/<unknown> al_/conjsubo_/al deze_/det__demo_/deze grote_/adj_/groot dingen_/nounpl_/ding voor_/prep_/voor de_/det__art_/de liefde_/nounsg_/liefde ,_/punc_/, deze_/det__demo_/deze experimenten_/nounpl_/experiment ,_/punc_/, 
[233]  "_/punc_/" Lieve_/adj_/lief geboortemoeder_/nounsg_/<unknown> ,_/punc_/, ik_/pronpers_/ik heb_/verbpressg_/hebben geweldige_/adj_/geweldig ouders_/nounpl_/ouder ._/$._/. 
[234]  Ik_/nounsg_/ik heb_/verbpressg_/hebben liefde_/nounsg_/liefde gevonden_/verbpapa_/vinden ._/$._/. Ik_/pronpers_/ik ben_/verbpressg_/zijn gelukkig_/adj_/gelukkig ._/$._/. "_/punc_/" 
[235]  liefde_/nounsg_/liefde en_/conjcoord_/en bewondering_/nounsg_/bewondering naar_/prep_/naar hem_/pronpers_/hem ._/$._/. 
[236]  En_/nounsg_/<unknown> ik_/pronpers_/ik denk_/verbpressg_/denken :_/$._/: ik_/pronpers_/ik zie_/verbpressg_/zien geen_/det__indef_/geen liefde_/nounsg_/liefde en_/conjcoord_/en bewondering_/nounsg_/bewondering links_/nounpl_/link ._/$._/. (_/punc_/( Gelach_/nounsg_/gelach )_/punc_/) 
[237]  dat_/pronrel_/dat is_/verbpressg_/zijn een_/det__art_/een heel_/adj_/heel ander_/adj_/ander liefdesverhaal_/nounsg_/<unknown> ._/$._/. 
[238]  want_/conjcoord_/want dit_/det__demo_/dit liefdesverhaal_/nounsg_/<unknown> ,_/punc_/, 
[239]  het_/det__art_/het verlies_/nounsg_/verlies van_/prep_/van hun_/det__poss_/hun kindje_/nounsg_/kind verwerkten_/verbpastpl_/verwerken ._/$._/. 
[240]  als_/conjsubo_/als deel_/nounsg_/deel van_/prep_/van de_/det__art_/de moderne_/adj_/modern liefde_/nounsg_/liefde en_/conjcoord_/en van_/prep_/van de_/det__art_/de individualistische_/adj_/individualistisch samenleving_/nounsg_/samenleving ._/$._/. 
[241]  Wat_/nounsg_/<unknown> is_/verbpressg_/zijn de_/det__art_/de relatie_/nounsg_/relatie tussen_/prep_/tussen liefde_/nounsg_/liefde en_/conjcoord_/en verlangen_/nounsg_/verlangen ?_/$._/? 
[242]  Als_/nounpl_/<unknown> er_/pronadv_/er een_/det__art_/een werkwoord_/nounsg_/werkwoord is_/verbpressg_/zijn dat_/pronrel_/dat past_/verbpressg_/passen bij_/prep_/bij liefde_/nounsg_/liefde ,_/punc_/, is_/verbpressg_/zijn dat_/prondemo_/dat '_/punc_/' hebben_/verbprespl_/hebben '_/punc_/' ._/$._/. 
[243]  In_/nounpl_/<unknown> liefde_/nounsg_/liefde willen_/verbprespl_/willen we_/pronpers_/we hebben_/verbinf_/hebben ,_/punc_/, we_/pronpers_/we willen_/verbprespl_/willen de_/det__art_/de geliefde_/adj_/geliefd kennen_/verbinf_/kennen ._/$._/. 
[244]  Voor_/adv_/voor iemand_/pronindef_/iemand zorgen_/verbinf_/zorgen is_/verbpressg_/zijn geweldig_/adv_/geweldig liefdevol_/adj_/liefdevol ._/$._/. Maar_/conjcoord_/maar het_/pronpers_/het is_/verbpressg_/zijn een_/det__art_/een krachtig_/adj_/krachtig anti-aphrodisiac_/nounsg_/<unknown> 
[245]  In_/nounpl_/<unknown> deze_/det__demo_/deze paradox_/nounsg_/paradox tussen_/prep_/tussen liefde_/nounsg_/liefde en_/conjcoord_/en verlangen_/nounsg_/verlangen 
[246]  die_/pronrel_/die liefde_/nounsg_/liefde voeden_/verbinf_/voeden --_/punc_/-- wederkerigheid_/nounsg_/wederkerigheid ,_/punc_/, 
[247]  die_/pronrel_/die niet_/adv_/niet altijd_/adv_/altijd de_/det__art_/de liefde_/nounsg_/liefde ten_/prep_/te goede_/nounsg_/goede komen_/verbprespl_/komen :_/$._/: 
[248]  omdat_/conjsubo_/omdat we_/pronpers_/we denken_/verbprespl_/denken dat_/conjsubo_/dat bij_/prep_/bij liefde_/nounsg_/liefde onzelfzuchtigheid_/nounsg_/<unknown> hoort_/verbpressg_/horen 
[249]  die_/pronrel_/die met_/prep_/met extra_/adj_/extra zorg_/nounsg_/zorg beladen_/verbpapa_/beladen zal_/verbpressg_/zullen worden_/verbinf_/worden ,_/punc_/, 
[250]  Erotische_/adj_/erotisch koppels_/nounpl_/koppel begrijpen_/verbprespl_/begrijpen ook_/adv_/ook dat_/conjsubo_/dat passie_/nounsg_/passie toe-_/adj_/<unknown> en_/conjcoord_/en afneemt_/verbpressg_/afnemen ._/$._/. 
[251]  zal_/verbpressg_/zullen je_/pronpers_/je een_/det__art_/een gevoel_/nounsg_/gevoel van_/prep_/van liefde_/nounsg_/liefde over_/prep_/over je_/pronpers_/je voelen_/verbprespl_/voelen komen_/verbinf_/komen ,_/punc_/, 
[252]  en_/conjcoord_/en ik_/pronpers_/ik was_/verbpastsg_/wezen|zijn klaar_/verbpressg_/klaren voor_/prep_/voor die_/det__demo_/die immense_/adj_/immens hoeveelheid_/nounsg_/hoeveelheid liefde_/nounsg_/liefde 
[253]  Ik_/nounpl_/<unknown> vloeide_/verbpastsg_/vloeien over_/adv_/over van_/prep_/van liefde_/nounsg_/liefde en_/conjcoord_/en affectie_/nounsg_/affectie voor_/prep_/voor mijn_/det__poss_/mijn vrouw_/nounsg_/vrouw ,_/punc_/, 
[254]  We_/nounpl_/<unknown> hebben_/verbprespl_/hebben een_/det__art_/een grafiek_/nounsg_/grafiek gemaakt_/verbpapa_/maken 
[255]  Je_/nounsg_/<unknown> mag_/verbpressg_/mogen liefde_/nounsg_/liefde niet_/adv_/niet in_/prep_/in een_/det__art_/een grafiek_/nounsg_/grafiek gieten_/verbinf_/gieten ._/$._/. 
[256]  De_/adj_/<unknown> reden_/nounsg_/reden dat_/pronrel_/dat dat_/conjsubo_/dat niet_/adv_/niet mag_/verbpressg_/mogen ,_/punc_/, 
[257]  is_/verbpressg_/zijn dat_/conjsubo_/dat we_/pronpers_/we liefde_/nounsg_/liefde als_/conjsubo_/als iets_/adv_/iets binairs_/nounpl_/<unknown> beschouwen_/verbinf_/beschouwen ._/$._/. 
[258]  Ik_/nounpl_/<unknown> denk_/verbpressg_/denken dat_/det__demo_/dat liefde_/nounsg_/liefde eigenlijk_/adj_/eigenlijk een_/det__art_/een proces_/nounsg_/proces is_/verbpressg_/zijn ._/$._/. 
[259]  als_/conjsubo_/als iets_/adv_/iets binairs_/nounpl_/<unknown> ,_/punc_/, 
[260]  dat_/conjsubo_/dat liefde_/nounsg_/liefde bedrieglijk_/adj_/bedrieglijk ,_/punc_/, ongepast_/adj_/ongepast of_/conjcoord_/of wat_/adv_/wat dan_/conjsubo_/dan ook_/adv_/ook is_/verbpressg_/zijn ._/$._/. 
[261]  Er_/nounpl_/<unknown> was_/verbpastsg_/wezen|zijn een_/det__art_/een correlatie_/nounsg_/correlatie tussen_/prep_/tussen houden_/verbinf_/houden van_/prep_/van Lego_/nounsg_/<unknown> 
[262]  Er_/nounpl_/<unknown> was_/verbpastsg_/wezen|zijn geen_/det__indef_/geen verband_/nounsg_/verband tussen_/prep_/tussen houden_/verbinf_/houden van_/prep_/van Lego_/nounsg_/<unknown> en_/conjcoord_/en hoeveel_/num__card_/hoeveel mensen_/nounpl_/mens bouwden_/verbpastpl_/bouwen ,_/punc_/, 
[263]  en_/conjcoord_/en zou_/verbpastsg_/zullen gaan_/verbinf_/gaan trouwen_/verbinf_/trouwen ,_/punc_/, want_/conjcoord_/want zij_/pronpers_/zij was_/verbpastsg_/wezen|zijn de_/det__art_/de liefde_/nounsg_/liefde van_/prep_/van zijn_/det__poss_/zijn leven_/nounsg_/leven ._/$._/. 
[264]  "_/punc_/" Ben_/nounsg_/<unknown> je_/pronpers_/je hier_/adv_/hier op_/prep_/op vakantie_/nounsg_/vakantie ?_/$._/? "_/punc_/" 
[265]  met_/prep_/met liefde_/nounsg_/liefde is_/verbpressg_/zijn gemaakt_/verbpapa_/maken ,_/punc_/, meestal_/adv_/meestal ._/$._/. 
[266]  "_/punc_/" Van_/nounsg_/<unknown> 52_/num__card_/@card@ naar_/prep_/naar 48_/num__card_/@card@ met_/prep_/met liefde_/nounsg_/liefde "_/punc_/" 
[267]  "_/punc_/" Beste_/nounabbr_/<unknown> 48_/num__card_/@card@ ,_/punc_/, ik_/pronpers_/ik beloof_/verbpressg_/beloven naar_/prep_/naar je_/pronpers_/je te_/partte_/te luisteren_/verbinf_/luisteren ,_/punc_/, voor_/prep_/voor je_/pronpers_/je te_/partte_/te vechten_/verbinf_/vechten ,_/punc_/, je_/pronpers_/je altijd_/adv_/altijd te_/partte_/te respecteren_/verbinf_/respecteren ._/$._/. "_/punc_/" 
[268]  Ik_/adj_/<unknown> deel_/nounsg_/deel met_/prep_/met plezier_/nounsg_/plezier wat_/pronrel_/wat meer_/adv_/meer liefde_/nounsg_/liefde op_/prep_/op de_/det__art_/de wereld_/nounsg_/wereld ._/$._/. 
[269]  "_/punc_/" Schat_/nounsg_/schat ,_/punc_/, geen_/det__indef_/geen kousen_/nounpl_/kous dragen_/verbinf_/dragen onder_/prep_/onder je_/pronpers_/je pyjama_/nounsg_/pyjama ,_/punc_/, 


