# query-parallel
Simple code to query parallel corpora

If you need any particular feature, or want to make the searches more powerful in any way, send me a feature request I can add it.

I have included a tagged parallel TED talk corpus. I couldn't tag portuguese.

The corpus here is based on: https://github.com/ajinkyakulkarni14/TED-Multilingual-Parallel-Corpus

If you use this tagged corpus, or found the explanations here useful pleace cite:

Laura Becker, Matías Guzmán Naranjo. (2016, June). Psych predicates from a cross-linguistic perspective. The Olomouc Linguistics Colloquium (Olinco).

(and https://github.com/ajinkyakulkarni14/TED-Multilingual-Parallel-Corpus)
